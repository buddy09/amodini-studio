<?php
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
$slug1 = $_REQUEST['param1'];
$slug2 = $_REQUEST['param2'];
$slug3 = $_REQUEST['param3'];
$short = $_REQUEST['param4'];
$page = $_REQUEST['param5'];
$order_by = '';
if($short == 'price_l_h'){
    $order_by = "Asc";
}else{
    $order_by = "Desc";
}

$cat = mysqli_query($con,"SELECT *FROM `tbl_category` WHERE slug='$slug1'");
$cr = mysqli_fetch_array($cat);
$catid = $cr['id'];
$category_name = $cr['category_name'];

$subcat = mysqli_query($con,"SELECT *FROM `tbl_sub_category` WHERE slug='$slug2'");
$scr = mysqli_fetch_array($subcat);
$subcatid = $scr['id'];
$sub_categorys = $scr['sub_category'];

$innercat = mysqli_query($con,"SELECT * FROM `tbl_inner_category` WHERE slug='$slug3'");
$icr = mysqli_fetch_array($innercat);
$innercatid = $icr['id'];
$inner_categorys = $icr['inner_category'];

$limit = 15;
if($page == 1){
    $pn = 1;
}else{
    $pn = $page;
}
$start_from = ($pn-1) * $limit;
$product = mysqli_query($con,"select p.* from tbl_products p, tbl_attributes a where p.category_id='$catid' AND p.subcat_id='$subcatid' AND p.inner_cat_id ='$innercatid' AND p.status='1' and p.is_attribute=1 order by a.price $order_by limit $start_from, $limit") or die(mysqli_error());
// $row_count = mysqli_num_rows($product);
$total_records = mysqli_num_rows($product);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $category_name; ?> / <?php echo $sub_categorys; ?></title>
    <?php include'includes/css.php'; ?>
</head>

<body class="boxed">
    <!-- Loader -->
    <!-- <div id="loader-wrapper">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
        </div>
    </div> -->

    <div id="wrapper">
        <!-- Page -->
        <div class="page-wrapper">
            <!-- Header -->
            <?php include 'includes/head.php'; ?>
            <?php include 'includes/menu.php'; ?>
            <!-- Page Content -->
            <main class="page-main">
                <div class="block">
                    <div class="container">
                        <ul class="breadcrumbs">
                            <li><a href="<?php echo $siteurl; ?>"><i class="icon icon-home"></i></a></li>
                            <li>/<span><?php echo $category_name; ?></span>/<span><?php echo $sub_categorys; ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="container">
                    <!-- Two columns -->
                    <div class="row row-table">
                        <?php include'leftbar.php'; ?>
                        <!-- Center column -->
                        <div class="col-md-9 aside">
                            <!-- Page Title -->
                            <div class="page-title">
                                <div class="title center">
                                    <h1><?php echo $category_name; ?></h1>
                                </div>
                            </div>

                            <?php
                                if(isset($_SESSION['success'])){
                                    echo '
                                    <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success!</strong> '.$_SESSION['success'].'
                                    </div>
                                    ';
                                    unset($_SESSION['success']);
                                }
                            ?>
                            <!-- /Page Title -->
                            <!-- Filter Row -->
                            <div class="filter-row">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-7 col-lg-5 col-left">
                                        <div class="filter-button">
                                            <a href="#" class="btn filter-col-toggle"><i
                                                    class="icon icon-filter"></i><span>FILTER</span></a>
                                        </div>
                                        <div class="form-label">Sort by:</div>
                                        <div class="select-wrapper-sm">
                                            <select class="form-control input-sm" id="short_by_price">
                                                <option value="price_l_h" <?php if($short=='price_l_h'){echo 'selected';}?>>Price (Low >> High)</option>
                                                <option value="price_h_l" <?php if($short=='price_h_l'){echo 'selected';}?>>Price (High >> Low )</option>
                                            </select>
                                        </div>
                                        <div class="directions">
                                            <a href="#"><i class="icon icon-arrow-down"></i></a>
                                            <a href="#"><i class="icon icon-arrow-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-lg-2 hidden-xs">
                                        <div class="view-mode">
                                            <a href="#" class="grid-view"><i class="icon icon-th"></i></a>
                                            <a href="#" class="list-view"><i class="icon icon-th-list"></i></a>
                                        </div>
                                    </div>
                                    <!-- <div class="col-xs-4 col-sm-3 col-lg-5 col-right">
                                        <div class="form-label">Show:</div>
                                        <div class="select-wrapper-sm">
                                            <select class="form-control input-sm" id="show_result">
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                                <option value="all">All</option>
                                            </select>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="bg-striped"></div>
                            </div>
                            <!-- /Filter Row -->
                            <!-- Total -->
                            <!-- <div class="items-total">Items 1 to 15 of 28 total</div> -->
                            <!-- /Total -->
                            <!-- Products Grid -->
                            <div class="products-grid three-in-row product-variant-5">
                                <!-- Product Item -->
                                <?php 
                                    if(mysqli_num_rows($product)){
                                        while($data  = mysqli_fetch_array($product)){
                                            $pid = $data['product_id'];
                                            $at = mysqli_query($con, "select *from tbl_attributes where product_id='$pid' and is_default=1") or die(mysqli_error());
									$att = mysqli_fetch_array($at);
									$aid = $att['id'];
									$imgs = mysqli_query($con, "select product_photo from tbl_product_images where attribute_id='$aid' order by imgid desc limit 0,1") or die(mysqli_error());
									$img = mysqli_fetch_array($imgs);
                                    ?>
                                <div class="product-item  large">
                                    <div class="product-item-inside">
                                        <div class="product-item-info">
                                            <!-- Product Photo -->
                                            <div class="product-item-photo">
                                            <?php 
											if($data['new_arrival'] =='1')
											{
											?>
											<div class="product-item-label label-new"><span>New</span></div>
											<?php }?>
                                            <?php 
											$dis = $att['discount'];
											if($dis > 0.00)
											{
											?>
											<div class="product-item-label label-sale"><span>-<?php echo $dis;?>%</span></div>
											<?php }?>
                                                <div class="product-item-gallery">
                                                    <!-- product main photo -->
                                                    <!-- product inside carousel -->
                                                    <div class="carousel-inside slide" data-ride="carousel">
                                                        <div class="carousel-inner" role="listbox">
                                                           <?php
                                                           $i = 1;
                                                           $aid = $att['id'];
                                                            $pslider = mysqli_query($con,"select *from tbl_product_images where attribute_id='$aid' order by imgid desc") or die(mysqli_error());
                                                            while($r = mysqli_fetch_array($pslider)){
                                                                if($i == 1){
                                                                    $ac = 'active';
                                                                }
                                                                else{
                                                                    $ac='';
                                                                }
                                                            ?>
                                                            <div class="item <?php echo $ac; ?>">
                                                                <a href="#"><img class="product-image-photo"
                                                                        src="<?php echo $siteurl; ?>admin/uploads/productImages/<?php echo $r['product_photo']; ?>"
                                                                        alt="<?php echo $data['product_name'];?>"
                                                                        ></a>
                                                            </div>
                                                            <?php $i++; } ?>
                                                        </div>
                                                        <a class="carousel-control next"></a>
                                                        <a class="carousel-control prev"></a>
                                                    </div>
                                                    <!-- /product inside carousel -->
                                                    <a href="<?php echo $siteurl; ?>product-detail/<?php echo $data['slug'];?>/0/0"
                                                        title="Quick View" class="quick-view-btn"> <i
                                                            class="icon icon-eye"></i><span>Quick View</span></a>
                                                    <!-- /product main photo  -->
                                                </div>
                                                <!-- Product Actions -->
                                                <a href="javascript:void(0);" id="<?php echo $pid;  ?>"
                                                    title="Add to Wishlist"
                                                    class="addToWishlist pdpwishlist no_wishlist">
                                                    <?php
                                                $query = mysqli_query($con,"Select * from wishlist_item where product_id='".$pid."' && uid='".$_SESSION['uid']."'");
                                                $totalRows = mysqli_num_rows($query);
                                                if($totalRows > 0)
                                                {
                                                ?>
                                                <i class="icon icon-heart" style="color:red;"></i>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <i class="icon icon-heart"></i>
                                                <?php } ?>

                                                <span>Add to Wishlist</span> </a>
                                                <div class="product-item-actions">
                                                    <div class="share-button toBottom">
                                                        <span class="toggle"></span>
                                                        <ul class="social-list">
                                                            <li>
                                                                <a href="#" class="icon icon-google google"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="icon icon-fancy fancy"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="icon icon-pinterest pinterest"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" class="icon icon-twitter-logo twitter"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#"
                                                                    class="icon icon-facebook-logo facebook"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- /Product Actions -->
                                            </div>
                                            <!-- /Product Photo -->
                                            <!-- Product Details -->
                                            <div class="product-item-details">
                                                <div class="product-item-name"> <a title=""
                                                        href="<?php echo $siteurl; ?>product-detail/<?php echo $data['slug'];?>/0/0"
                                                        class="product-item-link"><?php echo $data['product_name'];?></a>
                                                </div>
                                                <div class="product-item-description">
                                                    <?php if($data['short_description'] == ''){}else{echo $data['short_description'];} ?>
                                                </div>
                                                <div class="price-box"> <span class="price-container"> <span
                                                class="price-wrapper">
                                                <?php 
												if($dis >0.00)
												{
												?>
												<span class="old-price">&#8377;<?php echo number_format($att['price'], 2);?></span>
												<span class="special-price">&#8377;<?php echo number_format($att['price']-(($att['price']*$att['discount'])/100), 2);?></span> 
												<?php }else{?>
												<span class="special-price">&#8377;<?php echo number_format($att['price'], 2);?></span>
												<?php }?>
                                                            </span>
                                                    </span>
                                                </div>
                                                <div class="product-item-rating"> <i class="icon icon-star-fill"></i><i
                                                        class="icon icon-star-fill"></i><i
                                                        class="icon icon-star-fill"></i><i
                                                        class="icon icon-star-fill"></i><i
                                                        class="icon icon-star-fill"></i></div>
                                                <!-- <button class="btn add-to-cart" data-product="789123"> <i
                                                        class="icon icon-cart"></i><span>Add to Cart</span> </button> -->
                                                <a href="<?php echo $siteurl; ?>product-detail/<?php echo $data['slug'];?>/0/0"><button class="btn"> <i
                                                        class="icon icon-cart"></i><span>Add to Cart</span> </button></a>
                                                <!-- <button class="btn add-cart-item" id="<?php echo $data['product_id'];?>"> <i
                                                        class="icon icon-cart"></i><span>Add to Cart</span> </button>
                                                <input type="hidden" id="hdnpPrice-<?php echo $data['product_id']?>" value="<?php echo $data['product_price']?>"> -->
                                            </div>
                                            <!-- /Product Details -->
                                        </div>
                                    </div>
                                </div>
                                <!-- /Product Item -->

                                <?php } ?>
                                <?php }else{ ?>
                                <div class="product-item-details">
                                    <div class="product-item-name">
                                        <center>
                                            <p>Record Not Found</p>
                                        </center>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <!-- /Products Grid -->
                            <!-- Filter Row -->
                            <!-- <div class="filter-row">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-7 col-lg-5 col-left">
                                        <div class="filter-button">
                                            <a href="#" class="btn filter-col-toggle"><i
                                                    class="icon icon-filter"></i><span>FILTER</span></a>
                                        </div>
                                        <div class="form-label">Sort by:</div>
                                        <div class="select-wrapper-sm">
                                            <select class="form-control input-sm">
                                                <option value="featured">Featured</option>
                                                <option value="rating">Rating</option>
                                                <option value="price">Price</option>
                                            </select>
                                        </div>
                                        <div class="directions">
                                            <a href="#"><i class="icon icon-arrow-down"></i></a>
                                            <a href="#"><i class="icon icon-arrow-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-lg-2 hidden-xs">
                                        <div class="view-mode">
                                            <a href="#" class="grid-view"><i class="icon icon-th"></i></a>
                                            <a href="#" class="list-view"><i class="icon icon-th-list"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-3 col-lg-5 col-right">
                                        <div class="form-label">Show:</div>
                                        <div class="select-wrapper-sm">
                                            <select class="form-control input-sm">
                                                <option value="featured">12</option>
                                                <option value="rating">36</option>
                                                <option value="price">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- /Filter Row -->
                            <!-- Total -->
                            <div class="pagination-container">
                            <ul class="pagination" role="menubar" aria-label="Pagination">
                            <?php
                                // $row = mysqli_query($con, "select *from tbl_products where category_id='$catid' and subcat_id='$subcatid' and inner_cat_id ='$innercatid'") or die(mysqli_error());
                                // $total_records = mysqli_num_rows($row);
                                $total_pages = ceil($total_records / $limit);
                                if($pn == 1){
                                    $pagLink = "<li class='arrow unavailable'><a style='pointer-events: none;' href=''>&laquo; Previous</a></li>";
                                }else{
                                    $previous_page = $pn - 1;
                                    $pagLink = "<li class='arrow'><a href='".$siteurl."short/".$slug1."/".$slug2."/".$slug3."/".$short."/".$previous_page."'>&laquo; Previous</a></li>";
                                }
                                for($i=1; $i<=$total_pages; $i++){ 
                                    if($i==$pn)  
                                      $pagLink .= "<li class='active'><a style='pointer-events: none;' href='short/".$slug1."/".$slug2."/".$slug3."/".$short."/".$i."'>".$i."</a></li>"; 
                                    else
                                      $pagLink .= "<li><a href='".$siteurl."short/".$slug1."/".$slug2."/".$slug3."/".$short."/".$i."'>".$i."</a></li>";   
                                  };
                                  if($total_pages == $pn){
                                    $pagLink .= "<li class='arrow'><a style='pointer-events: none;' href=''>Next &raquo;</a></li>";
                                  }else{
                                    $next_page = $pn + 1;
                                    $pagLink .= "<li class='arrow'><a href='".$siteurl."short/".$slug1."/".$slug2."/".$slug3."/".$short."/".$next_page."'>Next &raquo;</a></li>";
                                  }
                                  echo $pagLink;
                                ?>
                            </ul>
                            </div>
                            <!-- /Total -->
                        </div>
                        <!-- /Center column -->
                    </div>
                    <div class="ymax"></div>
                    <!-- /Two columns -->
                </div>
            </main>
            <!-- /Page Content -->
            <!-- Footer -->
            <?php include 'includes/footer.php'; ?>
        </div>
        <!-- /Page -->
    </div>
    <!-- ProductStack -->
    <?php include 'includes/footerJs.php'; ?>
    <script>
    (function($){
        //Add To wishlist
        $(".addToWishlist").click(function(e){
            e.preventDefault();
            var product_id = $(this).attr('id');
            var myData = 'product_id=' + product_id + '&type=add_to_wishlist';
            jQuery.ajax({
                type: "POST",
                url: "<?php echo $siteurl; ?>save.php",
                dataType: "text",
                data: myData,
                success: function(response){
                    window.location.href = '';
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(thrownError);
                }
            });
        });
    })(jQuery);
  </script>
</body>
</html>