-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2020 at 12:56 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amodinistudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `photo` varchar(222) NOT NULL,
  `insert_at` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `photo`, `insert_at`, `status`) VALUES
(5, '5fe03430aeb18-1608528944.jpg', '', '1'),
(6, '5fe0343ed5c6a-1608528958.jpg', '', '1'),
(7, '5fe0344a21dac-1608528970.jpg', '', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(222) NOT NULL,
  `email` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `pass` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `password`, `pass`, `status`) VALUES
(1, 'Amodinistudio', 'admin@amodinistudio.com', 'e10adc3949ba59abbe56e057f20f883e', '123456', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cart`
--

CREATE TABLE `tbl_cart` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `size` varchar(15) NOT NULL,
  `color` varchar(15) NOT NULL,
  `quantity` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_cart`
--

INSERT INTO `tbl_cart` (`id`, `uid`, `product_id`, `price`, `size`, `color`, `quantity`) VALUES
(51, '5fec10801120b', 15, '567.00', '15', '#C70039', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category_name`, `slug`, `status`) VALUES
(2, 'Women', 'women', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_checkout`
--

CREATE TABLE `tbl_checkout` (
  `order_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `payment_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `order_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_checkout`
--

INSERT INTO `tbl_checkout` (`order_id`, `user_name`, `uid`, `payment_id`, `amount`, `payment_date`, `status`, `order_status`) VALUES
(1, 'rajesh@gmail.com', '5fec10801120b', 'pay_GGSOfeWLQzB9UX', '700', '2020-12-24 09:10:48', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_color`
--

CREATE TABLE `tbl_color` (
  `id` int(11) NOT NULL,
  `color_code` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_color`
--

INSERT INTO `tbl_color` (`id`, `color_code`, `status`) VALUES
(1, '#FF5733', 1),
(2, '#FFC300', 1),
(3, '#C70039', 1),
(4, '#900C3F', 1),
(5, '#581845', 1),
(7, '#cccccc', 1),
(8, '#e456ef', 1),
(9, '#db1f1f', 1),
(10, '#e82ce1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_detail`
--

CREATE TABLE `tbl_contact_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact_detail`
--

INSERT INTO `tbl_contact_detail` (`id`, `name`, `email`, `phone`, `website`, `time`, `facebook`, `linkedin`, `twitter`, `instagram`, `status`) VALUES
(1, 'Amodini Studio', 'support@amodinistudio.com', '+01 234 567 89', 'www.amodinistudio.com', '8:00 - 19:00, Monday - Saturday', 'https://www.facebook.com/', 'https://www.linkedin.com/', 'https://twitter.com/', 'https://www.instagram.com/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inner_category`
--

CREATE TABLE `tbl_inner_category` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `inner_category` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_inner_category`
--

INSERT INTO `tbl_inner_category` (`id`, `sub_category_id`, `inner_category`, `slug`, `status`) VALUES
(2, 2, 'Lace Blouse', 'lace-blouse', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_materials`
--

CREATE TABLE `tbl_materials` (
  `mid` int(11) NOT NULL,
  `material_name` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_materials`
--

INSERT INTO `tbl_materials` (`mid`, `material_name`, `status`) VALUES
(1, 'Paints', '0'),
(3, 'new ', '1'),
(4, 'new material', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE `tbl_pages` (
  `id` int(11) NOT NULL,
  `page_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pages`
--

INSERT INTO `tbl_pages` (`id`, `page_name`, `description`, `slug`, `status`) VALUES
(4, 'About Us', '<p><strong>Abount Us:</strong></p>\r\n\r\n<p>This beautifully stitched ready to wear saree blouse by Amodini Designer Studio is available on Amazon.in. The fabric is made of threads and sequined in a floral pattern. This readymade padded blouse has a back dori with back button and is made from 100% comfortable fabric.Pair this blouse with contrast lehenga or contrast saree and be the showstopper. With net fabric on shoulders, this sleeveless blouse is highly comfortable and a perfect pick for any occasion be it wedding, cocktail or festival. This fully stitched readymade sweetheart neck blouse has ample space for margins. We make blouse in sizes ranging from 32 to 38. The fabric, colour combinations and designs are selected after the research on fashion trends and stitching is strong enough for longer durability.Boost your royal looks by wearing this ready to wear off white saree blouse . This blouse will match most of your lehenga&#39;s , sarees and bottoms. Make your normal day, a festive day with ready to wear saree blouse by Amodini Designer Studio.</p>\r\n', 'about-us', 1),
(5, 'Privacy Policy', '<p><strong>PRIVACY POLICY</strong></p>\r\n\r\n<p><strong>Your Privacy-Our Commitment </strong></p>\r\n\r\n<p>At <a href=\"https://amodinistudio.in/\" target=\"_blank\">amodinistudio.in</a>, we are extremely proud of our commitment to protect your privacy. We value your trust in us and assure you that we would use the information we collect only to provide a more personalized shopping experience. We will work hard to earn your confidence so that you can enthusiastically use our services and recommend us to your friends and family. Please read the following policy to understand how your personal information will be treated so that you can make full use of our many offerings. <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> Privacy Guarantee <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> promises never to rent, sell, share, barter or in any other way use personally-identifying information collected during your use of our Web site except as agreed to by you. From time to time we may reveal general statistical information about our Web site and visitors, such as number of visitors, number and type of goods and services purchased, etc. but we will never refer you by name or specific address. Your trust and confidence are our highest priority.</p>\r\n\r\n<p><strong>Information we collect from you </strong></p>\r\n\r\n<p>When you use our services, we collect and store your personal information. Our primary goal in doing so is to provide an efficient, smooth and customized experience while using our services. This allows us to provide services and features that most likely meet your needs, and to customize our service to make your experience easier and quicker. To fully use our services, you will need to register using our online registration form, where you will be required to provide us with your contact information and other personal information. You may provide us with a amodinistudio login ID. We may automatically track certain information about you based upon your behaviour on our site. We use this information to do internal research on our users&rsquo; demographics, interests, and behaviour to better understand and serve our users. This information is compiled and analysed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on our site or not), which URL you next go to (whether this URL is on our site or not), what browser you are using, and your IP address, our bidding, buying and selling behaviour. We also collect other users&rsquo; comments about you in our feedback area. If you send us personal correspondence such as emails or letters or if other users or third party send us correspondence about your activities or postings on the site, we may collect such information into a file specific to you.</p>\r\n\r\n<p><strong>Our use of information collected from you</strong></p>\r\n\r\n<p>Information we collect is used to build features that will make using our service an easier and more personal experience. This includes faster purchase requests, better customer support, improved product offerings and timely notice of new <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> services and special offers. We believe these uses allow us to improve our site and better tailor it to meet your needs. We may also use your information to deliver information to you that, in some cases, are targeted to your interests, such as new services and promotions. By accepting the User Agreement you expressly agree to receive this information. We use your email address, your mailing address, and phone number to contact you regarding administrative notices, new product offerings, and communications relevant to your use of the Site.</p>\r\n\r\n<p><strong>Our Disclosure of your information</strong></p>\r\n\r\n<p>We may also use your information to deliver information to you that, in some cases, are targeted to your interests, such as new services and promotions. The following are examples of some categories of persons to whom we would part with your information from time-to-time. We also collect information from you to ensure that our site is not accessed by machines to misuse the data and to prevent hacking of sites.</p>\r\n\r\n<p>1. Advertisements</p>\r\n\r\n<p>a. Where a user enters information on any form of an advertiser the said information is simultaneously collected by <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> and the advertiser. The information is used by <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> in accordance with the terms of this privacy policy and is used by the advertiser as per the advertiser&rsquo;s prevalent privacy policies. Because we do not control the privacy practices of these advertisers, you should evaluate their practices before deciding to provide the said information. b. <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> may also aggregate (gather up data across all user accounts) personally identifiable information and disclose such information in a non-personally identifiable manner to advertisers and other third parties for other marketing and promotional purposes.</p>\r\n\r\n<p>2. Classifieds Listings</p>\r\n\r\n<p>When a user lists a classified advertisements and the said advertisement receives an expression of interest by the interested user, the interested user will be provided the contact information of the user listing the said advertisement as provided in that user&rsquo;s registration form. b. When a user expresses an interest in a classifieds advertisement, the user who has listed the advertisement will be provided the contact information of the interested user as provided in the interested user&rsquo;s registration form.</p>\r\n\r\n<p>3. Items and Services offered on the Site along with third parties</p>\r\n\r\n<p>There are certain items and services provided on the Site for which third parties request for information before the item/service is listed (example: application for home loan auctions). The information provided by the user for such purposes is simultaneously collected by <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a> and the respective third party. This information is utilized by the third party in accordance with its privacy policy and the terms of the offer. Because we do not control the privacy practices of these third parties, you should evaluate their practices before deciding to use their services.</p>\r\n\r\n<p>4. Internal Service Providers</p>\r\n\r\n<p>We may use internal service providers to facilitate our services (e.g., research, surveys, data mining) and therefore we may provide some of your personally identifiable information directly to them. In some instances, the internal service provider may collect information directly from you (such as the situation where we ask a internal service provider to conduct a research for us). In these cases, you will be notified of the involvement of the internal service providers, and all information disclosures you make will be strictly optional. In all cases, this internal service provider&rsquo;s use of information we supply them is restricted by confidentiality agreements. If you provide additional information to an internal service provider directly, then their use of your information is governed by their applicable privacy policy. Because we do not control the privacy practices of these service providers, you should evaluate their practices before deciding to provide information.</p>\r\n\r\n<p><strong>How do we protect your information?</strong></p>\r\n\r\n<p>When you place orders or access your account information, we offer the use of a secure server. The secure server software (SSL) encrypts all information you input before it is sent to us. Further more, all of the customer data we collect is protected against unauthorized access.</p>\r\n\r\n<p><strong>What about children&rsquo;s privacy?</strong></p>\r\n\r\n<p>Children are not eligible to use our services and we ask that minors (under the age of 18) do not submit any personal information to us. If you are a minor, you can use this service only in conjunction with your parents or guardians.</p>\r\n\r\n<p><strong>Access or change your personally identifiable information.</strong></p>\r\n\r\n<p>To protect your privacy and security, we will verify your identity before granting access or making changes to your personally-identifying information. If you have registered your profile on <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a>, your amodinistudio ID i.e email id and amodinistudio Password are required in order to access your profile information.</p>\r\n\r\n<p><strong>Security</strong></p>\r\n\r\n<p>To prevent unauthorized access, maintain data accuracy, and ensure correct use of information, we will employ reasonable and current Internet security methods and technologies. Any changes to our privacy policy will be communicated through our Web site at least 10 days in advance of implementation. Information collected before changes are made will be secured according to the previous privacy policy.</p>\r\n\r\n<p><strong>Your consent</strong></p>\r\n\r\n<p>By using our Web site, you consent to the collection and use of this information by <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a>. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>\r\n\r\n<p><strong>Tell us what you think</strong></p>\r\n\r\n<p>At <a href=\"https://amodinistudio.in\" target=\"_blank\">amodinistudio.in</a>, we welcome your questions and comments regarding our privacy policy. Please send e-mail to support@amodinistudio.in</p>\r\n', 'privacy-policy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `inner_cat_id` int(11) NOT NULL,
  `product_material` varchar(222) NOT NULL,
  `product_size` varchar(222) NOT NULL,
  `product_color` varchar(255) NOT NULL,
  `product_name` varchar(222) NOT NULL,
  `product_code` varchar(222) NOT NULL,
  `product_price` varchar(222) NOT NULL,
  `product_photo` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `stock` int(11) NOT NULL,
  `product_stock` int(11) NOT NULL,
  `slug` varchar(222) NOT NULL,
  `meta_title` varchar(222) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `new_arrival` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `category_id`, `subcat_id`, `inner_cat_id`, `product_material`, `product_size`, `product_color`, `product_name`, `product_code`, `product_price`, `product_photo`, `short_description`, `product_description`, `stock`, `product_stock`, `slug`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `new_arrival`) VALUES
(14, 2, 2, 2, 'new ,new material', '15,10,30', '#FF5733,#FFC300,#C70039', 'Blouse One', 'BL-001', '345', '1609226723.jpg', '<p>New Blouse</p>\r\n', '<p>New Blouse</p>\r\n', 1, 12, 'blouse-one', '', '', '', '1', 1),
(15, 2, 2, 2, 'new ,new material', '15,10,30', '#C70039,#900C3F,#581845', 'Blouse Two', 'BL-002', '567', '1609226827.jpg', '<p>Blouse Two</p>\r\n', '<p>Blouse Two</p>\r\n', 1, 15, 'blouse-two', '', '', '', '1', 1),
(16, 2, 2, 2, 'new ,new material', '15,10,30', '#900C3F,#cccccc,#e456ef', 'Blouse Three', 'BL-003', '675', '1609226905.jpg', '<p>Blouse Three</p>\r\n', '<p>Blouse Three</p>\r\n', 1, 25, 'blouse-three', '', '', '', '1', 1),
(17, 2, 2, 2, 'new ,new material', '15,10,30', '#FF5733,#C70039,#900C3F', 'Blouse Four', 'BL-004', '678', '1609226990.JPG', '<p>Blouse Four</p>\r\n', '<p>Blouse Four</p>\r\n', 1, 20, 'blouse-four', '', '', '', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_images`
--

CREATE TABLE `tbl_product_images` (
  `imgid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_photo` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product_images`
--

INSERT INTO `tbl_product_images` (`imgid`, `product_id`, `product_photo`) VALUES
(42, 13, '100000129226_MAIN.jpg'),
(43, 13, '100000129226_OTHER_1.jpg'),
(44, 13, '100000129226_OTHER_2.jpg'),
(45, 13, '100000129228_MAIN.jpg'),
(46, 14, '100000129226_MAIN.jpg'),
(47, 14, '100000129226_OTHER_1.jpg'),
(48, 14, '100000129226_OTHER_2.jpg'),
(49, 14, '100000129228_MAIN.jpg'),
(50, 15, '100000129228_OTHER_1.jpg'),
(51, 15, '100000129228_OTHER_2.jpg'),
(52, 15, '100000138769_1_3.jpg'),
(53, 15, '100000138769_2_3.jpg'),
(54, 16, '100000138769_5.jpg'),
(55, 16, '100000143501_1_4.jpg'),
(56, 16, '100000143501_2_4.jpg'),
(57, 16, '100000143501_6.jpg'),
(58, 17, 'A (1) (1).JPG'),
(59, 17, 'A (1) (2).JPG'),
(60, 17, 'A (1) (27).JPG'),
(61, 17, 'A (1) (34).JPG');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_size`
--

CREATE TABLE `tbl_size` (
  `size_id` int(11) NOT NULL,
  `size_name` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_size`
--

INSERT INTO `tbl_size` (`size_id`, `size_name`, `status`) VALUES
(1, '15', '1'),
(2, '10', '1'),
(3, '30', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_category`
--

CREATE TABLE `tbl_sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sub_category`
--

INSERT INTO `tbl_sub_category` (`id`, `category_id`, `sub_category`, `slug`, `status`) VALUES
(2, 2, 'Blouse', 'blouse', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(222) NOT NULL,
  `lname` varchar(252) NOT NULL,
  `email` varchar(222) NOT NULL,
  `phone` varchar(222) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(222) NOT NULL,
  `state` varchar(222) NOT NULL,
  `country` varchar(222) NOT NULL,
  `zip` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `pass` varchar(222) NOT NULL,
  `photo` varchar(222) NOT NULL,
  `created` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `ip_address` varchar(222) NOT NULL,
  `last_update` varchar(222) NOT NULL,
  `s_address` varchar(255) NOT NULL,
  `s_city` varchar(255) NOT NULL,
  `s_state` varchar(255) NOT NULL,
  `s_country` varchar(255) NOT NULL,
  `s_zip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `fname`, `lname`, `email`, `phone`, `address`, `city`, `state`, `country`, `zip`, `password`, `pass`, `photo`, `created`, `status`, `ip_address`, `last_update`, `s_address`, `s_city`, `s_state`, `s_country`, `s_zip`) VALUES
(1, 'Vishnu', 'Choudhary', '', '8769606272', 'asdasd', 'Asdas', 'Dasd', 'Asdas', 'dasdasd', 'e10adc3949ba59abbe56e057f20f883e', '123456', '-5fd208fff3033-1607600383.jpg', '9th  December 2020 01:04 PM', '1', '::1', '10th  December 2020 05:09 PM', '', '', '', '', ''),
(3, 'Rajesh', 'Kumar', 'rajesh@gmail.com', '9784910088', 'VKIA area', 'Fdfdf', 'Rajasthan', 'India', '302013', 'e10adc3949ba59abbe56e057f20f883e', '123456', '', '23rd  December 2020 12:48 PM', '1', '::1', '23rd  December 2020 03:21 PM', 'dfdfasd asdf as', 'Ggggg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist_item`
--

CREATE TABLE `wishlist_item` (
  `wishlistid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `uid` varchar(222) NOT NULL,
  `ip_address` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wishlist_item`
--

INSERT INTO `wishlist_item` (`wishlistid`, `product_id`, `uid`, `ip_address`) VALUES
(1, 5, '1', ''),
(2, 5, '1', ''),
(3, 5, '1', ''),
(4, 5, '3', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_checkout`
--
ALTER TABLE `tbl_checkout`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_color`
--
ALTER TABLE `tbl_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_detail`
--
ALTER TABLE `tbl_contact_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inner_category`
--
ALTER TABLE `tbl_inner_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  ADD PRIMARY KEY (`imgid`);

--
-- Indexes for table `tbl_size`
--
ALTER TABLE `tbl_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wishlist_item`
--
ALTER TABLE `wishlist_item`
  ADD PRIMARY KEY (`wishlistid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_cart`
--
ALTER TABLE `tbl_cart`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_checkout`
--
ALTER TABLE `tbl_checkout`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_color`
--
ALTER TABLE `tbl_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_contact_detail`
--
ALTER TABLE `tbl_contact_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_inner_category`
--
ALTER TABLE `tbl_inner_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_pages`
--
ALTER TABLE `tbl_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  MODIFY `imgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `tbl_size`
--
ALTER TABLE `tbl_size`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wishlist_item`
--
ALTER TABLE `wishlist_item`
  MODIFY `wishlistid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;