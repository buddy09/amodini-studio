<?php 
include 'includes/config.php';
session_start();
if(isset($_SESSION['username'])){
  header('location: home.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>Web Admin panel</title>
<link rel="icon" type="image/png" href="images/fav.png">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css /color.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="notifications/css/lobibox.min.css"/>
</head>
<body>
<!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
<div class="panel-layout">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="admin-lock vh100">
          <div class="admin-form">
			  <!-- <div class="logo"><img src="images/logo2.png" alt=""></div> -->
            <h4>Sign In Account</h4>
            <span>Please enter your user information</span>
            <form method="post">
              <label><i class="fa fa-envelope"></i></label>
              <input type="email" name="email" id="email" placeholder="Email Address" style="text-transform: lowercase">
              <label><i class="fa fa-unlock-alt"></i></label>
              <input type="password" id="password" id="password"  placeholder="Password">
              <input type="checkbox" id="remember">
              <label for="remember">Remember Me <a href="#" title="">Forgot password?</a></label>
              <button type="button" id="loginBtn">sign in</button>
            </form>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="notifications/js/lobibox.min.js"></script>
<script src="notifications/js/notifications.min.js"></script>
<script src="notifications/js/notification-custom-script.js"></script>
<script src="forms.js"></script>
</body>
</html>