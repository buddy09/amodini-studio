<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manage Delivery</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
             <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p>
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
              <div class="content-area">
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Delivery List</h4>
                        <!-- <a class="btn-st" style="float: right;"  href="#addnew" data-toggle="modal" id="addTours" title="">Add New</a> -->
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Name</em></th>
                            <th><em>User Name</em></th>
                            <th><em>Date</em></th>
                            <th><em>Payment Id</em></th>
                            <th><em>Amount</em></th>
                            <th><em>Delivery Status</em></th>
                            <th><em>Update</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $i = 1;
                            $rw = mysqli_query($con,"select c.order_id,c.user_name,c.uid,c.payment_id,c.amount,left(c.payment_date,10) as payment_date,c.delivery_status,u.fname  from tbl_checkout c, tbl_users u where u.email=c.user_name") or die(mysqli_error());
                            while($row = mysqli_fetch_array($rw)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td><?php echo $row['fname'];?></td>
                            <td><?php echo $row['user_name'];?></td>
                            <td><?php echo $row['payment_date'];?></td>
                            <td><?php echo $row['payment_id'];?></td>
                            <td>&#8377; <?php echo $row['amount'];?></td>
                            <td>
                            <select name="dstatus" id="dstatus-<?php echo $row['order_id'];?>">
                            <option value="<?php echo $row['delivery_status'];?>"><?php echo $row['delivery_status'];?></option>
                            <option value="Canceled">Canceled</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Pending">Pending</option>
                            <option value="Shipped">Shipped</option>
                            </select>
                            </td>
                            <td><a href="#" title="" class="btn-st grn-clr update-status" id="updtate~status-<?php echo $row['order_id'];?>"><i class="fa fa-check"></i> Update</a></td>
                            </tr>
                           <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>

<script>
$(document).ready(function(){
  $(".update-status").click(function(){
    var id = $(this).attr("id");
    var ids = id.split("-");
    var order_id = ids[1];
    var status = $("#dstatus-" + order_id).val();
    $.ajax({
    type: 'POST',
    url: 'ajax_table.php',
    data: {tag:'update-delivery-status', order_id:order_id, status:status},
    dataType: 'text',
    cache: false,
    success: function(response){
      if(response == 1){
        window.location.reload();
      }else{
        alert("Server error");
      }
    }
  });
  });
});
</script>

</body>
</html>