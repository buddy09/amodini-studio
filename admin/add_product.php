<?php
include 'includes/session.php';
include 'includes/slugify.php';
if(isset($_POST['productAdd'])){
    $product_name = mysqli_real_escape_string($con, $_POST['product_name']);
    $product_name     = ucwords(strtolower($product_name));
    $slug = slugify($product_name);
    $product_code = mysqli_real_escape_string($con, $_POST['product_code']);
    $category = mysqli_real_escape_string($con, $_POST['category']);
    $subcategory = mysqli_real_escape_string($con, $_POST['subcategory']);
    $innercategory = mysqli_real_escape_string($con, $_POST['innercategory']);
    $short_description = mysqli_real_escape_string($con, $_POST['short_description']);
    $product_description = mysqli_real_escape_string($con, $_POST['product_description']);
    $stock = mysqli_real_escape_string($con, $_POST['stock']);
    $product_stock = mysqli_real_escape_string($con, $_POST['product_stock']);
    $meta_title = mysqli_real_escape_string($con, $_POST['meta_title']);
    $meta_keywords = mysqli_real_escape_string($con, $_POST['meta_keywords']);
    $meta_description = mysqli_real_escape_string($con, $_POST['meta_description']);
    $newarrival = $_POST['newarrival'];
    // $deal = $_POST['deal'];
    $best_seller = $_POST['bestseller'];
    
    $check = mysqli_query($con,"SELECT * FROM `tbl_products` WHERE slug='$slug'");
    if(mysqli_num_rows($check) > 1){
        $_SESSION['error'] = 'Already exist ! Please Try other name...';
        header('location: product_add.php');
        exit;
    }

$temp = explode(".", $_FILES["main_image"]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["main_image"]["tmp_name"], "uploads/productImages/" . $newfilename);

    $proIn = mysqli_query($con,"INSERT INTO `tbl_products`(`category_id`, `subcat_id`,`inner_cat_id`, `product_name`, `product_code`,`product_photo`,`short_description`, `product_description`, `stock`, `product_stock`, `slug`, `meta_title`, `meta_description`, `meta_keywords`,`new_arrival`,`best_seller`) 
    VALUES ('$category','$subcategory','$innercategory','$product_name','$product_code','$newfilename','$short_description','$product_description','$stock','$product_stock','$slug','$meta_title','$meta_description','$meta_keywords','$newarrival','$best_seller')");

  if($proIn){
    $_SESSION['success'] = 'Product added successfully';
  }else{
    $_SESSION['error'] = 'Something went wrong';
  }
  header('location: product_add.php');
}
?>