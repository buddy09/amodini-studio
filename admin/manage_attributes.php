<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin panel</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
             <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['aid'];
                  $status = $_REQUEST['status'];
                  $row = mysqli_query($con, "update tbl_attributes set status='$status' where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Status updated successfully</p>
                      </div>
                    ";
                  }
                }
                ?>
                <?php
                if(isset($_REQUEST['delete'])){
                  $id = $_REQUEST['did'];
                  $row = mysqli_query($con, "delete from tbl_attributes where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Product deleted successfully</p>
                      </div>
                    ";
                  }
                }
                ?>

                <?php 
                if(isset($_REQUEST['default'])){
                  $product_id = $_REQUEST['id'];
                  $aid = $_REQUEST['aid'];
                  mysqli_query($con, "update tbl_attributes set is_default = 0 where product_id = '$product_id'") or die(mysqli_error());
                  mysqli_query($con, "update tbl_attributes set is_default = 1 where id='$aid'") or die(mysqli_error());
                  echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Default has been set successfully</p>
                      </div>
                    ";
                }
                ?>

                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
               
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Attributes List</h4>
                        <a class="btn-st" style="float: right;" href="add_attributes.php?id=<?php echo $_REQUEST['id'];?>" title="">Add New</a>
                        <input type="hidden" id="hdnproductid" name="hdnproductid" value="<?php echo $_REQUEST['id'];?>">
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Images</em></th>
                            <th><em>Size</em></th>
                            <th><em>Color</em></th>
                            <th><em>Price</em></th>
                            <th><em>Dis.</em></th>
                            <th><em>Status</em></th>
                            <th><em>Default</em></th>
                            <th><em>Set</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $product_id = $_REQUEST['id'];
                            $i = 1;
                            $rw = mysqli_query($con,"select c.color_code,s.size_name,a.id,a.price,a.discount,a.status,a.is_default from tbl_color c,tbl_size s, tbl_attributes a where c.id=a.color_id and s.size_id=a.size_id and a.product_id='$product_id'") or die(mysqli_error());
                            while($row = mysqli_fetch_array($rw)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td>
                            <?php
                            $aid = $row['id'];
                            $img = mysqli_query($con, "select imgid,product_photo from tbl_product_images where attribute_id='$aid'") or die(mysqli_error());
                            while($imgs = mysqli_fetch_array($img))
                            {
                            ?>
                            <img style="width:50px;heigth:50px" src="uploads/productImages/<?php echo $imgs['product_photo'];?>">
                            <span class=''><a href='#' class='edit-image' id="<?php echo $imgs['imgid']; ?>"><i class='fa fa-edit'></i></a></span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <?php }?>
                            </td>
                            <td><i><?php echo $row['size_name']; ?></i></td>
                            <td><span style="background-color:<?php echo $row['color_code'];?>;width:10px;height:5px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                            <td><?php echo $row['price'];?></td>
                            <td><?php echo $row['discount'];?> %</td>
                             <td><i>
                          <?php if($row['status']=='0'){ ?>
                             <a href="manage_attributes.php?aid=<?php echo $row['id']; ?>&status=1&id=<?php echo $_REQUEST['id'];?>" class='btn btn-success'>Active</a>
                           <?php } else{?>
                            <a href="manage_attributes.php?aid=<?php echo $row['id']; ?>&status=0&id=<?php echo $_REQUEST['id'];?>" class='btn btn-danger'>Inactive</a>
                           <?php }?></i>
                            </td>
                            <td>
                            <?php 
                            $is_default = $row['is_default'];
                            if($is_default == 1){
                              echo '<img src="images/check_icon.png" style="width:24px;height:24px">';
                            }
                            ?>
                            </td>
                            <td><a href="manage_attributes.php?id=<?php echo $_REQUEST['id'];?>&aid=<?php echo $row['id'];?>&default=yes" title="" class="btn-st grn-clr update-status">Set</a></td> 
                            <td><i><a href="manage_attributes.php?did=<?php echo $row['id'];?>&delete=yes&id=<?php echo $_REQUEST['id'];?>" onclick="return confirm('Are you sure to delete?')"> <i class="icon-trash"></i></a>
                                <a href="#" id="<?php echo $row['id'];?>" class="edit-att"><i class="icon-pencil"></i></a></i>
                            </td>
                          </tr>
                         <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>
<?php include 'includes/attributes_modal.php'; ?>
<?php include 'includes/edit_att_modal.php'; ?>

<!-- <script>
$(function(){
  $(document).on('click', '#edit-image', function(e){
    e.preventDefault();
    alert();
    // $('#edit_attributes').modal('show');
    // var id = $(this).data('id');
    // alert(id);
  }); 
});

// function getRow(id){
//   $.ajax({
//     type: 'POST',
//     url: 'banner_row.php',
//     data: {id:id},
//     dataType: 'json',
//     success: function(response){
//       $('.bannerid').val(response.id);
//     }
//   });
}
</script> -->

<script>
$(document).ready(function(){
  $(".edit-image").click(function(e){
    e.preventDefault();
    var id = $(this).attr("id");
    var pid = $("#hdnproductid").val();
    $("#imgid").val(id);
    $("#pid").val(pid);
    $('#edit_attributes').modal('show');
  });

  $(".edit-att").click(function(e){
    e.preventDefault();
    var aid = $(this).attr("id");
    var pid = $("#hdnproductid").val();
    $.ajax({
    type: 'POST',
     url: 'edit_att.php',
     data: {aid:aid},
     dataType: 'json',
     success: function(response){
       $("#att_aid").val(aid);
       $("#ppid").val(pid);
       $("#att_size").append('<option selected value="' + response.size_id + '">' + response.size_name + '</option>');
       $("#att_color").append('<div class="col-md-3"><input type="radio" checked name="att_color" value="' + response.color_id + '"><span style="background-color:' + response.color_code + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>');
       $("#att_price").val(response.price);
       $("#att_discount").val(response.discount);
       $('#edit_att').modal('show');
     }
   });
  });

});
</script>


</body>
</html>