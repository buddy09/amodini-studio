<?php
   include 'includes/session.php';
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Deal</title>
      <?php include 'includes/css.php'; ?>
      <style type="text/css">
         .attributsc {
         padding: 6px;
         background: #1DE1C1;
         margin-right: 4px;
         color: #ffffff !important;
         font-weight: bold;
         border-radius: 7px;
         cursor: pointer;
         }
      </style>
   </head>
   <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
         <div id="loader">
         </div>
         <div class="loader-section section-left">
         </div>
         <div class="loader-section section-right">
         </div>
      </div>
      <div class="panel-layout">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="main-page">
                     <?php include 'includes/sidebar.php'; ?>
                     <!-- side header -->
                     <?php include 'includes/head.php'; ?>
                     <div class="main-content">
                        <div class="panel-body">
                           <div class="content-area">
                              <?php
                                 if(isset($_SESSION['error'])){
                                 echo "
                                 <div class='alert alert-danger brd-5 text-center'>
                                 <p>".$_SESSION['error']."</p> 
                                 </div>
                                 ";
                                 unset($_SESSION['error']);
                                 }
                                 if(isset($_SESSION['success'])){
                                 echo "
                                 <div class='alert alert-success brd-5 text-center'>
                                 <p>".$_SESSION['success']."</p>
                                 </div>
                                 ";
                                 unset($_SESSION['success']);
                                 }
                                 ?>
                              <div class="gap no-gap">
                                 <div class="inner-bg">
                                    <div class="element-title">
                                       <h4>Edit Deal</h4>
                                    </div>
                                    <?php 
                                    $id = $_REQUEST['id'];
                                    $row = mysqli_query($con,"select c.id as cid,c.category_name,s.id as sid,s.sub_category,i.id as iid,i.inner_category from tbl_category c, tbl_sub_category s, tbl_inner_category i, tbl_deal d where c.id=d.cat_id and s.id=d.sub_cat_id and i.id=d.inner_cat_id and d.id='$id'") or die(mysqli_error());
                                    $data = mysqli_fetch_array($row);
                                    ?>
                                    <form action="update_deal.php" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="hdnid" value="<?php echo $_REQUEST['id'];?>">
                                    <div class="add-prod-from">
                                       <div class="row">
                                          <!-- <div class="col-md-6">
                                             <label>Product Name</label>
                                             <input type="text" name="product_name" id="product_name"
                                                parsley-trigger="change" required
                                                placeholder="Enter product name" class="form-control">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Code</label>
                                             <input type="text" name="product_code" id="product_code"
                                                parsley-trigger="change" required
                                                placeholder="Enter product code" class="form-control">
                                          </div> -->
                                          <div class="col-md-6">
                                             <label>Category</label>
                                             <select id="category" name="category" parsley-trigger="change" required
                                                placeholder="Enter category" class="form-control">
                                                <option value="<?php echo $data['cid'];?>"><?php echo $data['category_name'];?></option>
                                                <?php 
                                                   $category=mysqli_query($con,"select * from tbl_category");
                                                   $total_category = mysqli_num_rows($category);
                                                   if($total_category > 0)
                                                   {
                                                       while($row=mysqli_fetch_array($category))
                                                   {
                                                           echo "<option  value='" . $row['id'] . "' >" . $row['category_name'] . "</option>";
                                                   }
                                                   }
                                                   ?>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label>Sub Category</label>
                                             <select name="subcategory" id="subcategory"
                                                placeholder="Select Sub Category" class="form-control" required>
                                                <option value="<?php echo $data['sid'];?>"><?php echo $data['sub_category'];?></option>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Inner Category</label>
                                             <select name="innercategory" id="innercategory"
                                                placeholder="Select Inner Category" class="form-control" required>
                                                <option value="<?php echo $data['iid'];?>"><?php echo $data['inner_category'];?></option>
                                             </select>
                                          </div>                
                                          </div>
                                                                                                                              
                                          <div class="col-md-12">
                                             <div class="buttonz">
                                                <button type="submit" name="submit">update</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <?php include 'includes/footer.php'; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
            
      <?php include 'includes/footerJs.php'; ?>
      <script>
         $(document).ready(function() {
             $("#category").change(function() {
                 var id = $(this).val();
                 var dataString = 'id=' + id;
                 $.ajax({
                     type: "POST",
                     url: "ajax_subcategory.php",
                     data: dataString,
                     cache: false,
                     success: function(html)
                     {
                       $("#subcategory").html(html);
                     }
                 });
             });
             
             $("#subcategory").change(function() {
                 var id = $(this).val();
                 var dataString = 'id=' + id;
                 $.ajax({
                     type: "POST",
                     url: "ajax_innercategory.php",
                     data: dataString,
                     cache: false,
                     success: function(html)
                     {
                       $("#innercategory").html(html);
                     }
                 });
             });
           
         });
      </script>
   </body>
</html>
