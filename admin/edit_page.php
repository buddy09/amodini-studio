<?php
   include 'includes/session.php';
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Edit Page
      </title>
      <?php include 'includes/css.php'; ?>
      <style type="text/css">
         .attributsc {
         padding: 6px;
         background: #1DE1C1;
         margin-right: 4px;
         color: #ffffff !important;
         font-weight: bold;
         border-radius: 7px;
         cursor: pointer;
         }
      </style>
   </head>
   <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
         <div id="loader">
         </div>
         <div class="loader-section section-left">
         </div>
         <div class="loader-section section-right">
         </div>
      </div>
      <div class="panel-layout">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="main-page">
                     <?php include 'includes/sidebar.php'; ?>
                     <!-- side header -->
                     <?php include 'includes/head.php'; ?>
                     <div class="main-content">
                        <div class="panel-body">
                           <div class="content-area">
                              <?php 
                              $id = $_REQUEST['id'];
                              $row = mysqli_query($con, "select *from tbl_pages where id='$id'") or die(mysqli_error());
                              $data = mysqli_fetch_array($row);
                              ?>
                              <div class="gap no-gap">
                                 <div class="inner-bg">
                                    <div class="element-title">
                                       <h4>Edit Page</h4>
                                    </div>
                                    <form action="update_page.php" method="POST" enctype="multipart/form-data">
                                    <div class="add-prod-from">
                                       <div class="row">
                                          <div class="col-md-6">
                                             <label>Page Name</label>
                                             <input type="text" name="page_name" id="page_name"
                                                parsley-trigger="change" required
                                                placeholder="Enter page name" class="form-control" value="<?php echo $data['page_name'];?>">
                                          </div>
                                                                                  
                                          <div class="col-md-12">
                                             <label>Page Description</label>
                                             <textarea cols="30" rows="10"
                                                placeholder="Page Description" name="page_description" id="editor1" required><?php echo $data['description'];?></textarea>
                                          </div>
                                      
                                          <div class="col-md-12">
                                             <div class="buttonz">
                                             <input type="hidden" name="hdnid" value="<?php echo $data['id'];?>">
                                                <button type="submit" name="pageUpdate">update</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <?php include 'includes/footer.php'; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script src="js/ckeditor/ckeditor.js"></script>
      <script>
         CKEDITOR.replace('editor1');
      </script>
      <?php include 'includes/footerJs.php'; ?>
      <?php include 'includes/banner_modal.php'; ?>
   </body>
</html>
