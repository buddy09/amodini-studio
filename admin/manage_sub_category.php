<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin panel home</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php 
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['id'];
                  $status = $_REQUEST['status'];
                  $row = mysqli_query($con, "update tbl_sub_category set status='$status' where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Status updated successfully</p> 
                      </div>
                    ";
                  }
                }
                ?>
                <?php 
                if(isset($_REQUEST['delete'])){
                  $id = $_REQUEST['id'];
                  $row = mysqli_query($con, "delete from tbl_sub_category where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Sub category deleted successfully</p> 
                      </div>
                    ";
                  }
                }
                ?>
                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p> 
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Category List</h4>
                        <a class="btn-st" style="float: right;"  href="#addnew" data-toggle="modal" id="addTours" title="">Add New</a>
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Category</em></th>
                            <th style="width:50%"><em>Sub Category</em></th>
                            <th><em>Status</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $i = 1;
                            $brand = mysqli_query($con,"select c.category_name,s.id,s.sub_category,s.status from tbl_category c,tbl_sub_category s where c.id=s.category_id order by c.category_name ASC") or die(mysqli_error());
                            while($row = mysqli_fetch_array($brand)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td><i><?php echo $row['category_name']; ?></i></td>
                            <td><i><?php echo $row['sub_category']; ?></i>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" style="display:none" value="<?php echo $row['sub_category'];?>" id="txt_<?php echo $row['id'];?>">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="display:none" id="update-<?php echo $row['id'];?>" class="update-sub-category"><img src="images/success-icon.png"></a>
                            </td>
                             <td><i>
                          <?php if($row['status']=='0'){ ?>
                             <a href="manage_sub_category.php?id=<?php echo $row['id']; ?>&status=1" class='btn btn-success'>Active</a>
                           <?php } else{?>
                            <a href="manage_sub_category.php?id=<?php echo $row['id']; ?>&status=0" class='btn btn-danger'>Inactive</a>
                           <?php }?></i>
                        </td>
                            <td><i><a href="manage_sub_category.php?id=<?php echo $row['id'];?>&delete=yes" onclick="return confirm('Are you sure to delete?')"> <i class="icon-trash"></i></a>
                                <a href="#" id="<?php echo $row['id'];?>" class="edit-sub-category"><i class="icon-pencil"></i></a></i>
                            </td>
                          </tr>
                         <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php  include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>
<?php include 'includes/sub_category_modal.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
  $(".edit-sub-category").click(function(e){
    e.preventDefault();
    var id = $(this).attr("id");
    $("#txt_" + id).show();
    $("#update-" + id).show();
  });

  $(".update-sub-category").click(function(e){
    e.preventDefault();
    var res = $(this).attr("id");
    var a = res.split("-");
    var id = a[1];
    var sub_category = $("#txt_" + id).val();
    $.ajax({
    type: 'POST',
    url: 'ajax_table.php',
    data: {tag:'update-sub-category', id:id, sub_category:sub_category},
    //dataType: 'json',
    cache: false,
    success: function(response){
        window.location.href="manage_sub_category.php";
    }
  });
  });

});
</script>

 <script>
$(function(){
  $(document).on('click', '.edit', function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $(document).on('click', '.delete', function(e){
    e.preventDefault();
    $('#delete').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $(document).on('click', '.photo', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    getRow(id);
  });

  $(document).on('click', '.desc', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    getRow(id);
  });

  $("#addnew").on("hidden.bs.modal", function () {
      $('.append_items').remove();
  });

  $("#edit").on("hidden.bs.modal", function () {
      $('.append_items').remove();
       getRow(id);
  });

});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'category_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
        //alert(response);
      $('.cat_name').html(response.cat_name);
      $('.brand_name').html(response.brand_name);
      $('.brand_name').html(response.brand_name);
      $('.cat_id').val(response.c_id);
      $('.b_id').val(response.b_id);
      $('#edit_cat_name').val(response.cat_name);
      $('#bselected').val(response.b_id).html(response.brand_name);
      getCategory();
    }
  });
}
// function getCategory(){
//   $.ajax({
//     type: 'POST',
//     url: 'brand_fetch.php',
//     dataType: 'json',
//     success:function(response){
//      // alert(response);
//       $('#brand_name').append(response);
//       $('#id').append(response);
//       $('#brand_id').append(response);
//     }
//   });
// }
</script>
</body>
</html>