<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin panel pages</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['id'];
                  $status = $_REQUEST['status'];
                  $row = mysqli_query($con, "update tbl_pages set status='$status' where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Status updated successfully</p>
                      </div>
                    ";
                  }
                }
                ?>
                <?php
                if(isset($_REQUEST['delete'])){
                  $id = $_REQUEST['id'];
                  $row = mysqli_query($con, "delete from tbl_pages where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Page deleted successfully</p>
                      </div>
                    ";
                  }
                }
                ?>
                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Pages List</h4>
                        <a class="btn-st" style="float: right;"  href="add_page.php" title="">Add New</a>
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Page Name</em></th>
                            <th><em>Status</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $i = 1;
                            $rw = mysqli_query($con,"select *from tbl_pages") or die(mysqli_error());
                            while($row = mysqli_fetch_array($rw)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td style="width:40%"><i><?php echo $row['page_name']; ?></i></td>
                             <td><i>
                          <?php if($row['status']=='0'){ ?>
                             <a href="manage_pages.php?id=<?php echo $row['id']; ?>&status=1" class='btn btn-success'>Active</a>
                           <?php } else{?>
                            <a href="manage_pages.php?id=<?php echo $row['id']; ?>&status=0" class='btn btn-danger'>Inactive</a>
                           <?php }?></i>
                        </td>
                            <td><i><a href="manage_pages.php?id=<?php echo $row['id'];?>&delete=yes" onclick="return confirm('Are you sure to delete?')"> <i class="icon-trash"></i></a>
                                <a href="edit_page.php?id=<?php echo $row['id'];?>"><i class="icon-pencil"></i></a></i>
                            </td>
                          </tr>
                         <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>
<?php include 'includes/category_modal.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
  $(".edit-category").click(function(e){
    e.preventDefault();
    var id = $(this).attr("id");
    $("#txt_" + id).show();
    $("#update-" + id).show();
  });

  $(".update-category").click(function(e){
    e.preventDefault();
    var res = $(this).attr("id");
    var a = res.split("-");
    var id = a[1];
    var category = $("#txt_" + id).val();
    if(category == ""){
      $("#txt_" + id).css("border-color","red");
    }else{
    $.ajax({
    type: 'POST',
    url: 'ajax_table.php',
    data: {tag:'update-category', id:id, category:category},
    //dataType: 'json',
    cache: false,
    success: function(response){
        window.location.href="manage_category.php";
    }
  });
}
  });

});
</script>

</body>
</html>