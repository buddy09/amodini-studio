<?php
   include 'includes/session.php';
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Edit Product
      </title>
      <?php include 'includes/css.php'; ?>
      <style type="text/css">
         .attributsc {
         padding: 6px;
         background: #1DE1C1;
         margin-right: 4px;
         color: #ffffff !important;
         font-weight: bold;
         border-radius: 7px;
         cursor: pointer;
         }
      </style>
   </head>
   <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
         <div id="loader">
         </div>
         <div class="loader-section section-left">
         </div>
         <div class="loader-section section-right">
         </div>
      </div>
      <div class="panel-layout">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="main-page">
                     <?php include 'includes/sidebar.php'; ?>
                     <!-- side header -->
                     <?php include 'includes/head.php'; ?>
                     <div class="main-content">
                        <div class="panel-body">
                           <div class="content-area">
                             
                              <div class="gap no-gap">
                                 <div class="inner-bg">
                                    <div class="element-title">
                                       <h4>Edit Product</h4>
                                    </div>
                                    <?php 
                                    $id = $_REQUEST['id'];
                                    $row = mysqli_query($con, "select p.*,c.id as cat_id,c.category_name,s.id as sub_cat_id,s.sub_category,i.id as inner_cat_id,i.inner_category from tbl_products p,tbl_category c,tbl_sub_category s,tbl_inner_category i where p.category_id=c.id and p.subcat_id=s.id and p.inner_cat_id=i.id and p.product_id='$id'") or die(mysqli_error());
                                    $data = mysqli_fetch_array($row);
                                    ?>
                                    <form action="update_product.php" method="POST" enctype="multipart/form-data">
                                    <div class="add-prod-from">
                                       <div class="row">
                                          <div class="col-md-6">
                                             <label>Product Name</label>
                                             <input type="text" name="product_name" id="product_name"
                                                parsley-trigger="change" required
                                                placeholder="Enter product name" class="form-control" value="<?php echo $data['product_name'];?>">
                                                <input type="hidden" id="hdnproductid" name="hdnproductid" value="<?php echo $data['product_id'];?>">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Code</label>
                                             <input type="text" name="product_code" id="product_code"
                                                parsley-trigger="change" required
                                                placeholder="Enter product code" class="form-control" value="<?php echo $data['product_code'];?>">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Category</label>
                                             <select id="category" name="category" parsley-trigger="change" required
                                                placeholder="Enter category" class="form-control">
                                                <option value="<?php echo $data['cat_id'];?>"><?php echo $data['category_name'];?></option>
                                                <?php 
                                                   $category=mysqli_query($con,"select * from tbl_category");
                                                   $total_category = mysqli_num_rows($category);
                                                   if($total_category > 0)
                                                   {
                                                       while($row=mysqli_fetch_array($category))
                                                   {
                                                           echo "<option  value='" . $row['id'] . "' >" . $row['category_name'] . "</option>";
                                                   }
                                                   }
                                                   ?>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Sub Category</label>
                                             <select name="subcategory" id="subcategory"
                                                placeholder="Select Sub Category" class="form-control" required>
                                                <option value="<?php echo $data['sub_cat_id'];?>"><?php echo $data['sub_category'];?></option>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Inner Category</label>
                                             <select name="innercategory" id="innercategory"
                                                placeholder="Select Inner Category" class="form-control" required>
                                                <option value="<?php echo $data['inner_cat_id'];?>"><?php echo $data['inner_category'];?></option>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Product Main Image</label>
                                             <input type="file" name="main_image" id="main_image"  class="form-control"/>
                                             <img src="uploads/productImages/<?php echo $data['product_photo'];?>" style="width:100px;height:100px">
                                          </div>
                                         
                                          </div>
                                                                                   
                                          <div class="col-md-12">
                                             <label>Short Description</label>
                                             <textarea cols="30" rows="10"
                                                placeholder="Short Description" name="short_description" id="editor2"><?php echo $data['short_description'];?></textarea>
                                          </div>
                                          <div class="col-md-12">
                                             <label>Product Description</label>
                                             <textarea cols="30" rows="10"
                                                placeholder="Product Description" name="product_description" id="editor1"><?php echo $data['product_description'];?></textarea>
                                          </div>
                                          <div class="col-md-6">
                                             <label><strong> Stock Availability</strong> &nbsp;&nbsp;</label>
                                             <label class="radio-inline ddx">
                                             <?php $stock = $data['stock'];?>
                                            In Stock <input type="radio" <?php if($stock==1){ echo 'checked'; }?> name="stock" id="stock" value="1"></label>
                                            
                                            Out of Stock <label class="radio-inline ddx"><input type="radio" <?php if($stock==0){ echo 'checked'; }?> name="stock" id="stock" value="0"></label>
                                          </div>
                                          <div class="col-md-6 ddc">
                                             <label>Product Stock</label>
                                             <input type="text" name="product_stock" id="product_stock"   placeholder="Enter Product Stock" class="form-control" value="<?php echo $data['product_stock'];?>">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Meta Title</label>
                                             <input type="text" name="meta_title" placeholder="Title" value="<?php echo $data['meta_title'];?>">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Meta Keyword</label>
                                             <input type="text" name="meta_keywords" placeholder="Keyword" value="<?php echo $data['meta_keywords'];?>">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Meta description</label>
                                             <textarea type="text" name="meta_description" placeholder="description"><?php echo $data['meta_description'];?></textarea>
                                          </div>
                                          <!-- <div class="col-md-12">
                                             <label><strong> Status:</strong></label>
                                             <?php $status = $data['']?>
                                             <label>Published</label>
                                             <input type="radio" name="status" value="1" checked>
                                             <label>draft</label>
                                             <input type="radio" name="status" value="0">
                                          </div> -->
                                          <div class="col-md-12">
                                             <label><strong> New Arrivals:</strong></label>
                                             <?php $na = $data['new_arrival'];?>
                                             <label>Yes</label>
                                             <input type="radio" name="newarrival" <?php if($na==1){ echo 'checked'; }?> value="1"> 
                                             <label>No</label>
                                             <input type="radio" name="newarrival" value="0" <?php if($na==0){ echo 'checked'; }?>>
                                          </div>
                                          <!-- <div class="col-md-12">
                                          <?php $deal = $data['deal'];?>
                                             <label><strong> Deal:</strong></label>
                                             <label>Yes</label>
                                             <input type="radio" name="deal" value="1" <?php if($deal==1){ echo 'checked'; }?>> 
                                             <label>No</label>
                                             <input type="radio" name="deal" value="0" <?php if($deal==0){ echo 'checked'; }?>>
                                          </div> -->
                                          <div class="col-md-12">
                                             <label><strong> Best Seller:</strong></label>
                                             <?php $bs = $data['best_seller'];?>
                                             <label>Yes</label>
                                             <input type="radio" name="bestseller" value="1" <?php if($bs==1){ echo 'checked'; }?>> 
                                             <label>No</label>
                                             <input type="radio" name="bestseller" value="0" <?php if($bs==0){ echo 'checked'; }?>>
                                          </div>
                                          
                                          <div class="col-md-12">
                                             <div class="buttonz">
                                                <button type="submit" name="update-product">Update</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <?php include 'includes/footer.php'; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script src="js/ckeditor/ckeditor.js"></script>
      <script>
         CKEDITOR.replace('editor1');
         CKEDITOR.replace('editor2');
      </script>
      <?php include 'includes/footerJs.php'; ?>

      <script>
         $(document).ready(function() {
             $("#category").change(function() {
                 var id = $(this).val();
                 var dataString = 'id=' + id;
                 $.ajax({
                     type: "POST",
                     url: "ajax_subcategory.php",
                     data: dataString,
                     cache: false,
                     success: function(html)
                     {
                       $("#subcategory").html(html);
                     }
                 });
             });
             $("#subcategory").change(function() {
                 var id = $(this).val();
                 var dataString = 'id=' + id;
                 $.ajax({
                     type: "POST",
                     url: "ajax_innercategory.php",
                     data: dataString,
                     cache: false,
                     success: function(html)
                     {
                       $("#innercategory").html(html);
                     }
                 });
             });
             $("#pmaterial").click(function() {
                 $(".pmaterial").toggle();
             });
             $("#pcertificate").click(function() {
                 $(".pcertificate").toggle();
             });
             $("#pringsize").click(function() {
                 $(".pringsize").toggle();
             });
             $("#pcolor").click(function() {
                 $(".pcolor").toggle();
             });
             $(".ddx").click(function() {
                 var chkvl = $(this).find("input").val();
                 if (chkvl == 1) {
                     $(".ddc").css('display', 'block');
                 } else {
         
                     $(".ddc").css('display', 'none');
                 }
         
             });
         });
         
     
      </script>
      
   </body>
</html>
