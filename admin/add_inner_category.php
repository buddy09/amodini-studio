
<?php
	include 'includes/session.php';
	include 'includes/slugify.php';
	if(isset($_POST['add'])){
        $sub_cat_id = $_POST['sub_cat_id'];
        $inner_category = $_POST['inner_category'];
		$slug = slugify($inner_category);	
		$stmt = mysqli_query($con,"select count(*) as numrows from tbl_inner_category where slug='$slug'");
		$row = mysqli_fetch_array($stmt);
		if($row['numrows'] > 0){
			$_SESSION['error'] = 'Inner category already exist';
		}
		else{
			$stmt = mysqli_query($con,"insert into tbl_inner_category(sub_category_id,inner_category,slug) values('$sub_cat_id','$inner_category','$slug')") or die(mysqli_error());
			$_SESSION['success'] = 'Inner category added successfully';
		}
	}
	else{
		$_SESSION['error'] = 'Fill up inner category form first';
	}
	header('location: manage_inner_category.php');
?>