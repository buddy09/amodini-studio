<?php
   include 'includes/session.php';
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Product Add
      </title>
      <?php include 'includes/css.php'; ?>
      <style type="text/css">
         .attributsc {
         padding: 6px;
         background: #1DE1C1;
         margin-right: 4px;
         color: #ffffff !important;
         font-weight: bold;
         border-radius: 7px;
         cursor: pointer;
         }
      </style>
   </head>
   <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
         <div id="loader">
         </div>
         <div class="loader-section section-left">
         </div>
         <div class="loader-section section-right">
         </div>
      </div>
      <div class="panel-layout">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="main-page">
                     <?php include 'includes/sidebar.php'; ?>
                     <!-- side header -->
                     <?php include 'includes/head.php'; ?>
                     <div class="main-content">
                        <div class="panel-body">
                           <div class="content-area">
                              <?php
                                 if(isset($_SESSION['error'])){
                                 echo "
                                 <div class='alert alert-danger brd-5 text-center'>
                                 <p>".$_SESSION['error']."</p> 
                                 </div>
                                 ";
                                 unset($_SESSION['error']);
                                 }
                                 if(isset($_SESSION['success'])){
                                 echo "
                                 <div class='alert alert-success brd-5 text-center'>
                                 <p>".$_SESSION['success']."</p>
                                 </div>
                                 ";
                                 unset($_SESSION['success']);
                                 }
                                 ?>
                              <div class="gap no-gap">
                                 <div class="inner-bg">
                                    <div class="element-title">
                                       <h4>Add New Product</h4>
                                    </div>
                                    <form action="add_product.php" method="POST" enctype="multipart/form-data">
                                    <div class="add-prod-from">
                                       <div class="row">
                                          <div class="col-md-6">
                                             <label>Product Name</label>
                                             <input type="text" name="product_name" id="product_name"
                                                parsley-trigger="change" required
                                                placeholder="Enter product name" class="form-control">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Code</label>
                                             <input type="text" name="product_code" id="product_code"
                                                parsley-trigger="change" required
                                                placeholder="Enter product code" class="form-control">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Category</label>
                                             <select id="category" name="category" parsley-trigger="change" required
                                                placeholder="Enter category" class="form-control">
                                                <option value="">Select Category</option>
                                                <?php 
                                                   $category=mysqli_query($con,"select * from tbl_category");
                                                   $total_category = mysqli_num_rows($category);
                                                   if($total_category > 0)
                                                   {
                                                       while($row=mysqli_fetch_array($category))
                                                   {
                                                           echo "<option  value='" . $row['id'] . "' >" . $row['category_name'] . "</option>";
                                                   }
                                                   }
                                                   ?>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Sub Category</label>
                                             <select name="subcategory" id="subcategory"
                                                placeholder="Select Sub Category" class="form-control">
                                                <option value="0">Select Sub Category</option>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Inner Category</label>
                                             <select name="innercategory" id="innercategory"
                                                placeholder="Select Inner Category" class="form-control">
                                                <option value="0">Select Inner Category</option>
                                             </select>
                                          </div>
                                          <div class="col-md-6">
                                             <label> Product Main Image ( 772 X 960 )</label>
                                             <input type="file" name="main_image" id="main_image"  class="form-control" required/>
                                          </div>
                                         
                                          </div>
                                                                                    
                                          <div class="col-md-12">
                                             <label>Short Description</label>
                                             <textarea cols="30" rows="10"
                                                placeholder="Short Description" name="short_description" id="editor2"></textarea>
                                          </div>
                                          <div class="col-md-12">
                                             <label>Product Description</label>
                                             <textarea cols="30" rows="10"
                                                placeholder="Product Description" name="product_description" id="editor1"></textarea>
                                          </div>
                                          <div class="col-md-6">
                                             <label><strong> Stock Availability</strong> &nbsp;&nbsp;</label>
                                             <label class="radio-inline ddx">
                                            In Stock <input type="radio" checked="checked" name="stock" id="stock" value="1"></label>
                                            
                                            Out of Stock <label class="radio-inline ddx"><input type="radio" name="stock" id="stock" value="0"></label>
                                          </div>
                                          <div class="col-md-6 ddc">
                                             <label>Product Stock</label>
                                             <input type="text" name="product_stock" id="product_stock"   placeholder="Enter Product Stock" class="form-control">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Meta Title</label>
                                             <input type="text" name="meta_title" placeholder="Title">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Meta Keyword</label>
                                             <input type="text" name="meta_keywords" placeholder="Keyword">
                                          </div>
                                          <div class="col-md-6">
                                             <label>Meta description</label>
                                             <textarea type="text" name="meta_description" placeholder="description"></textarea>
                                          </div>
                                          <!-- <div class="col-md-12">
                                             <label><strong> Status:</strong></label>
                                             <label>Published</label>
                                             <input type="radio" name="status" value="1" checked>
                                             <label>draft</label>
                                             <input type="radio" name="status" value="0">
                                          </div> -->
                                          <div class="col-md-12">
                                             <label><strong> New Arrivals:</strong></label>
                                             <label>Yes</label>
                                             <input type="radio" name="newarrival" value="1"> 
                                             <label>No</label>
                                             <input type="radio" name="newarrival" value="0" checked>
                                          </div>
                                          <!-- <div class="col-md-12">
                                             <label><strong> Deal:</strong></label>
                                             <label>Yes</label>
                                             <input type="radio" name="deal" value="1"> 
                                             <label>No</label>
                                             <input type="radio" name="deal" value="0" checked>
                                          </div> -->
                                          <div class="col-md-12">
                                             <label><strong> Best Seller:</strong></label>
                                             <label>Yes</label>
                                             <input type="radio" name="bestseller" value="1"> 
                                             <label>No</label>
                                             <input type="radio" name="bestseller" value="0" checked>
                                          </div>
                                          
                                          <div class="col-md-12">
                                             <div class="buttonz">
                                                <button type="submit" name="productAdd">save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <?php include 'includes/footer.php'; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script src="js/ckeditor/ckeditor.js"></script>
      <script>
         CKEDITOR.replace('editor1');
         CKEDITOR.replace('editor2');
      </script>
      <?php include 'includes/footerJs.php'; ?>
      <?php include 'includes/banner_modal.php'; ?>
      <script>
         $(document).ready(function() {
             $("#category").change(function() {
                 var id = $(this).val();
                 var dataString = 'id=' + id;
                 $.ajax({
                     type: "POST",
                     url: "ajax_subcategory.php",
                     data: dataString,
                     cache: false,
                     success: function(html)
                     {
                       $("#subcategory").html(html);
                     }
                 });
             });
             $("#subcategory").change(function() {
                 var id = $(this).val();
                 var dataString = 'id=' + id;
                 $.ajax({
                     type: "POST",
                     url: "ajax_innercategory.php",
                     data: dataString,
                     cache: false,
                     success: function(html)
                     {
                       $("#innercategory").html(html);
                     }
                 });
             });
             $("#pmaterial").click(function() {
                 $(".pmaterial").toggle();
             });
             $("#pcertificate").click(function() {
                 $(".pcertificate").toggle();
             });
             $("#pringsize").click(function() {
                 $(".pringsize").toggle();
             });
             $("#pcolor").click(function() {
                 $(".pcolor").toggle();
             });
             $(".ddx").click(function() {
                 var chkvl = $(this).find("input").val();
                 if (chkvl == 1) {
                     $(".ddc").css('display', 'block');
                 } else {
         
                     $(".ddc").css('display', 'none');
                 }
         
             });
         });
         
         var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];    
        function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                    
                    if (!blnValid) {
                        alert("Sorry, File is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
      </script>
   </body>
</html>
