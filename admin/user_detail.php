<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>User Detail</title>
<?php include 'includes/css.php'; ?>
</head>
<body>
    <div id="loader-wrapper"> 
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
<div class="panel-layout">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="main-page">
            <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                        <?php include 'includes/head.php'; ?>
          <div class="main-content">
            
                            <!-- responsive header -->
            <div class="panel-body">
              
              <div class="content-area">
                <div class="sub-bar">
                  <div class="sub-title">
                    <h4>User Detail</h4>
                  </div>
                  <ul class="bread-crumb">
                    <li><a href="#" title="">Home</a></li>
                    <li>Dashbord</li>
                  </ul>
                </div>
                <div class="invoice-pad">
                  <div class="gap no-gap">
					  <div class="row">
						  <div class="col-lg-6 col-sm-6">
							  <div class="invoice-info"> 
                               <?php 
                                $user_id = base64_decode($_REQUEST['id']);
                                $user = mysqli_query($con, "select *from tbl_users where user_id='$user_id'") or die(mysqli_error());
                                $user_data = mysqli_fetch_array($user);
                                ?>  
                                  <h3>Subject: <span>Detail for customer <b><?php echo $user_data['fname'].' '.$user_data['lname'];?></b> </span></h3>
								<h4>Billing Information</h4>
								<ul class="some-about">
                                  <li><span>Phone:</span><?php echo $user_data['phone'];?></li>
								  <li><span>City</span><?php echo $user_data['city'];?></li>
								  <li><span>State:</span><?php echo $user_data['state'];?></li>
                                  <li><span>Country:</span><?php echo $user_data['country'];?></li>
                                  <li><span>Zip:</span><?php echo $user_data['zip'];?></li>
								  <li><span>Address:</span><?php echo $user_data['address'];?></li>
								</ul>
							  </div>
						</div>
						<div class="col-lg-6 col-sm-6">
						  <div class="invoice-detail">
							<h5>Shipping Address</h5>
							<ul class="some-about">
								  <li><span>City</span><?php echo $user_data['s_city'];?></li>
								  <li><span>State:</span><?php echo $user_data['s_state'];?></li>
                                  <li><span>Country:</span><?php echo $user_data['s_country'];?></li>
                                  <li><span>Zip:</span><?php echo $user_data['s_zip'];?></li>
								  <li><span>Address:</span><?php echo $user_data['s_address'];?></li>
							</ul>
						  </div>
						</div>
					  </div>	  
                  </div>
                  
					
                </div>
              </div>
              <?php include 'includes/footer.php'; ?>
              <!-- bottombar --> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>


<?php include 'includes/footerJs.php'; ?>
</body>
</html>
