<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin panel home</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['id'];
                  $status = $_REQUEST['status'];
                  if($status == 'delete'){
                    $row = mysqli_query($con, "delete from tbl_size where size_id='$id'") or die(mysqli_error());
                    if($row){
                      echo "
                        <div class='alert alert-success brd-5 text-center'>
                          <p>Size deleted successfully</p>
                        </div>
                      ";
                    }
                  }else{
                    $row = mysqli_query($con, "update tbl_size set status='$status' where size_id='$id'") or die(mysqli_error());
                    if($row){
                      echo "
                        <div class='alert alert-success brd-5 text-center'>
                          <p>Size updated successfully</p>
                        </div>
                      ";
                    }
                  }
                }
                ?>
                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Size List</h4>
                        <a class="btn-st" style="float: right;"  href="#addnew" data-toggle="modal" id="addTours" title="">Add New</a>
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Size</em></th>
                            <th><em>Status</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $i = 1;
                            $rw = mysqli_query($con,"select *from tbl_size") or die(mysqli_error());
                            while($row = mysqli_fetch_array($rw)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td style="width:40%"><i><?php echo $row['size_name']; ?></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" style="display:none" value="<?php echo $row['size_name'];?>" id="txt_<?php echo $row['size_id'];?>">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" style="display:none" id="update-<?php echo $row['size_id'];?>" class="update-size"><img src="images/success-icon.png"></a>
                            </td>
                             <td><i>
                          <?php if($row['status']=='0'){ ?>
                             <a href="manage_size.php?id=<?php echo $row['size_id']; ?>&status=1" class='btn btn-success'>Active</a>
                           <?php } else{?>
                            <a href="manage_size.php?id=<?php echo $row['size_id']; ?>&status=0" class='btn btn-danger'>Inactive</a>
                           <?php }?></i>
                        </td>
                            <td><i><a href="manage_size.php?id=<?php echo $row['size_id']; ?>&status=delete" onclick="return confirm('Are you sure to delete?')"> <i class="icon-trash"></i></a>
                                <a href="#" class="edit-size" id="<?php echo $row['size_id'];?>"><i class="icon-pencil"></i></a></i>
                            </td>
                          </tr>
                         <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>
<?php include 'includes/size_modal.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
  $(".edit-size").click(function(e){
    e.preventDefault();
    var id = $(this).attr("id");
    $("#txt_" + id).show();
    $("#update-" + id).show();
  });

  $(".update-size").click(function(e){
    e.preventDefault();
    var res = $(this).attr("id");
    var a = res.split("-");
    var id = a[1];
    var size_name = $("#txt_" + id).val();
    if(size_name == ""){
      $("#txt_" + id).css("border-color","red");
    }else{
    $.ajax({
    type: 'POST',
    url: 'ajax_table.php',
    data: {tag:'update-size', id:id, size_name:size_name},
    //dataType: 'json',
    cache: false,
    success: function(response){
        window.location.href="manage_size.php";
    }
  });
}
  });

});
</script>

</body>
</html>