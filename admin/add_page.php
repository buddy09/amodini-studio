<?php
   include 'includes/session.php';
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Add new page
      </title>
      <?php include 'includes/css.php'; ?>
      <style type="text/css">
         .attributsc {
         padding: 6px;
         background: #1DE1C1;
         margin-right: 4px;
         color: #ffffff !important;
         font-weight: bold;
         border-radius: 7px;
         cursor: pointer;
         }
      </style>
   </head>
   <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
         <div id="loader">
         </div>
         <div class="loader-section section-left">
         </div>
         <div class="loader-section section-right">
         </div>
      </div>
      <div class="panel-layout">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="main-page">
                     <?php include 'includes/sidebar.php'; ?>
                     <!-- side header -->
                     <?php include 'includes/head.php'; ?>
                     <div class="main-content">
                        <div class="panel-body">
                           <div class="content-area">
                              <?php
                                 if(isset($_SESSION['error'])){
                                 echo "
                                 <div class='alert alert-danger brd-5 text-center'>
                                 <p>".$_SESSION['error']."</p> 
                                 </div>
                                 ";
                                 unset($_SESSION['error']);
                                 }
                                 if(isset($_SESSION['success'])){
                                 echo "
                                 <div class='alert alert-success brd-5 text-center'>
                                 <p>".$_SESSION['success']."</p>
                                 </div>
                                 ";
                                 unset($_SESSION['success']);
                                 }
                                 ?>
                              <div class="gap no-gap">
                                 <div class="inner-bg">
                                    <div class="element-title">
                                       <h4>Add New Page</h4>
                                    </div>
                                    <form action="save_page.php" method="POST" enctype="multipart/form-data">
                                    <div class="add-prod-from">
                                       <div class="row">
                                          <div class="col-md-6">
                                             <label>Page Name</label>
                                             <input type="text" name="page_name" id="page_name"
                                                parsley-trigger="change" required
                                                placeholder="Enter page name" class="form-control">
                                          </div>
                                         
                                         
                                          <div class="col-md-12">
                                             <label>Page Description</label>
                                             <textarea cols="30" rows="10"
                                                placeholder="Page Description" name="page_description" id="editor1" required></textarea>
                                          </div>
                                      
                                          <div class="col-md-12">
                                             <div class="buttonz">
                                                <button type="submit" name="pageAdd">save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <?php include 'includes/footer.php'; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <script src="js/ckeditor/ckeditor.js"></script>
      <script>
         CKEDITOR.replace('editor1');
      </script>
      <?php include 'includes/footerJs.php'; ?>
      <?php include 'includes/banner_modal.php'; ?>
      
   </body>
</html>
