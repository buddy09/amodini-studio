
<?php
	include 'includes/session.php';
	include 'includes/slugify.php';
	if(isset($_POST['pageAdd'])){
		$page_name = $_POST['page_name'];
        $slug = slugify($page_name);
        $description = $_POST['page_description'];
		$stmt = mysqli_query($con,"select count(*) as numrows from tbl_pages where slug='$slug'") or die(mysqli_error());
		$row = mysqli_fetch_array($stmt);
		if($row['numrows'] > 0){
			$_SESSION['error'] = 'Page already exist';
		}
		else{
			$stmt = mysqli_query($con,"insert into tbl_pages(page_name,description,slug) values('$page_name','$description','$slug')") or die(mysqli_error());
			$_SESSION['success'] = 'Page added successfully';
		}
	}
	else{
		$_SESSION['error'] = 'Fill up page form first';
	}
	header('location: manage_pages.php');
?>