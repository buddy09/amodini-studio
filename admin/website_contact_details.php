<?php
 include 'includes/session.php';

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Contact Details</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['id'];
                  $status = $_REQUEST['status'];
                  $row = mysqli_query($con, "update banner set status='$status' where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Status updated successfully</p>
                      </div>
                    ";
                  }
                }
                if(isset($_GET['del']))
                {
                $id=$_GET['del'];
                $query=mysqli_Query($con,"DELETE  FROM banner WHERE id='$id'");
                echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Record Deleted Successfully</p>
                      </div>
                    ";
                }
                ?>
                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Website Contact Details</h4>
                        <a class="btn-st" style="float: right;"  href="#addnew" data-toggle="modal" id="addTours" title="">Add New</a>
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>Name</em></th>
                            <th><em>Logo</em></th>
                            <th><em>Emails</em></th>
                            <th><em>Phone No.</em></th>
                            <th><em>Address</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                          $query = mysqli_query($con,"SELECT * FROM `contact_us_details`");
                          while($row = mysqli_fetch_array($query)){
                      ?>
                          <tr>
                            <td><?php echo $row['name']; ?></td>
                            <td>
                              <img src="images/<?php echo $row['photo']; ?>" height='30px' width='30px'>
                              <span class='pull-right'><a href='#edit_photo' class='photo' data-toggle='modal' data-id="<?php echo $row['id']; ?>"><i class='fa fa-edit'></i></a></span>
                            </td>
                            <td><?php echo $row['email']; ?></td>
                            <td><?php echo $row['mobile']; ?></td>
                            <td><?php echo $row['address'].', '.$row['city'].', '.$row['state'].', '.$row['pin_code']; ?></td>
                            <td>
                              <button class='btn btn-success btn-sm edit btn-flat' data-id="<?php echo $row['id']; ?>"><i class='fa fa-edit'></i> Edit</button>
                            </td>
                          </tr>
                        <?php } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>
<?php include 'includes/contact_us_modal.php'; ?>
<script>
$(function(){
  $(document).on('click', '.edit', function(e){
    e.preventDefault();
    $('#edit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $(document).on('click', '.photo', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    getRow(id);
  });

  $("#edit").on("hidden.bs.modal", function () {
      $('.append_items').remove();
  });

});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'contact_us_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('.contactid').val(response.id);
      $('#edit_name').val(response.name);
      $('#edit_email').val(response.email);
      $('#edit_mobile').val(response.mobile);
      $('#edit_address').val(response.address);
      $('#edit_city').val(response.city);
      $('#edit_state').val(response.state);
      $('#edit_pin_code').val(response.pin_code);
    }
  });
}
</script>

</body>
</html>