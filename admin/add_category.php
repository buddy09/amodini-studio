
<?php
	include 'includes/session.php';
	include 'includes/slugify.php';
	if(isset($_POST['add'])){
		$category_name = $_POST['cat_name'];
		$slug = slugify($category_name);	
		$stmt = mysqli_query($con,"select count(*) as numrows from tbl_sub_category where slug='$slug'") or die(mysqli_error());
		$row = mysqli_fetch_array($stmt);
		if($row['numrows'] > 0){
			$_SESSION['error'] = 'Category already exist';
		}
		else{
			$stmt = mysqli_query($con,"insert into tbl_category(category_name,slug) values('$category_name','$slug')") or die(mysqli_error());
			$_SESSION['success'] = 'Category added successfully';
		}
	}
	else{
		$_SESSION['error'] = 'Fill up category form first';
	}
	header('location: manage_category.php');
?>