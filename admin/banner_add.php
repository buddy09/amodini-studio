<?php
   include 'includes/session.php';
   
   if(isset($_POST['banner_add'])){
   	$photo = $_FILES['photo']['name'];
   
   	if(!empty($photo))
   	{
   		$imageFileType = strtolower(pathinfo($photo,PATHINFO_EXTENSION));
   		$extensions_arr = array("jpg","jpeg","png","gif");
   		if( in_array($imageFileType,$extensions_arr) )
   		{
   			$uniq_photo = uniqid().'-'.time();
   			$ext = pathinfo($photo, PATHINFO_EXTENSION);
   			$image = $uniq_photo.'.'.$ext;
   			move_uploaded_file($_FILES['photo']['tmp_name'], 'images/banner/'.$image);
   		}
   	}
   	else
   	{
   		$image = '';
   	}
	$stmt = mysqli_query($con,"INSERT INTO `banner`(`photo`) VALUES ('$image')");
	$_SESSION['success'] = 'Banner added successfully'; 		
   	}
   else{
   	$_SESSION['error'] = 'Fill up blog form first';
   }
   
   header('location: banner.php');
   
   ?>