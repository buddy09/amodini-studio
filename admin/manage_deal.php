<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Panel - Manage Deal</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['id'];
                  $status = $_REQUEST['status'];
                  $row = mysqli_query($con, "update tbl_deal set status='$status' where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Deal updated successfully</p>
                      </div>
                    ";
                  }
                }
                ?>
                <?php
                if(isset($_REQUEST['delete'])){
                  $id = $_REQUEST['id'];
                  $row = mysqli_query($con, "delete from tbl_deal where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Deal deleted successfully</p>
                      </div>
                    ";
                  }
                }
                ?>
                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Deal List</h4>
                        <a class="btn-st" style="float: right;"  href="add_new_deal.php" title="">Add New</a>
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Images</em></th>
                            <th><em>Category</em></th>
                            <th><em>Sub Category</em></th>
                            <th><em>Inner Category</em></th>
                            <th><em>Status</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $i = 1;
                            $rw = mysqli_query($con,"select d.id,d.status,c.category_name,s.sub_category,i.inner_category from tbl_category c, tbl_sub_category s, tbl_inner_category i, tbl_deal d where c.id=d.cat_id and s.id=d.sub_cat_id and i.id=d.inner_cat_id") or die(mysqli_error());
                            while($row = mysqli_fetch_array($rw)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td>
                            <?php
                            $deal_id = $row['id'] ;
                            $r = mysqli_query($con, "select *from tbl_deal_images where deal_id='$deal_id'") or die(mysqli_error());
                            while($d = mysqli_fetch_array($r)){
                            ?>
                            <img src="uploads/productImages/<?php echo $d['image'];?>" style="width:70px">
                            <a href="#" class="edit-deal-image" id="<?php echo $d['id'];?>"><i class="icon-pencil"></i></a></i>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <?php }?>
                            </td>
                            <td><?php echo $row['category_name']?></td>
                            <td><?php echo $row['sub_category']?></td>
                            <td><?php echo $row['inner_category']?></td>
                            <td><i>
                          <?php if($row['status']=='0'){ ?>
                             <a href="manage_deal.php?id=<?php echo $row['id']; ?>&status=1" class='btn btn-success'>Active</a>
                           <?php } else{?>
                            <a href="manage_deal.php?id=<?php echo $row['id']; ?>&status=0" class='btn btn-danger'>Inactive</a>
                           <?php }?></i>
                        </td>
                            <td><i><a href="manage_deal.php?id=<?php echo $row['id'];?>&delete=yes" onclick="return confirm('Are you sure to delete?')"> <i class="icon-trash"></i></a>
                                <a href="edit_deal.php?id=<?php echo $row['id'];?>"><i class="icon-pencil"></i></a></i>
                            </td>
                          </tr>
                         <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>

<script>
$(document).ready(function(){
  $(".edit-deal-image").click(function(e){
    e.preventDefault();
    var id = $(this).attr("id");
    $("#imgid").val(id);
    $('#edit_deal_image').modal('show');
  });
});
</script>
<?php include 'includes/deal_modal.php'; ?>

</body>
</html>