  <!-- Add -->
<div class="modal fade" id="edit">
    <div class="modal-dialog model-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Edit Contact Details</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="add_category.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                        <div class="col-md-6">
                         <label>Name</label>
                         <input type="text" class="form-control" id="edit_name" name="name" required>
                         </div>
                         <div class="col-md-6">
                         <label>Email</label>
                         <input type="email" class="form-control" id="edit_email" name="email" required>
                         </div>
                         <div class="col-md-6">
                         <label>Mobile Number</label>
                         <input type="text" class="form-control" id="edit_mobile" name="mobile" required>
                         </div>
                         <div class="col-md-6">
                         <label>City</label>
                         <input type="text" class="form-control" id="edit_city" name="city" required>
                         </div>
                         <div class="col-md-6">
                         <label>Address</label>
                         <textarea class="form-control" id="edit_address" name="address" required></textarea>
                         </div>
                         <div class="col-md-6">
                         <label>State</label>
                         <input type="text" class="form-control" id="edit_state" name="state" required>
                         </div>
                         <div class="col-md-6">
                         <label>Pin Code</label>
                         <input type="text" class="form-control" id="edit_pin_code" name="pin_code" required>
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
                <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="edit"><i class="fa fa-save"></i> Update</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>

  <!-- Edit -->
<div class="modal fade" id="edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Edit Category</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="category_edit.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                    <div class="col-md-12">
                         <label>Brand Select</label>
                         <input type="hidden" name="cat_id" class="cat_id">
                           <select class="form-control" id="brand_id" name="brand_id" required>
                            <option value="" selected id="bselected"></option>
                          </select>
                         </div>
                       <div class="col-md-12">
                         <label>Category Name</label>
                          <input type="text" name="cat_name" id="edit_cat_name" placeholder="Category Name">
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="edit"><i class="fa fa-save"></i> Update</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Deleting...</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="category_delete.php">
                <input type="hidden" class="cat_id" name="cat_id">
                <div class="text-center">
                    <p>DELETE CATEGORY</p>
                    <h2 class="bold brand_name"></h2>
                </div>
            <br>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button"  data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit"  name="delete"><i class="fa fa-trash"></i> Delete</button>
            </div>
          </div>
              </form>
               </div>
            </div>
        </div>
    </div>
