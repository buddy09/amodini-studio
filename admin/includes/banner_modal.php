  <!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Add New Banner</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="banner_add.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                        <div class="col-md-12">
                         <label>Photo (1800 X 700)</label>
                          <input type="file" class="form-control" name="photo" required>
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
                <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="banner_add"><i class="fa fa-save"></i> Save</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>

  <!-- Edit -->
<div class="modal fade" id="edit_photo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Edit Banner</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="banner_photo.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                  
                       <div class="col-md-12">
                         <label>Photo (1800 X 700)</label>
                         <input type="hidden" name="bid" class="bannerid">
                          <input type="file" class="form-control" required name="photo" id="photo">
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="upload"><i class="fa fa-save"></i> Update</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Deleting...</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="category_delete.php">
                <input type="hidden" class="cat_id" name="cat_id">
                <div class="text-center">
                    <p>DELETE CATEGORY</p>
                    <h2 class="bold brand_name"></h2>
                </div>
            <br>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button"  data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit"  name="delete"><i class="fa fa-trash"></i> Delete</button>
            </div>
          </div>
              </form>
               </div>
            </div>
        </div>
    </div>
