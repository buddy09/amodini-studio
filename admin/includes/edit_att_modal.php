<div class="modal fade" id="edit_att">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Edit</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="update-att.php" enctype="multipart/form-data">
              <input type="hidden" id="att_aid" name="att_aid">
              <input type="hidden" id="ppid" name="ppid">
               <div class="add-prod-from">
                  <div class="row">
                  
                        <div class="col-md-12">
                         <label>Size</label>
                         <select name="att_size" id="att_size" class="form-control">
                         <?php 
                         $s = mysqli_query($con, "select *from tbl_size") or die(mysqli_error());
                         while($sz = mysqli_fetch_array($s))
                         {
                         ?>
                         <option value="<?php echo $sz['size_id'];?>"><?php echo $sz['size_name'];?></option>
                         <?php }?>
                         </select>
                        </div>

                        <div class="col-md-12">
                         <label>Color</label><br>
                         <div class="row" id="att_color">
                         <?php 
                         $c = mysqli_query($con, "select *from tbl_color") or die(mysqli_error());
                         while($cl = mysqli_fetch_array($c))
                         {
                         ?>
                         <div class="col-md-3">
                         <input type="radio" name="att_color" value="<?php echo $cl['id']?>"><span style="background-color:<?php echo $cl['color_code'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                         </div>
                         <?php }?>
                         </div>
                        </div>

                        <div class="col-md-12">
                         <label>Price</label>
                         <input type="text" name="att_price" id="att_price" class="form-control">
                        </div>

                        <div class="col-md-12">
                         <label>Discount</label>
                         <input type="text" name="att_discount" id="att_discount" class="form-control">
                        </div>

                    </div>
                 </div>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="update-att"><i class="fa fa-save"></i> Update</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>
</div>
