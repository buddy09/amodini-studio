<header>
                            <div class="side-menus">
                                <div class="side-header">
                                    <div class="logo"><a title="" href="index.html"><img alt="" src="images/logo2.png"></a></div>
                                    <nav class="slide-menu">
                                        <span>Navigation <i class="ti-layout"></i></span>
                                        <ul class="parent-menu">
                                            <li> <a href="home.php"><i class="fa fa-home"></i><span>Dashboard</span></a>
                                            </li>
                                            <li> <a href="banner.php"><i class="fa fa-photo"></i><span>Banner</span></a>
                                            </li>
                                            <!-- <li> <a href="website_contact_details.php"><i class="fa fa-info-circle"></i><span>Website Contact Details</span></a> -->
                                            </li>
                                            <li class="menu-item-has-children"> <a title=""><i class="fa fa-tachometer"></i><span>Categories</span></a>
                                                <ul class="mega">
                                                    <li><a href="manage_category.php" title="">Manage Category</a></li>
                                                    <li><a href="manage_sub_category.php" title="">Manage Sub Category</a></li>
                                                    <li><a href="manage_inner_category.php" title="">Manage Inner Category</a></li>
                                                    <li><a href="manage_material.php" title="">Manage Material</a></li>
                                                    <li><a href="manage_size.php" title="">Manage Size</a></li>
                                                    <li><a href="manage_color.php" title="">Manage Color</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"> <a title=""><i class="fa fa-shopping-cart"></i><span>Manage Product</span></a>
                                                <ul class="mega">
                                                    <li><a href="manage_products.php" title="">Manage Products</a></li>
                                                    <li><a href="product_add.php">add product</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children"> <a title=""><i class="fa fa-shopping-cart"></i><span>Manage Contact Detail</span></a>
                                                <ul class="mega">
                                                    <li><a href="manage_contact_detail.php" title="">Manage Contact</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="manage_deal.php"><i class="fa fa-photo"></i><span>Manage Deal</span></a></li>
                                            <li><a href="manage_pages.php"><i class="fa fa-photo"></i><span>Manage Pages</span></a></li>
                                            <li><a href="manage_users.php"><i class="fa fa-photo"></i><span>Manage Users</span></a></li>
                                            <li><a href="order_management.php"><i class="fa fa-photo"></i><span>Order Management</span></a></li>
                                            <li><a href="delivery_management.php"><i class="fa fa-photo"></i><span>Delivery Management</span></a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </header>