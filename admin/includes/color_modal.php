  <!-- Add -->
<div class="modal fade" id="addnew">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Add New Color</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="add_color.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                        <div class="col-md-12">
                         <label>Choose color</label>
                          <input type="color" name="color_code" placeholder="Color Code" value="#FF5733">
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
                <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="add"><i class="fa fa-save"></i> Save</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>

  <!-- Edit -->
<div class="modal fade" id="edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Edit Category</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="category_edit.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                    <div class="col-md-12">
                         <label>Brand Select</label>
                         <input type="hidden" name="cat_id" class="cat_id">
                           <select class="form-control" id="brand_id" name="brand_id" required>
                            <option value="" selected id="bselected"></option>
                          </select>
                         </div>
                       <div class="col-md-12">
                         <label>Category Name</label>
                          <input type="text" name="cat_name" id="edit_cat_name" placeholder="Category Name">
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="edit"><i class="fa fa-save"></i> Update</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>

<!-- Delete -->
<div class="modal fade" id="delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Deleting...</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="category_delete.php">
                <input type="hidden" class="cat_id" name="cat_id">
                <div class="text-center">
                    <p>DELETE CATEGORY</p>
                    <h2 class="bold brand_name"></h2>
                </div>
            <br>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button"  data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit"  name="delete"><i class="fa fa-trash"></i> Delete</button>
            </div>
          </div>
              </form>
               </div>
            </div>
        </div>
    </div>
