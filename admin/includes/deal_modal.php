<div class="modal fade" id="edit_deal_image">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><b>Edit</b></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="update-deal-image.php" enctype="multipart/form-data">
               <div class="add-prod-from">
                  <div class="row">
                  
                       <div class="col-md-12">
                         <label>Image ( 772 X 960 )</label>
                         <input type="hidden" name="imgid" id="imgid">
                          <input type="file" class="form-control" required name="deal_image" id="deal_image">
                         </div>
                    </div>
                 </div>
            <div class="modal-footer">
              <div class="buttonz">
              <button type="button" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" name="update-deal-image"><i class="fa fa-save"></i> Update</button>
              </div>
            </div>
              </form>
               </div>
            </div>
      </div>
    </div>
</div>
