<?php 
include 'includes/session.php';
include 'includes/slugify.php';
$tag = $_POST['tag'];
if($tag == 'update-category'){
    $id = $_POST['id'];
    $category_name = $_POST['category'];
	$slug = slugify($category_name);	
	$stmt = mysqli_query($con,"select count(*) as numrows from tbl_category where slug='$slug'") or die(mysqli_error());
	$row = mysqli_fetch_array($stmt);
	if($row['numrows'] > 0){
        $_SESSION['error'] = 'Category already exist';
        echo 'success';
	}
	else{
		$stmt = mysqli_query($con,"update tbl_category set category_name='$category_name', slug='$slug' where id='$id'") or die(mysqli_error());
        $_SESSION['success'] = 'Category updated successfully';
        echo 'success';
	}
}

else if($tag == 'update-sub-category'){
    $id = $_POST['id'];
    $sub_category = $_POST['sub_category'];
	$slug = slugify($sub_category);	
	$stmt = mysqli_query($con,"select count(*) as numrows from tbl_sub_category where slug='$slug'") or die(mysqli_error());
	$row = mysqli_fetch_array($stmt);
	if($row['numrows'] > 0){
        $_SESSION['error'] = 'Sub category already exist';
        echo 'success';
	}
	else{
		$stmt = mysqli_query($con,"update tbl_sub_category set sub_category='$sub_category', slug='$slug' where id='$id'") or die(mysqli_error());
        $_SESSION['success'] = 'Sub category updated successfully';
        echo 'success';
	}
}

else if($tag == 'update-inner-category'){
    $id = $_POST['id'];
    $inner_category = $_POST['inner_category'];
	$slug = slugify($inner_category);
	$stmt = mysqli_query($con,"select count(*) as numrows from tbl_inner_category where slug='$slug'") or die(mysqli_error());
	$row = mysqli_fetch_array($stmt);
	if($row['numrows'] > 0){
        $_SESSION['error'] = 'Inner category already exist';
        echo 'success';
	}
	else{
		$stmt = mysqli_query($con,"update tbl_inner_category set inner_category='$inner_category', slug='$slug' where id='$id'") or die(mysqli_error());
        $_SESSION['success'] = 'Inner category updated successfully';
        echo 'success';
	}
}

else if($tag == 'update-material'){
    $id = $_POST['id'];
    $material_name = $_POST['material_name'];
	$stmt = mysqli_query($con,"select count(*) as numrows from tbl_materials where material_name='$material_name'") or die(mysqli_error());
	$row = mysqli_fetch_array($stmt);
	if($row['numrows'] > 0){
        $_SESSION['error'] = 'Material already exist';
        echo 'success';
	}
	else{
		$stmt = mysqli_query($con,"update tbl_materials set material_name='$material_name' where mid='$id'") or die(mysqli_error());
        $_SESSION['success'] = 'Material updated successfully';
        echo 'success';
	}
}

else if($tag == 'update-size'){
    $id = $_POST['id'];
    $size_name = $_POST['size_name'];
	$stmt = mysqli_query($con,"select count(*) as numrows from tbl_size where size_name='$size_name'") or die(mysqli_error());
	$row = mysqli_fetch_array($stmt);
	if($row['numrows'] > 0){
        $_SESSION['error'] = 'Size already exist';
        echo 'success';
	}
	else{
		$stmt = mysqli_query($con,"update tbl_size set size_name='$size_name' where size_id='$id'") or die(mysqli_error());
        $_SESSION['success'] = 'Size updated successfully';
        echo 'success';
	}
}

else if($tag == 'update-color-code'){
    $id = $_POST['id'];
    $color_code = $_POST['color_code'];
	$stmt = mysqli_query($con,"select count(*) as numrows from tbl_color where color_code='$color_code'") or die(mysqli_error());
	$row = mysqli_fetch_array($stmt);
	if($row['numrows'] > 0){
        $_SESSION['error'] = 'Color already exist';
        echo 'success';
	}
	else{
		$stmt = mysqli_query($con,"update tbl_color set color_code='$color_code' where id='$id'") or die(mysqli_error());
        $_SESSION['success'] = 'Color updated successfully';
        echo 'success';
	}
}

else if($tag == 'update-delivery-status'){
	$order_id = $_POST['order_id'];
	$status = $_POST['status'];
	$query = mysqli_query($con, "update tbl_checkout set delivery_status='$status' where order_id='$order_id'") or die(mysqli_error());
	if($query){
		$_SESSION['success'] = 'Delivery status updated successfully';
		echo 1;
	}else{
		$_SESSION['error'] = 'Error while updating delivery status';
		echo 0;
	}
}

else{

}
?>