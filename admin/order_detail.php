<?php include 'includes/session.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>Order Detail</title>
<?php include 'includes/css.php'; ?>
</head>
<body>
    <div id="loader-wrapper"> 
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
<div class="panel-layout">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="main-page">
            <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                        <?php include 'includes/head.php'; ?>
          <div class="main-content">
            <div class="responsive-header">
                                <div class="logo-area">
                                    <ul class="notify-area">
                                        <li>
                                            <div class="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div>
                                        </li>
                                        <li class="notifications"><a href="#" title=""><i class="fa fa-bell-o"></i></a><span class="red-bg">02</span>
                                            <div class="drop notify"> <span class="drop-head">Notifications</span>
                                                <ul class="drop-meta">
                                                    <li> <i class="notifi-icon blue">N</i>
                                                        <div class="notifi-meta">
                                                            <h4><a href="#" title="">Nulla Vel Metus Scelerisque Ante Commodo. </a></h4>
                                                            <span>02:34PM</span> </div>
                                                    </li>
                                                    <li> <i class="notifi-icon red">C</i>
                                                        <div class="notifi-meta">
                                                            <h4><a href="#" title="">Nulla Vel Metus Scelerisque Ante Commodo. </a></h4>
                                                            <span>02:34PM</span> </div>
                                                    </li>
                                                    <li> <i class="notifi-icon yellow">A</i>
                                                        <div class="notifi-meta">
                                                            <h4><a href="#" title="">Nulla Vel Metus Scelerisque Ante Commodo. </a></h4>
                                                            <span>02:34PM</span> </div>
                                                    </li>
                                                    <li> <i class="notifi-icon blue">N</i>
                                                        <div class="notifi-meta">
                                                            <h4><a href="#" title="">Nulla Vel Metus Scelerisque Ante Commodo. </a></h4>
                                                            <span>02:34PM</span> </div>
                                                    </li>
                                                </ul>
                                                <span class="drop-bottom"><a href="#" title="">View More Notifications</a></span> </div>
                                        </li>
                                        <li class="messages"><a href="#" title=""><i class="fa fa-envelope-o"></i></a><span class="blue-bg">10</span>
                                            <div class="drop messages"> <span class="drop-head">3 New Message <i class="fa fa-pencil-square-o"></i></span>
                                                <ul class="drop-meta">
                                                    <li> <i class="notifi-icon"><img src="images/resources/user-mesg.jpg" alt=""></i>
                                                        <div class="notifi-meta"> <span>02:34PM</span>
                                                            <h4><a href="#" title="">Hi Teddy, Just wanted to let you...</a></h4>
                                                        </div>
                                                    </li>
                                                    <li> <i class="notifi-icon"><img src="images/resources/user-mesg2.jpg" alt=""></i>
                                                        <div class="notifi-meta"> <span>02:34PM</span>
                                                            <h4><a href="#" title="">Hi Teddy, Just wanted to let you...</a></h4>
                                                        </div>
                                                    </li>
                                                    <li> <i class="notifi-icon"><img src="images/resources/user-mesg3.jpg" alt=""></i>
                                                        <div class="notifi-meta"> <span>02:34PM</span>
                                                            <h4><a href="#" title="">Hi Teddy, Just wanted to let you...</a></h4>
                                                        </div>
                                                    </li>
                                                    <li> <i class="notifi-icon"><img src="images/resources/user-mesg.jpg" alt=""></i>
                                                        <div class="notifi-meta"> <span>02:34PM</span>
                                                            <h4><a href="#" title="">Hi Teddy, Just wanted to let you...</a></h4>
                                                        </div>
                                                    </li>
                                                    <li> <i class="notifi-icon"><img src="images/resources/user-mesg2.jpg" alt=""></i>
                                                        <div class="notifi-meta"> <span>02:34PM</span>
                                                            <h4><a href="#" title="">Hi Teddy, Just wanted to let you...</a></h4>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <span class="drop-bottom"><a href="#" title="">View More messages</a></span> </div>
                                        </li>
                                    </ul>
                                    
                                    <div class="user-head">
                                    <div class="admin">
                                        <div class="admin-avatar"> <img src="images/resources/admin.png" alt=""> <i class="online"></i> </div>
                                    </div>
                                    <div class="drop setting"> <span class="drop-head">stifen Doe <i>30 days trial</i></span>
                                        <ul class="drop-meta">
                                            <li> <a href="#" title=""><i class="fa fa-eyedropper"></i>Edit Profile</a> </li>
                                            <li> <a href="#" title=""><i class="fa fa-envelope-o"></i>My Inbox</a> </li>
                                            <li> <a href="#" title=""><i class="fa fa-adjust"></i>task</a> </li>
                                            <li> <a href="#" title=""><i class="fa fa-calendar"></i>Calender</a> </li>
                                            <li> <a href="#" title=""><i class="fa fa-align-right"></i>Balance Report</a> </li>
                                        </ul>
                                        <span class="drop-bottom"><a href="#" title=""><i class="fa fa-sign-out"></i>log Out</a></span> </div>
                                    </div>
                                    <ul class="seting-area">
                                    <li class="langages">
                                        <a title="" href="#">Eng</a>
                                        <ul class="drop language">
                                            <li class="lang-selected"><a href="#"><i class="fa fa-check"></i> Eng</a></li>
                                            <li><a href="#">Rus</a></li>
                                            <li><a href="#">Jap</a></li>
                                            <li><a href="#">Arb</a></li>
                                        </ul>
                                    </li>
                                    <li class="setting-panel"><a title="" href="#"><i class="icon-equalizer"></i></a></li>
                                </ul>
                                </div>
                                <div class="t-search">
                                    <form method="post">
                                        <input type="text" placeholder="Enter Your Keyword">
                                        <button type="submit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                            <!-- responsive header -->
            <div class="panel-body">
              
              <div class="content-area">
                <div class="sub-bar">
                  <div class="sub-title">
                    <h4>Order Detail</h4>
                  </div>
                  <ul class="bread-crumb">
                    <li><a href="#" title="">Home</a></li>
                    <li>Dashbord</li>
                  </ul>
                </div>
                <div class="invoice-pad">
                  <div class="gap no-gap">
					  <div class="row">
						  <div class="col-lg-6 col-sm-6">
							  <div class="invoice-info"> 
                               <?php 
                                $user_name = base64_decode($_REQUEST['user']);
                                $user = mysqli_query($con, "select *from tbl_users where email='$user_name'") or die(mysqli_error());
                                $user_data = mysqli_fetch_array($user);
                                ?>  
                                  <h3>Subject: <span>Order Detail for customer <b><?php echo $user_data['fname'].' '.$user_data['lname'];?> </span></b></h3>
								<h4>Billing Information</h4>
								<ul class="some-about">
                                  <li><span>Phone:</span><?php echo $user_data['phone'];?></li>
								  <li><span>City</span><?php echo $user_data['city'];?></li>
								  <li><span>State:</span><?php echo $user_data['state'];?></li>
                                  <li><span>Country:</span><?php echo $user_data['country'];?></li>
                                  <li><span>Zip:</span><?php echo $user_data['zip'];?></li>
								  <li><span>Address:</span><?php echo $user_data['address'];?></li>
								</ul>
							  </div>
						</div>
						<div class="col-lg-6 col-sm-6">
						  <div class="invoice-detail">
							<h5>Shipping Address</h5>
							<ul class="some-about">
								  <li><span>City</span><?php echo $user_data['s_city'];?></li>
								  <li><span>State:</span><?php echo $user_data['s_state'];?></li>
                                  <li><span>Country:</span><?php echo $user_data['s_country'];?></li>
                                  <li><span>Zip:</span><?php echo $user_data['s_zip'];?></li>
								  <li><span>Address:</span><?php echo $user_data['s_address'];?></li>
							</ul>
						  </div>
						</div>
					  </div>	  
                  </div>
                  <div class="gap no-top">
                    <div class="invoice-table">
                      <div class="widget">
                        <table class="prj-tbl striped bordered table-responsive">
                          <thead class="drk">
                            <tr>
                              <th><em>No.</em></th>
                              <th><em>Product Name</em></th>
                              <th><em>Image</em></th>
                              <th><em>Color</em></th>
                              <th><em>Size</em></th>
                              <th><em>Price</em></th>
                              <th><em>Quantity</em></th>
                              <th><em>Total</em></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                            $i = 1;
                            $total_price = 0;
                            $uid = $_REQUEST['id'];
                            $row = mysqli_query($con, "select c.price,c.quantity,(c.price * c.quantity) as total_price,c.size,c.color,p.product_id,p.product_name,p.product_photo from tbl_cart c, tbl_products p where c.product_id=p.product_id and c.uid='$uid'") or die (mysqli_error());
                            while($data = mysqli_fetch_array($row))
                            {
                                $total_price = $total_price + $data['total_price'];
                            ?>  
                            <tr>
                              <td><span><?php echo $i;?></span></td>
                              <td><i><?php echo $data['product_name'];?></i></td>
                              <td><img style="width:100px" src="uploads/productImages/<?php echo $data['product_photo'];?>"></td>
                              <td><span style="background-color:<?php echo $data['color'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                              <td><i><?php echo $data['size'];?></i></td>
                              <td><i>&#8377;<?php echo $data['price'];?></i></td>
                              <td><i><?php echo $data['quantity'];?></i></td>
                              <td><i>&#8377;<?php echo $data['total_price'];?></i></td>
                            </tr>
                            <?php $i++;}?>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
					<div class="gap no-gap">
					<div class="row">
                  <div class="col-md-6 col-sm-6">
                    <ul class="some-about">
                      <li class="total-amount"><span>Total:</span><i>&#8377;<?php echo $total_price;?></i></li>
                    </ul>
                  </div>
                  <div class="col-md-6 col-sm-6">
                    <!-- <div class="proced-btns"> <a class="btn-st " href="#" title="">print invoice</a> <a class="btn-st drk-clr" href="#" title="">cancel invoice</a> </div> -->
                  </div>
					</div>	
					</div>	
                </div>
              </div>
              <?php include 'includes/footer.php'; ?>
              <!-- bottombar --> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</div>


<?php include 'includes/footerJs.php'; ?>
</body>
</html>
