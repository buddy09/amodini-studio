<?php
   include 'includes/session.php';
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Add Attributes</title>
      <?php include 'includes/css.php'; ?>
      <style type="text/css">
         .attributsc {
         padding: 6px;
         background: #1DE1C1;
         margin-right: 4px;
         color: #ffffff !important;
         font-weight: bold;
         border-radius: 7px;
         cursor: pointer;
         }
      </style>
   </head>
   <body>
      <!-- Start Page Loading -->
      <div id="loader-wrapper">
         <div id="loader">
         </div>
         <div class="loader-section section-left">
         </div>
         <div class="loader-section section-right">
         </div>
      </div>
      <div class="panel-layout">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-12">
                  <div class="main-page">
                     <?php include 'includes/sidebar.php'; ?>
                     <!-- side header -->
                     <?php include 'includes/head.php'; ?>
                     <div class="main-content">
                        <div class="panel-body">
                           <div class="content-area">
                           <?php
                           if(isset($_SESSION['error'])){
                           echo "
                           <div class='alert alert-danger brd-5 text-center'>
                           <p>".$_SESSION['error']."</p> 
                           </div>
                           ";
                           unset($_SESSION['error']);
                           }
                           if(isset($_SESSION['success'])){
                           echo "
                           <div class='alert alert-success brd-5 text-center'>
                           <p>".$_SESSION['success']."</p>
                           </div>
                           ";
                           unset($_SESSION['success']);
                           }
                           ?>
                              <a href="manage_attributes.php?id=<?php echo $_REQUEST['id'];?>" title="" class="btn-st black">Go Back</a>
                              <div class="gap no-gap">
                                 <div class="inner-bg">
                                    <div class="element-title">
                                       <h4>Add New Attributes</h4>
                                    </div>
                                    <form action="save_attributes.php" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="hdnid" value="<?php echo $_REQUEST['id'];?>">
                                    <div class="add-prod-from">
                                       <div class="row">
                                          <div class="col-md-6">
                                             <label>Size</label>
                                             <select name="size" id="size" class="form-control" style="width:50%" required>
                                             <option value="">-- Select</option>
                                             <?php
                                             $size = mysqli_query($con, "select *from tbl_size where status='1'") or die(mysqli_error());
                                             while($sz = mysqli_fetch_array($size))
                                             {
                                             ?>
                                             <option value="<?php echo $sz['size_id'];?>"><?php echo $sz['size_name'];?></option>
                                             <?php }?>
                                             </select>
                                          </div>

                                          <div class="col-md-6">Image Size ( 772 X 960 )</div>

                                          <div class="col-md-12">
                                          <?php 
                                          $color = mysqli_query($con, "select *from tbl_color where status=1") or die(mysqli_error());
                                          while($clr = mysqli_fetch_array($color))
                                          {
                                          ?>
                                          <div class="row" style="padding-bottom: 15px;"><div class="col-md-3"><input class="color" type="checkbox" id="chk-<?php echo $clr['id'];?>" name="color[]" value="<?php echo $clr['id']?>">&nbsp;&nbsp;<span style="background-color:<?php echo $clr['color_code'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div><div class="col-md-3" id="price-<?php echo $clr['id'];?>"></div><div class="col-md-3" id="discount-<?php echo $clr['id'];?>"></div><div class="col-md-3" id="file-<?php echo $clr['id'];?>"></div></div>
                                          <?php }?>
                                          </div>
                                                                                                                                                                
                                          <div class="col-md-12">
                                             <div class="buttonz" style="text-align:left">
                                             <button type="submit" name="btnaddattributes">Save</button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                           <?php include 'includes/footer.php'; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <?php include 'includes/footerJs.php'; ?>

    <script>
    $(document).ready(function(){
        $("input:checkbox.color").click(function() {
        if($(this).is(":checked")){
            var id = $(this).val();
            $('#price-' + id).html('<input type="text" placeholder="Price" name="price' + id + '" class="form-control" style="border:1px solid" required>');
            $('#discount-' + id).html('<input type="text" placeholder="Discount" value="0.00" name="discount' + id + '" class="form-control" style="border:1px solid" required>');
            $('#file-' + id).html('<input type="file" name="file' + id + '[]' + '" required multiple/>');
        }
        else{
            var id = $(this).val();
            $('#price-' + id).html('');
            $('#discount-' + id).html('');
            $('#file-' + id).html('');
        }
    });

 

    });
    </script>

   </body>
</html>
