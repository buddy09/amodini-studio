<?php 
   date_default_timezone_set("Asia/Kolkata");
   include'includes/config.php';
   if(!isset($_SESSION['username'])){
       header('location: index.php');
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Dashboard :: Amodinistudio</title>
      <?php include'includes/css.php'; ?>
   </head>
   <body class="boxed">
      <!-- Loader -->
      <div id="loader-wrapper">
         <div class="cube-wrapper">
            <div class="cube-folding">
               <span class="leaf1"></span>
               <span class="leaf2"></span>
               <span class="leaf3"></span>
               <span class="leaf4"></span>
            </div>
         </div>
      </div>
      <div id="wrapper">
         <!-- Page -->
         <div class="page-wrapper">
            <?php include'includes/head.php'; ?>
            <?php include'includes/menu.php'; ?>
            <!-- Page Content -->
            <main class="page-main">
               <div class="block">
                  <div class="container">
                     <ul class="breadcrumbs">
                        <li><a href="index.php"><i class="icon icon-home"></i></a></li>
                        <li>/<span>My-Account</span></li>
                     </ul>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="form-card">
                              <div class="tabaccordion">
                                 <div class="myaccount-tab-menu nav" role="tablist">
                                    <ul style="color: #fff;">
                                       <li class="active"> <a href="#Tab1" role="tab"  data-toggle="tab"><i class="fa fa-dashboard"></i>
                                          Dashboard</a>
                                       </li>
                                       <li> <a href="#Tab2" role="tab" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i>
                                          Orders</a>
                                       </li>
                                       <li> <a href="#Tab3" role="tab" data-toggle="tab"><i class="fa fa-heart"></i>
                                       Wishlist</a>
                                       </li>
                                       <li> <a href="#Tab4" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i>
                                          address</a>
                                       </li>
                                       <li> <a href="#Tab5" role="tab" data-toggle="tab"><i class="fa fa-lock"></i> Password Change</a></li>
                                       <li style="border-bottom: solid 1px #efefef;"> <a href="<?php echo $siteurl; ?>logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-8">
                           <div class="row">
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane myaccount-content active" id="Tab1">
                                 <h5>Dashboard</h5>
                                 <?php
                                       if(isset($_SESSION['error'])){
                                          echo "
                                             <div class='alert alert-danger alert-dismissible'>
                                             <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                             <h4><i class='icon fa fa-warning'></i> Error!</h4>
                                             ".$_SESSION['error']."
                                             </div>
                                          ";
                                          unset($_SESSION['error']);
                                       }
                                       if(isset($_SESSION['success'])){
                                          echo "
                                             <div class='alert alert-success alert-dismissible'>
                                             <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                             <h4><i class='icon fa fa-check'></i> Success!</h4>
                                             ".$_SESSION['success']."
                                             </div>
                                          ";
                                          unset($_SESSION['success']);
                                       }
                                       ?>
                                    <form class="white" action="save.php" method="POST" autocomplete="off" enctype="multipart/form-data">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>First Name<span class="required">*</span></label>
                                             <input type="hidden" required  name="use_id" id="use_id" value="<?php echo $result['user_id']; ?>">
                                             <input type="hidden" required  name="type" value="profile_up">
                                             <input type="text" class="form-control" required value="<?php echo $result['fname']; ?>" name="fname" id="fname"
                                                placeholder="First Name">
                                          </div>
                                          <div class="col-sm-6">
                                             <label>Last Name<span class="required">*</span></label>
                                             <input type="text" class="form-control" required value="<?php echo $result['lname']; ?>" name="lname" id="lname"
                                                placeholder="Last Name">
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>E-mail<span class="required">*</span></label>
                                             <input type="email" class="form-control" required readonly name="email" value="<?php echo $result['email']; ?>" id="email"
                                                placeholder="Email">
                                          </div>
                                          <div class="col-sm-6">
                                             <label>Phone Number<span class="required">*</span></label>
                                             <input type="text" class="form-control" required name="phone" value="<?php echo $result['phone']; ?>" id="phone"
                                                placeholder="Phone Number">
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>Photo<span class="required"></span></label>
                                             <input type="file" class="form-control" name="photo" id="photo"
                                                placeholder="Photo">
                                          </div>
                                          <div class="col-sm-6">
                                          <div class="photo">
                                                <img style="height: 61px; width: 61px;box-shadow: 0px 0px 14px 2px rgb(255, 255)" src="images/users/<?php if($result['photo'] == ''){echo 'avatar.jpg';}else{ echo $result['photo'];} ?>" alt="">
                                             </div>
                                          
                                       </div>
                                       </div>
                                      
                                       <div>
                                          <div class="alert alert-success alert-dismissible" id="psuccess" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <div class="alert alert-danger alert-dismissible" id="perror" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <button type="submit" class="btn btn-lg">Update</button>
                                       </div>
                                    </form>
                                 </div>
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab2">
                                   <h5>My Orders</h5>
                                    <div class="table-responsive">
                                       <table class="table table-bordered">
                                          <tbody>
                                          <tr style="background: #14122d;color:#fff;">
                                          <th>Product</th>
                                          <th>Amount</th>
                                          <th>Payment Date</th>
                                          <th>Order Status</th>
                                       </tr>
                                       <tr>
                                       <td>Alfreds Futterkiste</td>
                                       <td>Maria Anders</td>
                                       <td>Germany</td>
                                    </tr>  
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane" id="Tab3">
                                    <h3>Single Size Conversion</h3>
                                    <div class="table-responsive">
                                       <table class="table table-bordered">
                                          <tbody>
                                             <tr>
                                                <td><strong>UK</strong></td>
                                                <td>
                                                   <ul class="params-row">
                                                      <li>18</li>
                                                      <li>20</li>
                                                      <li>22</li>
                                                      <li>24</li>
                                                      <li>26</li>
                                                   </ul>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td><strong>European</strong></td>
                                                <td>
                                                   <ul class="params-row">
                                                      <li>46</li>
                                                      <li>48</li>
                                                      <li>50</li>
                                                      <li>52</li>
                                                      <li>54</li>
                                                   </ul>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td><strong>US</strong></td>
                                                <td>
                                                   <ul class="params-row">
                                                      <li>14</li>
                                                      <li>16</li>
                                                      <li>18</li>
                                                      <li>20</li>
                                                      <li>22</li>
                                                   </ul>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td><strong>Australia</strong></td>
                                                <td>
                                                   <ul class="params-row">
                                                      <li>8</li>
                                                      <li>10</li>
                                                      <li>12</li>
                                                      <li>14</li>
                                                      <li>16</li>
                                                   </ul>
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab4">
                                    <h5 style="color:green;font-size: 17px;">Address</h5>
                                    <form class="white" action="#" method="POST" autocomplete="off">
                                       <div class="row">
                                          <label>Street Address</label>
                                          
                                          <input type="hidden" required  name="use_id" id="userr_id" value="<?php echo $result['user_id']; ?>">
                                          <input type="text" class="form-control" name="address"  id="address" value="<?php echo $result['address']; ?>" placeholder="Street Address">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Town / City</label>
                                                <input type="text" class="form-control" name="city" id="city" value="<?php echo $result['city']; ?>" placeholder="Town / City">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>State/Province</label>
                                                <input type="text" class="form-control" id="state" name="state" value="<?php echo $result['state']; ?>" placeholder="State/Province">
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Country</label>
                                                <input type="text" class="form-control" id="country" value="<?php echo $result['country']; ?>" name="country"
                                                   placeholder="State/Province">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>Postcode / Zip</label>
                                                <input type="text" class="form-control" name="zip" value="<?php echo $result['zip']; ?>" id="zip"
                                                   placeholder="Postcode / Zip">
                                             </div>
                                          </div>
                                       </div>
                                       <h5 style="color:green;font-size: 17px;">Shipping Address</h5>
                                       <div class="row">
                                          <label>Street Address</label>
                                          <input type="text" class="form-control" name="s_address"  id="s_address" value="<?php echo $result['s_address']; ?>" placeholder="Street Address">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Town / City</label>
                                                <input type="text" class="form-control" name="s_city" id="s_city" value="<?php echo $result['s_city']; ?>" placeholder="Town / City">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>State/Province</label>
                                                <input type="text" class="form-control" id="s_state" name="s_state" value="<?php echo $result['s_state']; ?>" placeholder="State/Province">
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Country</label>
                                                <input type="text" class="form-control" id="s_country" value="<?php echo $result['s_country']; ?>" name="s_country"
                                                   placeholder="State/Province">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>Postcode / Zip</label>
                                                <input type="text" class="form-control" name="s_zip" value="<?php echo $result['s_zip']; ?>" id="s_zip"
                                                   placeholder="Postcode / Zip">
                                             </div>
                                          </div>
                                       </div>
                                       <div>
                                          <div class="alert alert-success alert-dismissible" id="asuccess" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <div class="alert alert-danger alert-dismissible" id="aerror" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <button type="button" id="addressUpdate" class="btn btn-lg">Update</button>
                                       </div>
                                    </form>
                                 </div>
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab5">
                                    <h5>Password Change</h5>
                                    <form class="white" action="#" method="POST" autocomplete="off">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>Old Password<span class="required">*</span></label>
                                             <input type="hidden" required  name="us_id" id="us_id" value="<?php echo $result['user_id']; ?>">
                                             <input type="text" class="form-control" required  name="oldpass" id="old_password"
                                                placeholder="Old Password">
                                          </div>
                                          </div>
                                          <div class="row">
                                          <div class="col-sm-6">
                                             <label>New Password<span class="required">*</span></label>
                                             <input type="text" class="form-control" required  name="newpassword" id="newpassword"
                                                placeholder="New Password">
                                          </div>
                                          <div class="col-sm-6">
                                             <label>Confirm Password <span class="required">*</span></label>
                                             <input type="text" required class="form-control" name="cpassword" id="cpassword2"
                                                placeholder="Confirm Password">
                                          </div>
                                       </div>
                                       <div>
                                          <div class="alert alert-success alert-dismissible" id="csuccess" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <div class="alert alert-danger alert-dismissible" id="cerror" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <button type="button" id="changepassword" class="btn btn-lg">Change Password</button>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </main>
            <!-- /Page Content -->
            <?php include'includes/footer.php'; ?>
         </div>
         <!-- /Page -->
      </div>
      <?php include'includes/footerJs.php'; ?>
   </body>
</html>