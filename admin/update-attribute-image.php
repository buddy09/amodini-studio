<?php 
include 'includes/session.php';
if(isset($_POST['update-attribute-image'])){
    $imgid = $_POST['imgid'];
    $pid = $_POST['pid'];
    $temp = explode(".", $_FILES["attribute_image"]["name"]);
    $newfilename = round(microtime(true)) . '.' . end($temp);
    move_uploaded_file($_FILES["attribute_image"]["tmp_name"], "uploads/productImages/" . $newfilename);
    $res = mysqli_query($con, "update tbl_product_images set product_photo='$newfilename' where imgid='$imgid'") or die(mysqli_error());
    if($res){
        $_SESSION['success'] = 'Image updated successfully';
    }else{
        $_SESSION['error'] = 'Error';
    }
    header('location: manage_attributes.php?id='.$pid);
}
?>