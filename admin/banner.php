<?php
 include 'includes/session.php';

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Manage Banner</title>
    <?php include 'includes/css.php'; ?>
</head>
<body>
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <div class="panel-layout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                <div class="main-page">
                <?php include 'includes/sidebar.php'; ?>
                        <!-- side header -->
                  <?php include 'includes/head.php'; ?>
                        <div class="main-content">
             <div class="panel-body">
              <div class="content-area">
                <?php
                if(isset($_REQUEST['status'])){
                  $id = $_REQUEST['id'];
                  $status = $_REQUEST['status'];
                  $row = mysqli_query($con, "update banner set status='$status' where id='$id'") or die(mysqli_error());
                  if($row){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Status updated successfully</p>
                      </div>
                    ";
                  }
                }
                if(isset($_GET['del']))
                {
                $id=$_GET['del'];
                $query=mysqli_Query($con,"DELETE  FROM banner WHERE id='$id'");
                echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>Record Deleted Successfully</p>
                      </div>
                    ";
                }
                ?>
                <?php
                  if(isset($_SESSION['error'])){
                    echo "
                      <div class='alert alert-danger brd-5 text-center'>
                        <p>".$_SESSION['error']."</p> 
                      </div>
                    ";
                    unset($_SESSION['error']);
                  }
                  if(isset($_SESSION['success'])){
                    echo "
                      <div class='alert alert-success brd-5 text-center'>
                        <p>".$_SESSION['success']."</p>
                      </div>
                    ";
                    unset($_SESSION['success']);
                  }
                ?>
                <div class="gap inner-bg">
                  <div class="element-title">
                    <h4>Category List</h4>
                        <a class="btn-st" style="float: right;"  href="#addnew" data-toggle="modal" id="addTours" title="">Add New</a>
                     </div>
                  <div class="table-styles">
                    <div class="widget">
                      <table id="example" class="prj-tbl striped bordered table-responsive">
                        <thead class="">
                          <tr>
                            <th><em>#</em></th>
                            <th><em>Photo</em></th>
                            <th><em>Change Status</em></th>
                            <th><em>Tools</em></th>
                          </tr>
                        </thead>
                        <tbody>
                           <?php
                            $i = 1;
                            $rw = mysqli_query($con,"select *from banner") or die(mysqli_error());
                            while($row = mysqli_fetch_array($rw)){
                            ?>
                            <tr>
                            <td><span><?php  echo $i; ?></span></td>
                            <td style="width:40%"><img src="images/banner/<?php echo $row['photo']; ?>" height='80px' width='130px'>
                                        <span class='pull-right'><a href='#edit_photo' class='photo' data-toggle='modal' data-id="<?php echo $row['id']; ?>"><i class='fa fa-edit'></i></a></span>
                            </td>
                             <td><i>
                          <?php if($row['status']=='0'){ ?>
                             <a href="banner.php?id=<?php echo $row['id']; ?>&status=1" class='btn btn-success'>Active</a>
                           <?php } else{?>
                            <a href="banner.php?id=<?php echo $row['id']; ?>&status=0" class='btn btn-danger'>Inactive</a>
                           <?php }?></i>
                        </td>
                            <td><i><a href="banner.php?del=<?php echo $row['id'] ?>"  onclick="return confirm('Do you want to delete');"> <i class="icon-trash"></i></a>
                            </td>
                          </tr>
                         <?php $i++; } ?>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            <?php include 'includes/footer.php'; ?>
            </div>
                   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footerJs.php'; ?>
<?php include 'includes/banner_modal.php'; ?>

<script>
$(function(){
  $(document).on('click', '.edit', function(e){
    e.preventDefault();
    $('#banneredit').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $(document).on('click', '.photo', function(e){
    e.preventDefault();
    $('#edit_photo').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $(document).on('click', '.delete', function(e){
    e.preventDefault();
    $('#delete_photo').modal('show');
    var id = $(this).data('id');
    getRow(id);
  });

  $("#edit").on("hidden.bs.modal", function () {
      $('.append_items').remove();
  });

});

function getRow(id){
  $.ajax({
    type: 'POST',
    url: 'banner_row.php',
    data: {id:id},
    dataType: 'json',
    success: function(response){
      $('.bannerid').val(response.id);
    }
  });
}
</script>

</body>
</html>