
<?php
	include 'includes/session.php';
	include 'includes/slugify.php';
	if(isset($_POST['add'])){
        $cat_id = $_POST['cat_id'];
        $sub_category = $_POST['sub_category'];
		$slug = slugify($sub_category);	
		$stmt = mysqli_query($con,"select count(*) as numrows from tbl_sub_category where slug='$slug'");
		$row = mysqli_fetch_array($stmt);
		if($row['numrows'] > 0){
			$_SESSION['error'] = 'Sub category already exist';
		}
		else{
			$stmt = mysqli_query($con,"insert into tbl_sub_category(category_id,sub_category,slug) values('$cat_id','$sub_category','$slug')") or die(mysqli_error());
			$_SESSION['success'] = 'Sub category added successfully';
		}
	}
	else{
		$_SESSION['error'] = 'Fill up sub category form first';
	}
	header('location: manage_sub_category.php');
?>