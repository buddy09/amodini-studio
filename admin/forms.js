$(document).ready(function () {

    $('#loginBtn').on('click', function () {
        var username = $('#email').val();
        var password = $('#password').val();
        if (username != "" && password != "") {
          $('#loginBtn').html('Please Wait..');
          $('#loginBtn').attr("disabled", true);
          
          $.ajax({
            url: "save.php",
            type: "POST",
            data: {
              type: 2,
              username: username,
              password: password
            },
            cache: false,
            success: function (dataResult) {
              //alert(dataResult);
              var dataResult = JSON.parse(dataResult);
              if (dataResult.statusCode == 203) {
                Lobibox.notify('success', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'center top',
                    showClass: 'rollIn',
                    hideClass: 'rollOut',
                    icon: 'fa fa-check-circle',
                    width: 600,
                    msg: 'login successful redirect...'
                    
                    });
                    var delay = 2000;
                    setTimeout(function(){
                        window.location.href = "home.php";
                    },delay);
                    
                //location.href = "super_home.php";
              } else if (dataResult.statusCode == 204) {
                $('#loginBtn').html('Login');
                $('#loginBtn').attr("disabled", false);
                Lobibox.notify('error', {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    icon: 'fa fa-times-circle',
                    position: 'center top',
                    showClass: 'lightSpeedIn',
                    hideClass: 'lightSpeedOut',
                    width: 600,
                    msg: 'Invalid EmailId or Password!'
                    });
                //  Swal.fire(
                //    'Error!',
                //   'Invalid EmailId or Password!',
                //   'error'
                //  )
              }
            }
          });
        } else {
            Lobibox.notify('info', {
                pauseDelayOnHover: true,
                icon: 'fa fa-info-circle',
                continueDelayOnInactiveTab: false,
                position: 'center top',
                showClass: 'bounceIn',
                hideClass: 'bounceOut',
                width: 600,
                msg: 'Please fill all the field!'
                });
          //alert('Please fill all the field!');
        }
      });
});