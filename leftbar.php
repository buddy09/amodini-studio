<!-- Left column -->
<div class="col-md-3 filter-col fixed aside">
    <div class="filter-container">
        <div class="fstart"></div>
        <div class="fixed-wrapper">
            <div class="fixed-scroll">
                <div class="filter-col-header">
                    <div class="title">Filters</div>
                    <a href="#" class="filter-col-toggle"></a>
                </div>
                <div class="filter-col-content">
                    <h2>Shoping By</h2>
                    <div class="sidebar-block collapsed">
                        <div class="block-title">
                            <span>Categories</span>
                            <div class="toggle-arrow"></div>
                        </div>
                        <div class="block-content">
                            <ul class="category-list">
                                <?php 
                                    $c1 = mysqli_query($con,"select *from tbl_inner_category where sub_category_id='$subcatid' and status='1'");
                                    while($cq = mysqli_fetch_array($c1)){ 
                                ?>
                                <li><a class="sidebarlink" href="<?php echo $siteurl;?>products/<?php echo $slug1;?>/<?php echo $slug2;?>/<?php echo $cq['slug'];?>/1"><?php echo $cq['inner_category']; ?></a>
                                    <a href="#" class="clear"></a>
                                </li>
                                <?php }?>
                            </ul>
                            <div class="bg-striped"></div>
                        </div>
                    </div>
                    <div class="sidebar-block collapsed">
                        <div class="block-title">
                            <span>Size</span>
                            <div class="toggle-arrow"></div>
                        </div>
                        <div class="block-content">
                            <ul class="size-list">
                                <?php 
                                $siz = mysqli_query($con,"SELECT * FROM `tbl_size` WHERE status='1'");
                                while($f = mysqli_fetch_array($siz)){
                                ?>
                                <li><a href="<?php echo $siteurl;?>size/<?php echo $slug1;?>/<?php echo $slug2;?>/<?php echo $slug3;?>/<?php echo $f['size_name'];?>/1" class="size-short"><span class="clear"></span><span class="value"><?php echo $f['size_name']; ?></span></a></li>
                                <?php }?>
                            </ul>
                            <div class="bg-striped"></div>
                        </div>
                    </div>
                    <div class="sidebar-block collapsed">
                        <div class="block-title">
                            <span>Color</span>
                            <div class="toggle-arrow"></div>
                        </div>
                        <div class="block-content">
                            <ul class="size-list">
                                <?php
                                $color = mysqli_query($con,"SELECT *FROM `tbl_color` WHERE status='1'");
                                while($c = mysqli_fetch_array($color)){
                                ?>
                                <li style="background-color:<?php echo $c['color_code'];?>"><a href="<?php echo $siteurl;?>color/<?php echo $slug1;?>/<?php echo $slug2;?>/<?php echo $slug3;?>/<?php echo substr($c['color_code'], 1);?>/1" class="color-short"><span class="clear"></span><span class="value"><?php echo $f['size_name']; ?></span></a></li>
                                <?php }?>
                            </ul>
                            <div class="bg-striped"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fend"></div>
    </div>
</div>
<!-- /Left column -->