<?php 
   date_default_timezone_set("Asia/Kolkata");
   include'includes/config.php';
   if(!isset($_SESSION['username'])){
       header('location: index.php');
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Dashboard :: Amodinistudio</title>
      <?php include 'includes/css.php'; ?>
   </head>
   <body class="boxed">
      <!-- Loader -->
      <div id="loader-wrapper">
         <div class="cube-wrapper">
            <div class="cube-folding">
               <span class="leaf1"></span>
               <span class="leaf2"></span>
               <span class="leaf3"></span>
               <span class="leaf4"></span>
            </div>
         </div>
      </div>
      <div id="wrapper">
         <!-- Page -->
         <div class="page-wrapper">
            <?php include 'includes/head.php'; ?>
            <?php include 'includes/menu.php'; ?>
            <!-- Page Content -->
            <main class="page-main">
               <div class="block">
                  <div class="container">
                     <ul class="breadcrumbs">
                        <li><a href="index.php"><i class="icon icon-home"></i></a></li>
                        <li>/<span>My-Account</span></li>
                     </ul>
                  </div>
               </div>
               <div class="block">
                  <div class="container">
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="form-card">
                              <div class="tabaccordion">
                                 <div class="myaccount-tab-menu nav" role="tablist">
                                    <ul style="color: #fff;">
                                       <li class="active"> <a href="#Tab1" role="tab"  data-toggle="tab"><i class="fa fa-dashboard"></i>
                                          Dashboard</a>
                                       </li>
                                       <li> <a href="#Tab2" role="tab" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i>
                                       Order History</a>
                                       </li>
                                       <li> <a href="#Tab3" role="tab" data-toggle="tab"><i class="fa fa-heart"></i>
                                       Wishlist</a>
                                       </li>
                                       <li> <a href="#Tab4" role="tab" data-toggle="tab"><i class="fa fa-map-marker"></i>
                                          address</a>
                                       </li>
                                       <li> <a href="#Tab5" role="tab" data-toggle="tab"><i class="fa fa-lock"></i>Password Change</a></li>
                                       <li style="border-bottom: solid 1px #efefef;"> <a href="<?php echo $siteurl; ?>logout.php"><i class="fa fa-sign-out"></i> Logout</a></li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-8">
                           <div class="row">
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane myaccount-content active" id="Tab1">
                                 <h1 class="product-name">Dashboard</h1>
                                 <?php
                                       if(isset($_SESSION['error'])){
                                          echo "
                                             <div class='alert alert-danger alert-dismissible'>
                                             <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                             <h4><i class='icon fa fa-warning'></i> Error!</h4>
                                             ".$_SESSION['error']."
                                             </div>
                                          ";
                                          unset($_SESSION['error']);
                                       }
                                       if(isset($_SESSION['success'])){
                                          echo "
                                             <div class='alert alert-success alert-dismissible'>
                                             <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                             <h4><i class='icon fa fa-check'></i> Success!</h4>
                                             ".$_SESSION['success']."
                                             </div>
                                          ";
                                          unset($_SESSION['success']);
                                       }
                                       ?>
                                    <form class="white" action="save.php" method="POST" autocomplete="off" enctype="multipart/form-data">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>First Name<span class="required">*</span></label>
                                             <input type="hidden" required  name="use_id" id="use_id" value="<?php echo $result['user_id']; ?>">
                                             <input type="hidden" required  name="type" value="profile_up">
                                             <input type="text" class="form-control" required value="<?php echo $result['fname']; ?>" name="fname" id="fname"
                                                placeholder="First Name">
                                          </div>
                                          <div class="col-sm-6">
                                             <label>Last Name<span class="required">*</span></label>
                                             <input type="text" class="form-control" required value="<?php echo $result['lname']; ?>" name="lname" id="lname"
                                                placeholder="Last Name">
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>E-mail<span class="required">*</span></label>
                                             <input type="email" class="form-control" required readonly name="email" value="<?php echo $result['email']; ?>" id="email"
                                                placeholder="Email">
                                          </div>
                                          <div class="col-sm-6">
                                             <label>Phone Number<span class="required">*</span></label>
                                             <input type="text" class="form-control" required name="phone" value="<?php echo $result['phone']; ?>" id="phone"
                                                placeholder="Phone Number">
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>Photo<span class="required"></span></label>
                                             <input type="file" class="form-control" name="photo" id="photo"
                                                placeholder="Photo">
                                          </div>
                                          <div class="col-sm-6">
                                          <div class="photo">
                                                <img style="height: 61px; width: 61px;box-shadow: 0px 0px 14px 2px rgb(255, 255)" src="images/users/<?php if($result['photo'] == ''){echo 'avatar.jpg';}else{ echo $result['photo'];} ?>" alt="">
                                             </div>
                                            
                                       </div>
                                       </div>
                                      
                                       <div>
                                          <div class="alert alert-success alert-dismissible" id="psuccess" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <div class="alert alert-danger alert-dismissible" id="perror" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <button type="submit" class="btn btn-lg">Update</button>
                                       </div>
                                    </form>
                                 </div>
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab2">
                                   <h1 class="product-name">Order History</h1>
                                    <div class="table-responsive">
                                       <table class="table table-bordered">
                                          <tbody>
                                          <tr style="background: #14122d;color:#fff;">
                                          <th>#</th>
                                          <th>Payment ID</th>
                                          <th>Amount</th>
                                          <th>Payment Date</th>
                                          <th>Order Status</th>
                                          <th>Order Detail</th>
                                       </tr>
                                          <?php 
                                          $i = 1;
                                          $user_name = $_SESSION['username'];
                                             $p1 = mysqli_query($con,"select *from tbl_checkout where user_name = '$user_name' order by order_id desc");
                                             if(mysqli_num_rows($p1)){
                                             while($v = mysqli_fetch_array($p1)){
                                                // $uid = $v['uid'];
                                                // $date = date('F j, Y ', strtotime($v['payment_date']));
                                                // $cart = mysqli_query($con,"SELECT * FROM `tbl_cart` WHERE uid='$uid'");
                                                // $q3 = mysqli_fetch_array($cart);
                                                // $ppid = $q3['product_id'];
                                                // $pw = mysqli_query($con,"SELECT * FROM `tbl_products` WHERE product_id='$ppid'");
                                                // $pr = mysqli_fetch_array($pw);
                                          ?>
                                          <tr>
                                             <td style="border: 1px solid #ddd;"><?php echo $i; ?></td>
                                             <td style="border: 1px solid #ddd;"><?php echo $v['payment_id'];?></td>
                                             <td style="border: 1px solid #ddd;"><?php echo $v['amount']; ?></td>
                                             <td style="border: 1px solid #ddd;"><?php echo $v['payment_date']; ?></td>
                                             <td style="border: 1px solid #ddd;color:green;font-weight:bold"><?php echo $v['delivery_status']; ?></td>
                                             <td style="border: 1px solid #ddd;color:green;font-weight:bold"><a href="quick-view.php?uid=<?php echo $v['uid'];?>" class="quick-view-link">Order Detail</a></td>
                                          </tr>  
                                          <?php $i++; }?>
                                          <?php }else{?>
                                             <tr>
                                                 <td col='3'>Record not found</td>
                                          </tr>
                                          <?php }?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab3">
                                   <h1 class="product-name">Wishlist</h1>
                                    <div class="table-responsive">
                                       <table class="table table-bordered">
                                          <tbody>
                                          <tr style="background: #14122d;color:#fff;">
                                          <th>#</th>
                                          <th>Product</th>
                                          <th>Amount</th>
                                          <th>Tools</th>
                                       </tr>
                                          <?php 
                                          $i = 1;
                                          $uuid = $_SESSION['uid'];
                                             $vw1 = mysqli_query($con,"SELECT * FROM `wishlist_item` WHERE uid='$uuid' ORDER BY wishlistid DESC");
                                             if(mysqli_num_rows($vw1)){
                                             while($w = mysqli_fetch_array($vw1)){
                                                $uid = $w['uid'];
                                            
                                                $ppid = $w['product_id'];
                                                $pw = mysqli_query($con,"SELECT * FROM `tbl_products` WHERE product_id='$ppid'");
                                                $pr = mysqli_fetch_array($pw);
                                          ?>
                                          <tr>
                                             <td style="border: 1px solid #ddd;"><?php echo $i; ?></td>
                                             <td style="border: 1px solid #ddd;"><a href="<?php echo $siteurl; ?>product-detail/<?php echo $pr['slug'];?>"><?php echo $pr['product_name']; ?></a><br> ITEM CODE: <?php echo $pr['product_code']; ?></td>
                                             <td style="border: 1px solid #ddd;"><?php echo $pr['product_price']; ?></td>
                                             <td style="border: 1px solid #ddd;"><span class='rpdelete' data-id="<?php echo $w['wishlistid']; ?>"><i class="icon icon-close"></i></span></td>
                                          </tr>  
                                          <?php $i++; }?>
                                          <?php }else{?>
                                             <tr>
                                                 <td col='3'>Record not found</td>
                                          </tr>
                                          <?php }?>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab4">
                                    <h5 style="color:green;font-size: 17px;">Address</h5>
                                    <form class="white" action="#" method="POST" autocomplete="off">
                                       <div class="row">
                                          <label>Street Address</label>
                                          
                                          <input type="hidden" required  name="use_id" id="userr_id" value="<?php echo $result['user_id']; ?>">
                                          <input type="text" class="form-control" name="address"  id="address" value="<?php echo $result['address']; ?>" placeholder="Street Address">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Town / City</label>
                                                <input type="text" class="form-control" name="city" id="city" value="<?php echo $result['city']; ?>" placeholder="Town / City">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>State/Province</label>
                                                <input type="text" class="form-control" id="state" name="state" value="<?php echo $result['state']; ?>" placeholder="State/Province">
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Country</label>
                                                <input type="text" class="form-control" id="country" value="<?php echo $result['country']; ?>" name="country"
                                                   placeholder="State/Province">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>Postcode / Zip</label>
                                                <input type="text" class="form-control" name="zip" value="<?php echo $result['zip']; ?>" id="zip"
                                                   placeholder="Postcode / Zip">
                                             </div>
                                          </div>
                                       </div>
                                       <h5 style="color:green;font-size: 17px;">Shipping Address</h5>
                                       <div class="row">
                                          <label>Street Address</label>
                                          <input type="text" class="form-control" name="s_address"  id="s_address" value="<?php echo $result['s_address']; ?>" placeholder="Street Address">
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Town / City</label>
                                                <input type="text" class="form-control" name="s_city" id="s_city" value="<?php echo $result['s_city']; ?>" placeholder="Town / City">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>State/Province</label>
                                                <input type="text" class="form-control" id="s_state" name="s_state" value="<?php echo $result['s_state']; ?>" placeholder="State/Province">
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="col-sm-6">
                                                <label>Country</label>
                                                <input type="text" class="form-control" id="s_country" value="<?php echo $result['s_country']; ?>" name="s_country"
                                                   placeholder="State/Province">
                                             </div>
                                             <div class="col-sm-6">
                                                <label>Postcode / Zip</label>
                                                <input type="text" class="form-control" name="s_zip" value="<?php echo $result['s_zip']; ?>" id="s_zip"
                                                   placeholder="Postcode / Zip">
                                             </div>
                                          </div>
                                       </div>
                                       <div>
                                          <div class="alert alert-success alert-dismissible" id="asuccess" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <div class="alert alert-danger alert-dismissible" id="aerror" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <button type="button" id="addressUpdate" class="btn btn-lg">Update</button>
                                       </div>
                                    </form>
                                 </div>
                                 <div role="tabpanel" class="tab-pane myaccount-content" id="Tab5">
                                    <h1 class="product-name">Password Change</h1>
                                    <form class="white" action="#" method="POST" autocomplete="off">
                                       <div class="row">
                                          <div class="col-sm-6">
                                             <label>Old Password<span class="required">*</span></label>
                                             <input type="hidden" required  name="us_id" id="us_id" value="<?php echo $result['user_id']; ?>">
                                             <input type="text" class="form-control" required  name="oldpass" id="old_password"
                                                placeholder="Old Password">
                                          </div>
                                          </div>
                                          <div class="row">
                                          <div class="col-sm-6">
                                             <label>New Password<span class="required">*</span></label>
                                             <input type="text" class="form-control" required  name="newpassword" id="newpassword"
                                                placeholder="New Password">
                                          </div>
                                          <div class="col-sm-6">
                                             <label>Confirm Password <span class="required">*</span></label>
                                             <input type="text" required class="form-control" name="cpassword" id="cpassword2"
                                                placeholder="Confirm Password">
                                          </div>
                                       </div>
                                       <div>
                                          <div class="alert alert-success alert-dismissible" id="csuccess" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <div class="alert alert-danger alert-dismissible" id="cerror" style="display:none;">
                                             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                          </div>
                                          <button type="button" id="changepassword" class="btn btn-lg">Change Password</button>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </main>
            <!-- /Page Content -->
            <?php include'includes/footer.php'; ?>
         </div>
         <!-- /Page -->
      </div>
      <?php include 'includes/footerJs.php'; ?>
      <script>
         $(document).ready(function(){
      $(document).on('click', '.rpdelete', function(e){
         e.preventDefault();
         var el = this;
         var deleteid = $(this).data('id');
         var confirmalert = confirm("Are you sure?");
         if (confirmalert == true) {
            // AJAX Request
            $.ajax({
            url: 'wishlist_delete.php',
            type: 'POST',
            data: { id:deleteid },
            success: function(response){
               if(response == 1){
            alert("Record Deleted successfully.");
            window.location.href = "";

               }else{
              alert('Invalid ID.');
               }

            }
            });
         }

      });

   

   });

   </script>

   </body>
</html>