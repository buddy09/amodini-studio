<!DOCTYPE html>
<html lang="en">
<?php 
include 'includes/config.php';
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Checkout</title>
	<meta name="author" content="BigSteps">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
    <?php include 'includes/css.php'; ?>

	<style>
      .razorpay-payment-button {
        color: #ffffff !important;
        background-color: #7266ba;
        border-color: #7266ba;
        font-size: 14px;
        padding: 10px;
      }
    </style>
	
</head>

<body class="boxed">
	<!-- Loader -->
	<div id="loader-wrapper">
		<div class="cube-wrapper">
			<div class="cube-folding">
				<span class="leaf1"></span>
				<span class="leaf2"></span>
				<span class="leaf3"></span>
				<span class="leaf4"></span>
			</div>
		</div>
	</div>
	<!-- /Loader -->
	<div class="fixed-btns">
		<!-- Back To Top -->
		<a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
		<!-- /Back To Top -->
	</div>
	<div id="wrapper">
		<!-- Page -->
		<div class="page-wrapper">
			<!-- Header -->
			<?php include 'includes/head.php'; ?>
			<!-- /Header -->
			<!-- Sidebar -->
			<div class="sidebar-wrapper">
				<div class="sidebar-top"><a href="#" class="slidepanel-toggle"><i class="icon icon-left-arrow-circular"></i></a></div>
				<ul class="sidebar-nav">
					<li> <a href="index.html">HOME</a> </li>
					<li> <a href="gallery.html">GALLERY</a> </li>
					<li> <a href="blog.html">BLOG</a> </li>
					<li> <a href="category-fixed-sidebar.html">SHOP</a> </li>
					<li> <a href="faq.html">FAQ</a> </li>
					<li> <a href="contact.html">CONTACT</a> </li>
				</ul>
				<div class="sidebar-bot">
					<div class="share-button toTop">
						<span class="toggle"></span>
						<ul class="social-list">
							<li>
								<a href="#" class="icon icon-google google"></a>
							</li>
							<li>
								<a href="#" class="icon icon-fancy fancy"></a>
							</li>
							<li>
								<a href="#" class="icon icon-pinterest pinterest"></a>
							</li>
							<li>
								<a href="#" class="icon icon-twitter-logo twitter"></a>
							</li>
							<li>
								<a href="#" class="icon icon-facebook-logo facebook"></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /Sidebar -->
			<!-- Page Content -->
			<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="icon icon-home"></i></a></li>
							<li>/<span>Checkout</span></li>
						</ul>
					</div>
				</div>
				<div class="block">
					<div class="container">
						<div class="row">
							<!-- <div class="col-md-6">
								<h2>Billing Details</h2>
								<form class="white" action="#">
									<div class="row">
										<div class="col-sm-6">
											<label>Country<span class="required">*</span></label>
											<div class="select-wrapper">
												<select class="form-control">
													<option> - Select Country - </option>
													<option data-code="US" value="United States">United States</option>
													<option data-code="NL" value="Netherlands">Netherlands</option>
													<option data-code="CA" value="Canada">Canada</option>
													<option data-code="TD" value="Chad">Chad</option>
													<option data-code="CL" value="Chile">Chile</option>
													<option data-code="CN" value="China">China</option>
													<option data-code="CY" value="Cyprus">Cyprus</option>
													<option data-code="EG" value="Egypt">Egypt</option>
													<option data-code="FI" value="Finland">Finland</option>
													<option data-code="FR" value="French">French</option>
													<option data-code="DE" value="German">German</option>
													<option data-code="GR" value="Greece">Greece</option>
													<option data-code="HT" value="Haiti">Haiti</option>
													<option data-code="HU" value="Hungary">Hungary</option>
													<option data-code="IS" value="Iceland">Iceland</option>
													<option data-code="IN" value="India">India</option>
													<option data-code="ID" value="Indonesia">Indonesia</option>
													<option data-code="IR" value="Iran, Islamic Republic Of">Iran</option>
													<option data-code="IQ" value="Iraq">Iraq</option>
													<option data-code="IE" value="Ireland">Ireland</option>
													<option data-code="IL" value="Israel">Israel</option>
													<option data-code="IT" value="Italy">Italy</option>
													<option data-code="JP" value="Japan">Japan</option>
													<option data-code="LU" value="Luxembourg">Luxembourg</option>
													<option data-code="MX" value="Mexico">Mexico</option>
												</select>
											</div>
										</div>
										<div class="col-sm-6">
											<label>State/Province <span class="required">*</span></label>
											<div class="select-wrapper">
												<select class="form-control">
													<option> - Select Province/State - </option>
													<option value="AB">Alberta</option>
													<option value="BC">British Columbia</option>
													<option value="MB">Manitoba</option>
													<option value="NB">New Brunswick</option>
													<option value="NL">Newfoundland and Labrador</option>
													<option value="NS">Nova Scotia</option>
													<option value="NT">Northwest Territories</option>
													<option value="NU">Nunavut</option>
													<option value="ON">Ontario</option>
													<option value="PE">Prince Edward Island</option>
													<option value="QC">Quebec</option>
													<option value="SK">Saskatchewan</option>
													<option value="YT">Yukon</option>
													<option> ---------------- </option>
													<option value="AL">Alabama</option>
													<option value="AK">Alaska</option>
													<option value="AZ">Arizona</option>
													<option value="AR">Arkansas</option>
													<option value="CA">California</option>
													<option value="CO">Colorado</option>
													<option value="CT">Connecticut</option>
													<option value="DE">Delaware</option>
													<option value="DC">District Of Columbia</option>
													<option value="FL">Florida</option>
													<option value="GA">Georgia</option>
													<option value="HI">Hawaii</option>
													<option value="ID">Idaho</option>
													<option value="IL">Illinois</option>
													<option value="IN">Indiana</option>
													<option value="IA">Iowa</option>
													<option value="KS">Kansas</option>
													<option value="KY">Kentucky</option>
													<option value="LA">Louisiana</option>
													<option value="ME">Maine</option>
													<option value="MD">Maryland</option>
													<option value="MA">Massachusetts</option>
													<option value="MI">Michigan</option>
													<option value="MN">Minnesota</option>
													<option value="MS">Mississippi</option>
													<option value="MO">Missouri</option>
													<option value="MT">Montana</option>
													<option value="NE">Nebraska</option>
													<option value="NV">Nevada</option>
													<option value="NH">New Hampshire</option>
													<option value="NJ">New Jersey</option>
													<option value="NM">New Mexico</option>
													<option value="NY">New York</option>
													<option value="NC">North Carolina</option>
													<option value="ND">North Dakota</option>
													<option value="OH">Ohio</option>
													<option value="OK">Oklahoma</option>
													<option value="OR">Oregon</option>
													<option value="PA">Pennsylvania</option>
													<option value="RI">Rhode Island</option>
													<option value="SC">South Carolina</option>
													<option value="SD">South Dakota</option>
													<option value="TN">Tennessee</option>
													<option value="TX">Texas</option>
													<option value="UT">Utah</option>
													<option value="VT">Vermont</option>
													<option value="VA">Virginia</option>
													<option value="WA">Washington</option>
													<option value="WV">West Virginia</option>
													<option value="WI">Wisconsin</option>
													<option value="WY">Wyoming</option>
												</select>
											</div>
										</div>
									</div>
									<label>Street Address</label>
									<input type="text" class="form-control">
									<div class="row">
										<div class="col-sm-6">
											<label>Town / City<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
										<div class="col-sm-6">
											<label>Postcode / Zip<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
									</div>
									<div class="row">
										<div class="col-sm-6">
											<label>First Name<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
										<div class="col-sm-6">
											<label>Last Name<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
									</div>
									<label>Company Name</label>
									<input type="text" class="form-control">
									<div class="row">
										<div class="col-sm-6">
											<label>E-mail<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
										<div class="col-sm-6">
											<label>Phone Number<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
									</div>
									<div>
										<div class="checkbox-group">
											<input type="checkbox" name="createAccount" id="checkbox-createAccount">
											<label for="checkbox-createAccount"><span class="check"></span><span class="box"></span>Create an Account ?</label>
										</div>
									</div>
								</form>
								<h2>Delivery</h2>
								<p>Choose delivery option below</p>
								<form class="white" action="#">
									<div class="radioset">
										<label class="radio">
											<input id="radioDelivery1" type="radio" name="radioDelivery" value="none">
											<span class="outer"><span class="inner"></span></span><b>Standard Delivery</b> $5.99 / Delivery in 5 to 7 business Days
										</label>
										<label class="radio">
											<input id="radioDelivery2" type="radio" name="radioDelivery" value="none">
											<span class="outer"><span class="inner"></span></span><b>Express Delivery</b> $19.99 / Delivery in 1 business Days
										</label>
									</div>
								</form>
								<h2>Payment & Order Review</h2>
								<form class="white" action="#">
									<p>Credit Card</p>
									<p class="payment-link">
										<a href="#"><img src="images/payment-logo/icon-pay-1.png" alt=""></a>
										<a href="#"><img src="images/payment-logo/icon-pay-2.png" alt=""></a>
										<a href="#"><img src="images/payment-logo/icon-pay-3.png" alt=""></a>
										<a href="#"><img src="images/payment-logo/icon-pay-4.png" alt=""></a>
										<a href="#"><img src="images/payment-logo/icon-pay-5.png" alt=""></a>
									</p>
									<div class="row">
										<div class="col-sm-6">
											<label>Credit Card Number<span class="required">*</span></label>
											<input type="text" class="form-control">
										</div>
										<div class="col-xs-6 col-sm-3">
											<label>Month<span class="required">*</span></label>
											<div class="select-wrapper">
												<select class="form-control">
													<option value="">01</option>
													<option value="">02</option>
													<option value="">03</option>
													<option value="">04</option>
													<option value="">05</option>
													<option value="">06</option>
													<option value="">07</option>
													<option value="">08</option>
													<option value="">09</option>
													<option value="">10</option>
													<option value="">11</option>
													<option value="">12</option>
												</select>
											</div>
										</div>
										<div class="col-xs-6 col-sm-3">
											<label>Year<span class="required">*</span></label>
											<div class="select-wrapper">
												<select class="form-control">
													<option value="">2017</option>
													<option value="">2018</option>
													<option value="">2019</option>
													<option value="">2020</option>
													<option value="">2021</option>
												</select>
											</div>
										</div>
									</div>
								</form>
							</div> -->
							<div class="col-md-12">
								<h2>Your Order</h2>
								<div class="cart-table cart-table--sm">
									<div class="table-header">
										<div class="photo">
											Product Image
										</div>
										<div class="name">
											Product Name
										</div>
										<div class="price">
									       Color
								        </div>
								        <div class="price">
									       Size
								        </div>
										<div class="price">
											Unit Price
										</div>
										<div class="qty">
											Qty
										</div>
										<div class="subtotal">
											Subtotal
										</div>
                                    </div>
                                    <?php
                                    $uid = $_SESSION['uid'];
                                    $subtotal = 0;
                                    $cart = mysqli_query($con, "select c.id,c.quantity,(c.price * c.quantity) as subtotal,c.price,c.size,c.color,c.image,p.product_name,p.product_photo,p.product_id,p.slug from tbl_cart c, tbl_products p where c.product_id=p.product_id and c.uid='$uid'") or die(mysqli_error());
                                    while($items = mysqli_fetch_array($cart))
                                    {
                                       $subtotal = $subtotal + $items['subtotal'];
                                    ?>
									<div class="table-row">
										<div class="photo">
                                         	<a href="product-detail/<?php echo $items['slug'];?>"><img src="<?php echo $siteurl;?>admin/uploads/productImages/<?php echo $items['image'];?>" alt=""></a>
										</div>
										<div class="name" style="text-align:center">
                                        <a href="product-detail/<?php echo $items['slug'];?>"><?php echo $items['product_name'];?></a>
										</div>
										<div class="price">
                                          <span style="background-color:<?php echo $items['color'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								        </div>
								        <div class="price">
                                          <?php echo $items['size'];?>
								        </div>
										<div class="price">
                                        &#8377;<?php echo $items['price'];?>
										</div>
										<div class="qty">
											<?php echo $items['quantity'];?>
										</div>
										<div class="subtotal">
                                        &#8377;<?php echo $items['subtotal'];?>
										</div>
									</div>
									<?php }?>
								</div>
								<table class="total-price">
									<tr>
										<td>Subtotal</td>
										<td>&#8377;<?php echo number_format($subtotal, 2);?></td>
									</tr>
									<!-- <tr>
										<td>Discount</td>
										<td>$35.00</td>
									</tr> -->
									<tr class="total">
										<td>Grand Total</td>
										<td>&#8377;<?php echo number_format($subtotal, 2);?></td>
									</tr>
									<tr class="total">
									<td colspan="2"><br>
									<?php
									if(isset($_SESSION['username']) && $_SESSION['username']!='')
									{
									?>
										<button style="width:300px" id="btnConfirmOrder" class="btn btn-alt">PLACE ORDER</button>
									<?php }else{?>
										<button style="width:300px" id="btnPlaceOrder" data-toggle="modal" data-target="#Login" class="btn btn-alt">PLACE ORDER</button>
									<?php }?>
									</td>
									</tr>
								</table>
								<div class="clearfix"></div>
								<h2>Order Notes</h2>
								<form class="white" action="#">
									<label>Notes about your order, e.g. special notes for delivery</label>
									<textarea class="form-control"></textarea>
									<!-- <div>
									<?php
									if(isset($_SESSION['username']) && $_SESSION['username']!='')
									{
									?>
										<button id="btnConfirmOrder" class="btn btn-alt">PLACE ORDER</button>
									<?php }else{?>
										<button id="btnPlaceOrder" data-toggle="modal" data-target="#Login" class="btn btn-alt">PLACE ORDER</button>
									<?php }?>
									</div> -->
								</form>

                                <!-- <form action="http://localhost/amodini-studio/charge.php" method="POST">
    <script
        src="https://checkout.razorpay.com/v1/checkout.js"
        data-key="<?php echo $razor_api_key; ?>"
        data-amount="100"
        data-buttontext="PLACE ORDER"
        data-name="PHPExpertise.com"
        data-description="Test Txn with RazorPay"
        data-image="https://your-awesome-site.com/your_logo.jpg"
        data-prefill.name="Harshil Mathur"
        data-prefill.email="support@razorpay.com"
        data-theme.color="#F37254"
    ></script>
    <input type="hidden" value="Hidden Element" name="hidden">
    </form> -->

							</div>
						</div>
					</div>
				</div>
				<div class="divider"></div>
			</main>
			<!-- /Page Content -->
			<!-- Footer -->
            
            <?php include 'includes/footer.php'; ?>
            
			<!-- /Footer -->
		</div>
		<!-- Page Content -->
	</div>
	<!-- ProductStack -->
	<div class="productStack disable hide_on_scroll"> <a href="#" class="toggleStack"><i class="icon icon-cart"></i> (6) items</a>
		<div class="productstack-content">
			<div class="products-list-wrapper">
				<ul class="products-list">
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-10.jpg" alt=""></a> <span class="item-qty">3</span>
						<div class="actions"> <a href="#" class="action edit" title="Edit item"><i class="icon icon-pencil"></i></a> <a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-11.jpg" alt=""></a> <span class="item-qty">3</span>
						<div class="actions"> <a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a> <a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-12.jpg" alt=""></a> <span class="item-qty">3</span>
						<div class="actions"> <a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a> <a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-13.jpg" alt=""></a> <span class="item-qty">3</span>
						<div class="actions"> <a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a> <a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-14.jpg" alt=""></a> <span class="item-qty">3</span>
						<div class="actions"> <a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a> <a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-15.jpg" alt=""></a> <span class="item-qty">3</span>
						<div class="actions"> <a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a> <a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="action-cart">
				<button type="button" class="btn" title="Checkout"> <span>Checkout</span> </button>
				<button type="button" class="btn" title="Go to Cart"> <span>Go to Cart</span> </button>
			</div>
			<div class="total-cart">
				<div class="items-total">Items <span class="count">6</span></div>
				<div class="subtotal">Subtotal <span class="price">2.150</span></div>
			</div>
		</div>
	</div>
	<!-- /ProductStack -->

	<!-- Modal Quick View -->
	<div class="modal quick-view zoom" id="quickView">
		<div class="modal-dialog">
			<div class="modalLoader-wrapper">
				<div class="modalLoader bg-striped"></div>
			</div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button>
			</div>
			<div class="modal-content">
				<iframe></iframe>
			</div>
		</div>
	</div>
	<!-- /Modal Quick View -->

	<!-- jQuery Scripts  -->
    
<?php include 'includes/footerJs.php'; ?>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
$('body').on('click', '#btnConfirmOrder', function(e){
var total_amount =<?php echo $subtotal;?>;
var options = {
"key": "rzp_test_8RitInDMrC3cDw",
"Secret": "7WZk5zirhuF8QgmtI9sDx1iX",
"amount": (100 * total_amount), // 2000 paise = INR 20
"name": "Amonidini Studio",
"description": "Payment",
"image": "<?php echo $siteurl?>amolgoog.jpg",
"handler": function (response){
//alert(JSON.stringify(response));
$.ajax({
url: '<?php echo $siteurl?>payment-process.php',
type: 'post',
//dataType: 'json',
data: {
razorpay_payment_id: response.razorpay_payment_id , total_amount : total_amount
}, 
cache: false,
success: function (msg) {
window.location.href = 'payment-success.php';
}
});
},
"theme": {
"color": "#528FF0"
}
};
var rzp1 = new Razorpay(options);
rzp1.open();
e.preventDefault();
});
</script>

</body>


<!-- Mirrored from big-skins.com/frontend/seiko/html/checkout.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 27 Nov 2020 06:24:21 GMT -->
</html>