<?php 
date_default_timezone_set("Asia/Kolkata");
include'includes/config.php';
if(isset($_SESSION['username'])){
    header('location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Seikō HTML 5 eCommerce Responsive Theme</title>
	<?php include'includes/css.php'; ?>
	</head>

<body class="boxed">
    <!-- Loader -->
    <div id="loader-wrapper">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <!-- Page -->
        <div class="page-wrapper">
            <?php include'includes/head.php'; ?>
            <?php include'includes/menu.php'; ?>
            <!-- Page Content -->
            <main class="page-main">
                <div class="block">
                    <div class="container">
                        <ul class="breadcrumbs">
                            <li><a href="index.php"><i class="icon icon-home"></i></a></li>
                            <li>/<span>Register</span></li>
                        </ul>
                    </div>
                </div>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-md-8">
                                <div class="form-card">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h2>Personal Information</h2>
                                            <span style="float:right; " class="required-text">* Required Fields</span>
                                        </div>
                                    </div>
                                    <form class="white" action="#" method="POST" autocomplete="off">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>First Name<span class="required">*</span></label>
                                                <input type="text" required class="form-control" name="firstname" id="fname"
													placeholder="First Name">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Last Name<span class="required">*</span></label>
                                                <input type="text" required class="form-control" name="lastname" id="lname"
													placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>E-mail<span class="required">*</span></label>
                                                <input type="email" required class="form-control" name="email" id="email"
                                                    placeholder="Email">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Phone Number<span class="required">*</span></label>
                                                <input type="text" required class="form-control" name="phone" id="phone"
                                                    placeholder="Phone Number">
                                            </div>
                                        </div>
                                        <label>Street Address</label>
                                        <input type="text" class="form-control" name="address" id="address"
                                            placeholder="Street Address">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Town / City</label>
                                                <input type="text" class="form-control" name="city" id="city"
                                                    placeholder="Town / City">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>State/Province</label>
                                                <input type="text" class="form-control" id="state" name="state"
                                                    placeholder="State/Province">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label>Country</label>
                                                <input type="text" class="form-control" id="country" name="country"
                                                    placeholder="State/Province">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Postcode / Zip</label>
                                                <input type="text" class="form-control" name="zip" id="zip"
                                                    placeholder="Postcode / Zip">
                                            </div>
										</div>
										<div class="row">
                                            <div class="col-sm-6">
                                                <label>Password<span class="required">*</span></label>
                                                <input type="password" required class="form-control" id="password" name="password"
                                                    placeholder="Password">
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Confirm Password <span class="required">*</span></label>
                                                <input type="text" required class="form-control" name="cpassword" id="cpassword"
                                                    placeholder="Confirm Password">
                                            </div>
                                        </div>
                                        
                                        <div>
										<div class="alert alert-success alert-dismissible" id="success" style="display:none;">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								    </div>
								    <div class="alert alert-danger alert-dismissible" id="error" style="display:none;">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								    </div>
											<button type="button" id="registerNow" class="btn btn-lg">Create Account</button>
										
										<br>
                                            <span  style="float:right;" class="check">Already have an account? <a href="login.php">Sign in</a></span>
											</div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-2">
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <!-- /Page Content -->
            <?php include'includes/footer.php'; ?>
        </div>
        <!-- /Page -->
    </div>
    <?php include'includes/footerJs.php'; ?>
</body>

</html>