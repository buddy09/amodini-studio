<?php 
include 'includes/config.php';
$uid = $_REQUEST['uid'];
$query = mysqli_query($con, "select c.price,(c.price*c.quantity) as total_price,c.size,c.color,c.quantity,c.image,p.product_name from tbl_cart c, tbl_products p where p.product_id=c.product_id and c.uid='$uid'") or die(mysqli_error());
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Order Detail</title>
	<meta name="author" content="SEIKO">
	<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="favicon.ico">
	<link href="js/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="js/vendor/slick/slick.css" rel="stylesheet">
	<link href="js/vendor/magnificpopup/dist/magnific-popup.css" rel="stylesheet">
	<link href="js/vendor/darktooltip/dist/darktooltip.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">
	<link href="fonts/icomoon-reg/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700|Raleway:100,100i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
</head>
<body class="quickview">
	<div class="container-fluid">
		<div class="row">
			<div role="tabpanel" class="tab-pane myaccount-content">
				<h1 class="product-name">Order Detail</h1>
				 <div class="table-responsive">
					<table class="table table-bordered">
					   <tbody id="cartBody">
                       <tr style="background: #14122d;color:#fff;">
                       <th>#</th>
                       <th>Product Image</th>
					   <th>Product Name</th>
					   <th>Color</th>
                       <th>Price</th>
                       <th>Quantity</th>
                       <th>Total Price</th>
                       </tr>
                       <?php 
                       $i = 1;
                       $sub_total = 0;
                       while($row = mysqli_fetch_array($query))
                       {
                           $sub_total = $sub_total + $row['total_price'];
                       ?>
					   <tr>
						  <td style="border: 1px solid #ddd;"><?php echo $i; ?></td>
                          <td style="border: 1px solid #ddd;"><img style="width:100px;height:100px" src="admin/uploads/productImages/<?php echo $row['image'];?>"></td>
						  <td style="border: 1px solid #ddd;"><?php echo $row['product_name']; ?></td>
						  <td style="border: 1px solid #ddd;"><span style="background-color:<?php echo $row['color']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
						  <td style="border: 1px solid #ddd;">₹<?php echo $row['price']; ?></td>
                          <td style="border: 1px solid #ddd;"><?php echo $row['quantity']; ?></td>
                          <td style="border: 1px solid #ddd;">₹<?php echo $row['total_price']; ?></td>
                       </tr>
                       <?php $i = $i +1; }?>
					   </tbody>
					</table>
				 </div>
			  </div>
        </div>
        <div class="row">
			<div class="col-md-3 total-wrapper">
			<table class="total-price">
			<tbody>
            <tr>
			<td>Subtotal</td>
			<td>₹<?php echo number_format($sub_total, 2);?></td>
			</tr>
			<tr class="total">
			<td>Grand Total</td>
			<td>₹<?php echo number_format($sub_total, 2);?></td>
			</tr>
			</tbody></table>
		    </div>
        </div>
	</div>
</body>
<script src="js/vendor/jquery/jquery.js"></script>
<script src="js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="js/vendor/slick/slick.min.js"></script>
<script src="js/vendor/magnificpopup/dist/jquery.magnific-popup.js"></script>
<script src="js/vendor/countdown/jquery.countdown.min.js"></script>
<script src="js/vendor/ez-plus/jquery.ez-plus.js"></script>
<script src="js/vendor/tocca/tocca.min.js"></script>
<script src="js/vendor/darktooltip/dist/jquery.darktooltip.js"></script>
<script src="js/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="js/megamenu.html"></script>
<script src="js/app.js"></script>
</body>

</html>