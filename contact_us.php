<?php
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Contact us</title>
	<?php include 'includes/css.php'; ?>
</head>

<body class="boxed">
	<!-- Loader -->
	<div id="loader-wrapper">
		<div class="cube-wrapper">
			<div class="cube-folding">
				<span class="leaf1"></span>
				<span class="leaf2"></span>
				<span class="leaf3"></span>
				<span class="leaf4"></span>
			</div>
		</div>
	</div>
	<!-- /Loader -->
	
	<div id="wrapper">

		<!-- Page -->
		<div class="page-wrapper">
			<!-- Header -->
			<!-- Header -->
			<?php include 'includes/head.php'; ?>
            <?php include 'includes/menu.php'; ?>
			<!-- /Header -->
			<!-- Sidebar -->
			
			<!-- /Sidebar -->
			<!-- Page Content -->
			<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="icon icon-home"></i></a></li>
							<li>/<span>Contact Us</span></li>
						</ul>
					</div>
				</div>
				<div class="block">
					<div class="container">
						<div class="row">
							<div class="col-sm-5">
								<div class="text-wrapper">
									<h2>Amodini Designer Studio</h2>
									<div class="address-block">
										<ul class="simple-list">
											<li><i class="icon icon-location-pin"></i>Adress: 129 Muktanand Nagar, Gopal Pura Road, Jaipur, Rajasthan, 302018</li>
											<li><i class="icon icon-phone"></i>Phone: 9823xxx</li>
											<li><i class="icon icon-close-envelope"></i>Email: <a href="mailto:support@seiko.com">Seico@example.com</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-sm-7">
								<div class="text-wrapper">
                                    <h2>Contact Information</h2>
                                    <?php 
                                    if(isset($_POST['submit'])){
                                        $name = $_POST['name'];
                                        $email = $_POST['email'];
                                        $msg = $_POST['message'];
                                        $body = "Email: ". $email . "<br>" . "Message: ". $msg;

                                        $to = "info@amodinistudio.in";
                                        $subject = "Enquiry";
                                        $message = $body;

                                        $header = "From:enquiry@amodinistudio.in \r\n";
                                        $header .= "MIME-Version: 1.0\r\n";
                                        $header .= "Content-type: text/html\r\n";

                                        $mail = mail($to,$subject,$message,$header);
                                        if($mail){
                                            echo '<p id="" style="color:green">Your email was send successfully!</p>';
                                        }
                                    }
                                    ?>
									
									<!-- <p id="contactFormError">Error, try to submit this form again.</p> -->
									<form id="contactform" class="contact-form white" action="" name="contactform" method="post">
										<label>Name<span class="required">*</span></label>
										<input type="text" class="form-control input-lg" name="name" required>
										<label>E-mail<span class="required">*</span></label>
										<input type="text" class="form-control input-lg" name="email" required>
										<label>Comment</label>
										<textarea class="form-control input-lg" name="message"></textarea>
										<div>
											<button name="submit" type="submit" class="btn btn-lg" id="submit">Submit</button>
										</div>
										<div class="required-text">* Required Fields</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</main>
			<!-- /Page Content -->
			<!-- Footer -->
			<?php include 'includes/footer.php'; ?>
			<!-- /Footer -->


		</div>
		<!-- /Page -->
	</div>
	<!-- ProductStack -->
	<?php include 'includes/footerJs.php'; ?>
	</div>
</body>


<!-- Mirrored from big-skins.com/frontend/seiko/html/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 27 Nov 2020 06:31:37 GMT -->
</html>