<?php
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
if ($_POST['type'] == 1)
{
    if (empty($_POST["fname"])) {
        echo json_encode(array(
            "statusCode" => 207, "msg" => "First name is required."
        ));  
        exit;
     }
     if (empty($_POST["lname"])) {
        echo json_encode(array(
            "statusCode" => 207, "msg" => "Last name is required."
        ));  
        exit;
     }
     if (empty($_POST["email"])) {
        echo json_encode(array(
            "statusCode" => 207, "msg" => "Email is required."
        ));  
        exit;
     }
     $pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^";  
        if (!preg_match ($pattern, $_POST["email"]) ){ 
        echo json_encode(array(
            "statusCode" => 207, "msg" => "Email is not valid."
        ));  
        exit;
        }
     if (empty($_POST["phone"])) {
        $ErrMsg = "Mobile Number is required";
        echo $ErrMsg;
     }
     if (!preg_match ("/^[0-9]*$/", $_POST["phone"]) ){  
        $ErrMsg = "Only numeric value is allowed.";  
        echo $ErrMsg;  
    }
     if (empty($_POST["password"])) {
        $nameErr = "password is required";
        echo $nameErr; 
     }
    $fname = mysqli_real_escape_string($con, $_POST['fname']);
    $fname     = ucwords(strtolower($fname));
    $lname = mysqli_real_escape_string($con, $_POST['lname']);
    $lname     = ucwords(strtolower($lname));
    $email = mysqli_real_escape_string($con, $_POST['email']);
    $phone = mysqli_real_escape_string($con, $_POST['phone']);
    $phone     = stripslashes($phone);
    $address = mysqli_real_escape_string($con, $_POST['address']);
    $city = mysqli_real_escape_string($con, $_POST['city']);
    $city     = ucwords(strtolower($city));
    $state = mysqli_real_escape_string($con, $_POST['state']);
    $state     = ucwords(strtolower($state));
    $country = mysqli_real_escape_string($con, $_POST['country']);
    $country     = ucwords(strtolower($country));
    $zip = mysqli_real_escape_string($con, $_POST['zip']);
    $password = mysqli_real_escape_string($con, $_POST['password']);
    $passmd=md5($password);
    $ip_address = $_SERVER['REMOTE_ADDR'];

    $duplicate = mysqli_query($con, "SELECT * FROM `tbl_users` WHERE email='$email'");
    if (mysqli_num_rows($duplicate) > 0)
    {
        echo json_encode(array(
            "statusCode" => 204
        ));
    }else{
        $created = date("jS \ F Y h:i A");
        $sql = "INSERT INTO `tbl_users`(`fname`, `lname`, `email`, `phone`, `address`, `city`, `state`, `country`, `zip`, `password`, `pass`, `created`, `ip_address`) VALUES ('$fname','$lname','$email','$phone','$address','$city','$state','$country','$zip','$passmd','$password','$created','$ip_address')";
        if (mysqli_query($con, $sql))
        {
            $_SESSION['username'] = $email;
            echo json_encode(array(
                "statusCode" => 205
            ));
        }else{
            echo json_encode(array(
                "statusCode" => 206
            ));
        }
    }
    mysqli_close($con);
}
if ($_POST['type'] == 2)
{

    $username = mysqli_real_escape_string($con, $_POST['username']);
    $password = md5($_POST['password']);


    $check1 = mysqli_query($con, "SELECT * FROM tbl_users WHERE email='$username' AND password='$password'");
    if (mysqli_num_rows($check1) > 0)
    {
        $r = mysqli_fetch_array($check1);
        $_SESSION['username'] = $r['email'];
        echo json_encode(array(
            "statusCode" => 203
        ));
    }
    else
    {
        echo json_encode(array(
            "statusCode" => 204
        ));
    }

    mysqli_close($con);
}

if ($_POST['type'] == 3)
{

    $iid=$_POST['us_id'];
    $old_password= mysqli_real_escape_string($con,$_POST['old_password']);
    $password1=mysqli_real_escape_string($con,$_POST['newpassword']);
   	$password = md5($password1);
	$pass = $password1;
	
    $result = mysqli_query($con, "SELECT * FROM tbl_users WHERE user_id='$iid'");
    $row = mysqli_fetch_array($result);
    if($_POST["old_password"] == $row["pass"]) {
        mysqli_query($con, "UPDATE tbl_users set `password`='$password', `pass`='$pass' WHERE user_id='$iid'");
        echo json_encode(array(
            "statusCode" => 203
        ));
    } else{
        echo json_encode(array(
            "statusCode" => 204
        ));
    }

    mysqli_close($con);
}

if ($_POST['type'] == 'profile_up')
{

    $iid=$_POST['use_id'];
    $fname=mysqli_real_escape_string($con,$_POST['fname']);
    $lname=mysqli_real_escape_string($con,$_POST['lname']);
    $phone=mysqli_real_escape_string($con,$_POST['phone']);
    $photo = $_FILES['photo']['name'];
    $date = date("jS \ F Y h:i A");
    $s = mysqli_query($con,"SELECT * FROM `tbl_users` WHERE user_id='$iid'");
    $r = mysqli_fetch_array($s);
	if(!empty($photo))
    {   
    $imageFileType = strtolower(pathinfo($photo,PATHINFO_EXTENSION));
    $extensions_arr = array("jpg","jpeg","png","gif");
    if( in_array($imageFileType,$extensions_arr) )
    {
    $uniq_photo = $aadhar_number.'-'.uniqid().'-'.time();
    $ext = pathinfo($photo, PATHINFO_EXTENSION);
    $image = $uniq_photo.'.'.$ext;
    move_uploaded_file($_FILES['photo']['tmp_name'], 'images/users/'.$image);
    }
    else{
        $_SESSION['error'] = 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed.'; 
        header('location: dashboard.php');
        exit;
    }
    }
    else
    {
    $image = $r['photo'];
    }
  
    
       $up =  mysqli_query($con, "UPDATE `tbl_users` SET `fname`='$fname',`lname`='$lname',`phone`='$phone',`photo`='$image',`last_update`='$date' WHERE user_id='$iid'");
        
       if($up)
       {
        $_SESSION['success'] = 'Profile Update Successfull';
    } else{
        $_SESSION['error'] = 'something went wrong.!';
    }

    header('location: dashboard.php');
    exit;
}
if ($_POST['type'] == 5)
{

    $use_id = $_POST['use_id'];
    $address = mysqli_real_escape_string($con, $_POST['address']);
    $city = mysqli_real_escape_string($con, $_POST['city']);
    $city     = ucwords(strtolower($city));
    $state = mysqli_real_escape_string($con, $_POST['state']);
    $state     = ucwords(strtolower($state));
    $country = mysqli_real_escape_string($con, $_POST['country']);
    $country     = ucwords(strtolower($country));
    $zip = mysqli_real_escape_string($con, $_POST['zip']);

    $s_address = mysqli_real_escape_string($con, $_POST['s_address']);
    $s_city = mysqli_real_escape_string($con, $_POST['s_city']);
    $s_city     = ucwords(strtolower($s_city));
    $s_state = mysqli_real_escape_string($con, $_POST['s_state']);
    $s_state     = ucwords(strtolower($s_state));
    $s_country = mysqli_real_escape_string($con, $_POST['s_country']);
    $s_country     = ucwords(strtolower($s_country));
    $s_zip = mysqli_real_escape_string($con, $_POST['s_zip']);
    $date = date("jS \ F Y h:i A");
    $ch = mysqli_query($con, "UPDATE `tbl_users` SET `address`='$address',`city`='$city',`state`='$state',`country`='$country',`zip`='$zip',`s_address`='$s_address',`s_city`='$s_city',`s_state`='$s_state',`s_country`='$s_country',`s_zip`='$s_zip',`last_update`='$date' WHERE user_id='$use_id'");
    if ($ch)
    {
        echo json_encode(array(
            "statusCode" => 203
        ));
    }
    else
    {
        echo json_encode(array(
            "statusCode" => 204
        ));
    }

    mysqli_close($con);
}
if ($_POST['type'] == 'passwordreset')
{

    $useremail = mysqli_real_escape_string($con, $_POST['useremail']);
   
    $mailcheck = mysqli_query($con,"SELECT * FROM `tbl_users` WHERE email='$useremail'");
    $q1 = mysqli_fetch_array($mailcheck);
    if(mysqli_num_rows($mailcheck) > 0){
    $set='123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $codes=substr(str_shuffle($set), 0, 15);
    $emd = md5($useremail);
     $code = $emd.''.$codes;   
    $ch = mysqli_query($con, "UPDATE `tbl_users` SET `reset_code`='$code',`code_sent`='1' WHERE email='$useremail'");
    if ($ch)
    {
        require_once('PHPMailer/class.phpmailer.php');
        $mail = new PHPMailer(true);
        $mail->SMTPDebug = 1;
        $mail->SMTPAuth = true; 
        $mail->SetFrom('no-reply@amodinistudio.in', 'Amodinistudio');
        $mail->AddAddress($useremail);
        $mail->isHTML(true);  // Set email format to HTML
        $bodyContent = '
        <span style="white-space:pre-wrap">Hi '.$q1['fname'].',<br>

        Need to reset your Asana password? Click here: <a href="'.$site_rul.'/password_reset.php?e='.$useremail.'&c='.$code.'" target="_blank"></a><br>

        If you think you received this email by mistake, feel free to ignore it. <br>

        Thanks,<br>The Amodinistudio Team</span>
        ';
        $mail->Subject = 'Password reset instructions';
        $mail->Body    = $bodyContent;
        $mail->Send();
        echo json_encode(array(
            "statusCode" => 203
        ));
    }
    else
    {
        echo json_encode(array(
            "statusCode" => 204
        ));
    }
}else{
    echo json_encode(array(
        "statusCode" => 205
    ));
}

    mysqli_close($con);
}

if($_POST['type']=="add_to_wishlist")
{
    $uniqid = $_SESSION['uid'];
	$product_id=$_POST['product_id'];

    $productDe = mysqli_query($con,"Select * from wishlist_item where product_id='".$product_id."' && uid='".$uniqid."'");
	 $totalRowsaa = mysqli_num_rows($productDe);
	   if($totalRowsaa > 0)
		{
			$query = mysqli_query($con,"delete from wishlist_item where product_id='".$product_id."' && uid='".$uniqid."'") or die(mysqli_query());
            $_SESSION['success'] = 'Product removed to wishlist.';
		}
		else
		{
		$query = mysqli_query($con,"insert into wishlist_item(product_id,uid) values ('$product_id','$uniqid')") or die(mysqli_query());
            $_SESSION['success'] = 'Product added to wishlist.';
        }
	
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
?>