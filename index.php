<?php
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Amodini Designer Studio</title>
	<?php include 'includes/css.php'; ?>
</head>

<body class="loaded fullwidth">
	<!-- Loader -->
	<div id="loader-wrapper">
		<div class="cube-wrapper">
			<div class="cube-folding">
				<span class="leaf1"></span>
				<span class="leaf2"></span>
				<span class="leaf3"></span>
				<span class="leaf4"></span>
			</div>
		</div>
	</div>
	
	<div id="wrapper">
		<!-- Page -->
		<div class="page-wrapper">
			<?php include 'includes/head.php'; ?>
			<?php include 'includes/menu.php'; ?>
			<!-- Page Content -->
			<main class="page-main">
			<?php include 'includes/slider.php'; ?>
				<div class="block">
					<div class="container">
						<!-- Wellcome text -->
						<div class="text-center bottom-space">
							<h2 class="size-lg no-padding">WELCOME TO <span class="logo-font custom-color">Amodini Designer Studio</span> </h2>
							<div class="line-divider"></div>
							<!-- <p class="custom-color-alt">Lorem ipsum dolor sit amet, ex eam mundi populo accusamus, aliquam quaestio petentium te cum.
								<br> Vim ei oblique tacimates, usu cu iudico graeco. Graeci eripuit inimicus vel eu, eu mel unum laoreet splendide, cu integre apeirian has.
							</p> -->
							<?php
                                if(isset($_SESSION['success'])){
                                    echo '
                                    <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <strong>Success!</strong> '.$_SESSION['success'].'
                                    </div>
                                    ';
                                    unset($_SESSION['success']);
                                }
                            ?>
						</div>
						<!-- /Wellcome text -->
					</div>
				</div>
				
				<!-- <div class="block">
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<div class="box style2 bgcolor1 text-center">
									<div class="box-icon"><i class="icon icon-truck"></i></div>
									<h3 class="box-title">FREE SHIPPING</h3>
									<div class="box-text">Free shipping on all orders over $199</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box style2 bgcolor2 text-center">
									<div class="box-icon"><i class="icon icon-dollar"></i></div>
									<h3 class="box-title">MONEY BACK</h3>
									<div class="box-text">100% money back guarantee</div>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="box style2 bgcolor3 text-center">
									<div class="box-icon"><i class="icon icon-help"></i></div>
									<h3 class="box-title">ONLINE SUPPORT</h3>
									<div class="box-text">Service support fast 24/7</div>
								</div>
							</div>
						</div>
					</div>
				</div> -->
				<?php 
				$row = mysqli_query($con, "select *from tbl_products where status='1' and best_seller='1' and is_attribute=1 order by product_id DESC LIMIT 0,8")  or die(mysqli_error());
				if(mysqli_num_rows($row) > 0){
				?>
				<div class="block">
					<div class="container">
					<h1 class="text-center" >best seller </h1>
						
						<div class="products-grid-wrapper isotope-wrapper">
							<div class="products-grid isotope four-in-row product-variant-5">
							   <?php 
								while($data = mysqli_fetch_array($row))
								{
									$pid = $data['product_id'];
									$at = mysqli_query($con, "select *from tbl_attributes where product_id='$pid' and is_default=1") or die(mysqli_error());
									$att = mysqli_fetch_array($at);
									$aid = $att['id'];
									$imgs = mysqli_query($con, "select product_photo from tbl_product_images where attribute_id='$aid' order by imgid desc limit 0,1") or die(mysqli_error());
									$img = mysqli_fetch_array($imgs);
							    ?>
								<!-- Product Item -->
								<div class="product-item large colorvariants category2">
									<div class="product-item-inside">
										<div class="product-item-info">
											<!-- Product Photo -->
											<div class="product-item-photo">
												<!-- Product Label -->
											 <?php 
											 if($data['new_arrival'] =='1')
											 {
											?>
											<div class="product-item-label label-new"><span>New</span></div>
											<?php }?>
												<!-- /Product Label -->
												<?php 
												$dis = $att['discount'];
												if($dis > 0.00)
												{
												?>
												<div class="product-item-label label-sale"><span>-<?php echo $dis;?>%</span></div>
												<?php }?>
												<!-- product main photo -->
												<div class="product-item-gallery-main">
													<a href="#"><img class="product-image-photo" src="admin/uploads/productImages/<?php echo $img['product_photo'];?>" alt=""></a>
													<a href="product-detail/<?php echo $data['slug'];?>/0/0" title="Quick View" class="quick-view-btn"> <i class="icon icon-eye"></i><span>Quick View</span></a>
												</div>
												<!-- /product main photo  -->
												<a href="javascript:void(0);" id="<?php echo $pid;  ?>"
                                                    title="Add to Wishlist"
                                                    class="addToWishlist pdpwishlist no_wishlist">
                                                <?php
                                                $query = mysqli_query($con,"Select * from wishlist_item where product_id='".$pid."' && uid='".$_SESSION['uid']."'");
                                                $totalRows = mysqli_num_rows($query);
                                                if($totalRows > 0)
                                                {
                                                ?>
                                                <i class="icon icon-heart" style="color:red;"></i>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <i class="icon icon-heart"></i>
                                                <?php } ?>

                                                <span>Add to Wishlist</span> </a>
												
												<!-- /Product Actions -->
											</div>
											<!-- /Product Photo -->
											<!-- Product Details -->
											<div class="product-item-details">
												<div class="product-item-name"> <a title="" href="#" class="product-item-link"><?php echo $data['product_name'];?></a></div>
												<div class="product-item-description">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia nonkdni numquam eius modi tempora incidunt ut labore</div>
												<div class="price-box"> <span class="price-container"> <span class="price-wrapper">  <span class="special-price">
												<?php 
												if($dis >0.00)
												{
												?>
												<span class="old-price">&#8377;<?php echo number_format($att['price'], 2);?></span>
												<span class="special-price">&#8377;<?php echo number_format($att['price']-(($att['price']*$att['discount'])/100), 2);?></span> 
												<?php }else{?>
												<span class="special-price">&#8377;<?php echo number_format($att['price'], 2);?></span>
												<?php }?>
												</span> </span>
													</span>
												</div>
											
												<div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>
												<a href="product-detail/<?php echo $data['slug'];?>/0/0"><button class="btn"> <i class="icon icon-cart"></i><span>Add to Cart</span> </button></a>
												<!-- <button class="btn add-cart-item" id="<?php echo $data['product_id'];?>"> <i
                                                        class="icon icon-cart"></i><span>Add to Cart</span> </button>
                                                <input type="hidden" id="hdnpPrice-<?php echo $data['product_id']?>" value="<?php echo $data['product_price']?>"> -->
											</div>
											<!-- /Product Details -->
										</div>
									</div>
								</div>
								<!-- /Product Item -->
                                <?php }?>

							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php 
					$row = mysqli_query($con, "select *from tbl_products where status='1' and new_arrival=1 and is_attribute=1 order by product_id DESC LIMIT 0,8")  or die(mysqli_error());
					if(mysqli_num_rows($row)){
				?>
				<div class="block">
					<div class="container">
					<h1 class="text-center" >New Arrival </h1>
						
						<div class="products-grid-wrapper isotope-wrapper">
							<div class="products-grid isotope four-in-row product-variant-5">
							   <?php 
								while($data = mysqli_fetch_array($row))
								{
									$pid = $data['product_id'];
									$at = mysqli_query($con, "select *from tbl_attributes where product_id='$pid' and is_default=1") or die(mysqli_error());
									$att = mysqli_fetch_array($at);
									$aid = $att['id'];
									$imgs = mysqli_query($con, "select product_photo from tbl_product_images where attribute_id='$aid' order by imgid desc limit 0,1") or die(mysqli_error());
									$img = mysqli_fetch_array($imgs);
							    ?>
								<!-- Product Item -->
								<div class="product-item large colorvariants category2">
									<div class="product-item-inside">
										<div class="product-item-info">
											<!-- Product Photo -->
											<div class="product-item-photo">
											<?php 
											if($data['new_arrival'] =='1')
											{
											?>
											<div class="product-item-label label-new"><span>New</span></div>
											<?php }?>
												<!-- Product Label -->
												<?php 
												$dis = $att['discount'];
												if($dis > 0.00)
												{
												?>
												<div class="product-item-label label-sale"><span>-<?php echo $dis;?>%</span></div>
												<?php }?>
												<!-- /Product Label -->
												<!-- product main photo -->
												<div class="product-item-gallery-main">
													<a href="#"><img class="product-image-photo" src="admin/uploads/productImages/<?php echo $img['product_photo'];?>" alt=""></a>
													<a href="product-detail/<?php echo $data['slug'];?>/0/0" title="Quick View" class="quick-view-btn"> <i class="icon icon-eye"></i><span>Quick View</span></a>
												</div>
												<!-- /product main photo  -->
											
												<!-- Product Actions -->
												<a href="javascript:void(0);" id="<?php echo $pid;  ?>"
                                                    title="Add to Wishlist"
                                                    class="addToWishlist pdpwishlist no_wishlist">
                                                <?php
                                                $query = mysqli_query($con,"Select * from wishlist_item where product_id='".$pid."' && uid='".$_SESSION['uid']."'");
                                                $totalRows = mysqli_num_rows($query);
                                                if($totalRows > 0)
                                                {
                                                ?>
                                                <i class="icon icon-heart" style="color:red;"></i>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <i class="icon icon-heart"></i>
                                                <?php } ?>

                                                <span>Add to Wishlist</span> </a>
											
												<!-- /Product Actions -->
											</div>
											<!-- /Product Photo -->
											<!-- Product Details -->
											<div class="product-item-details">
												<div class="product-item-name"> <a title="" href="product.html" class="product-item-link"><?php echo $data['product_name'];?></a></div>
												<div class="product-item-description">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia nonkdni numquam eius modi tempora incidunt ut labore</div>
												<div class="price-box"> <span class="price-container"> <span class="price-wrapper">
												<?php 
												if($dis >0.00)
												{
												?>
												<span class="old-price">&#8377;<?php echo number_format($att['price'], 2);?></span>
												<span class="special-price">&#8377;<?php echo number_format($att['price']-(($att['price']*$att['discount'])/100), 2);?></span> 
												<?php }else{?>
												<span class="special-price">&#8377;<?php echo number_format($att['price'], 2);?></span>
												<?php }?>
													</span>
													</span>
												</div>
											
												<div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>
												<a href="product-detail/<?php echo $data['slug'];?>/0/0"><button class="btn"> <i class="icon icon-cart"></i><span>Add to Cart</span> </button></a>
												<!-- <button class="btn add-cart-item" id="<?php echo $data['product_id'];?>"> <i
                                                        class="icon icon-cart"></i><span>Add to Cart</span> </button>
                                                <input type="hidden" id="hdnpPrice-<?php echo $data['product_id']?>" value="<?php echo $data['product_price']?>"> -->
											</div>
											<!-- /Product Details -->
										</div>
									</div>
								</div>
								<!-- /Product Item -->
                                <?php }?>
							</div>
						</div>
					</div>
				</div>
				<?php }?>
				
				<div class="block">
				<div class="container">
					<h1 class="text-center" >Deal </h1>
						<?php
						$d = mysqli_query($con, "select d.id,c.category_name,s.sub_category,i.inner_category from tbl_deal d,tbl_category c,tbl_sub_category s,tbl_inner_category i where d.cat_id=c.id and d.sub_cat_id=s.id and d.inner_cat_id=i.id and d.status=1") or die(mysqli_error());
						while($dl = mysqli_fetch_array($d)){
							$deal_id = $dl['id'];
							$r = mysqli_query($con, "select image from tbl_deal_images where deal_id='$deal_id' order by id desc limit 0,1") or die(mysqli_error());
							$rw = mysqli_fetch_array($r);
						?>
						<div><a href="<?php echo $siteurl;?>products/<?php echo strtolower(str_replace(' ','-',$dl['category_name']));?>/<?php echo strtolower(str_replace(' ','-',$dl['sub_category']));?>/<?php echo strtolower(str_replace(' ','-',$dl['inner_category']));?>/1"><img src="admin/uploads/productImages/<?php echo $rw['image'];?>" style="width:100%;height:300px"></a></div><br><br>
						<?php }?>
					</div>
				</div>
				
				<div class="block fullwidth full-nopad">
					<div class="container">
						<div id="instafeed" class="instagramm-feed-full"></div>
						<div class="instagramm-title"></div>
					</div>
				</div>
			</main>
			<!-- /Page Content -->
		<?php include 'includes/footer.php'; ?>
		</div>
		<!-- /Page -->
	</div>
<?php include 'includes/footerJs.php'; ?>
</body>
</html>