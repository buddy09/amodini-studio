-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2020 at 01:10 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amodinistudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `photo` varchar(222) NOT NULL,
  `insert_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `photo`, `insert_at`, `status`) VALUES
(2, '5fd73c9205217-1607941266.jpg', '2020-12-14 15:50:45', '1'),
(4, '5fd73c8b4405e-1607941259.jpg', '2020-12-14 15:50:59', '0');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us_details`
--

CREATE TABLE `contact_us_details` (
  `id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobile` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `state` varchar(250) NOT NULL,
  `pin_code` varchar(250) NOT NULL,
  `photo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us_details`
--

INSERT INTO `contact_us_details` (`id`, `name`, `email`, `mobile`, `address`, `city`, `state`, `pin_code`, `photo`) VALUES
(1, 'मान्यवर कांशीराम राजकीय महाविद्यालय, कासगंज(उ. प्र.)', 'gdckasganj@gmail.com', '7983615379, 8881679571', '8,shanti nekatan colony', 'jaipur', 'rajasthan', '302015', 'logo_5f4cc4935a11b.jfif');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(222) NOT NULL,
  `email` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `pass` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `password`, `pass`, `status`) VALUES
(1, 'Amodinistudio', 'admin@amodinistudio.com', 'e10adc3949ba59abbe56e057f20f883e', '123456', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category_name`, `slug`, `status`) VALUES
(1, 'Men', 'men', 1),
(2, 'Women', 'women', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact_detail`
--

CREATE TABLE `tbl_contact_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_contact_detail`
--

INSERT INTO `tbl_contact_detail` (`id`, `name`, `email`, `phone`, `website`, `status`) VALUES
(1, 'UB Webs Jaipur', 'rajesh@ubwebs.com', '1234567898', 'www.ubwebs.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_inner_category`
--

CREATE TABLE `tbl_inner_category` (
  `id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `inner_category` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_inner_category`
--

INSERT INTO `tbl_inner_category` (`id`, `sub_category_id`, `inner_category`, `slug`, `status`) VALUES
(1, 3, 'inner category one 2 2 3', 'inner-category-one-2-2-3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_materials`
--

CREATE TABLE `tbl_materials` (
  `mid` int(11) NOT NULL,
  `material_name` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_materials`
--

INSERT INTO `tbl_materials` (`mid`, `material_name`, `status`, `modified`, `created`) VALUES
(1, 'Paints', '0', '2020-12-18 12:54:48', '2020-12-16 17:12:31'),
(3, 'new ', '1', '2020-12-18 12:58:33', '2020-12-18 12:48:29'),
(4, 'new material', '1', '0000-00-00 00:00:00', '2020-12-18 12:58:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `product_material` varchar(222) NOT NULL,
  `product_size` varchar(222) NOT NULL,
  `product_name` varchar(222) NOT NULL,
  `product_code` varchar(222) NOT NULL,
  `product_price` varchar(222) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `stock` int(11) NOT NULL,
  `product_stock` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `slug` varchar(222) NOT NULL,
  `meta_title` varchar(222) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `category_id`, `subcat_id`, `product_material`, `product_size`, `product_name`, `product_code`, `product_price`, `short_description`, `product_description`, `stock`, `product_stock`, `created`, `slug`, `meta_title`, `meta_description`, `meta_keywords`, `status`) VALUES
(1, 2, 2, '', '', 'Woman Blouse', 'BL-1011', '500', '<p>Blouse Red Color</p>\\r\\n', '<p>Blouse Red Color</p>\\r\\n', 1, 20, '2020-12-18 16:40:03', 'woman-blouse', '', '', '', '1'),
(3, 2, 2, '', '', 'Blouse Green Color', 'BL-1012', '650', '<p>Blouse Green Color</p>\\r\\n', '<p>Blouse Green Color</p>\\r\\n', 1, 40, '2020-12-18 16:46:06', 'blouse-green-color', '', '', '', '1'),
(4, 2, 2, '', '', 'Blouse Blue Color', 'BL-1013', '700', '<p>Blouse Blue Color</p>\\r\\n', '<p>Blouse Blue Color</p>\\r\\n', 1, 20, '2020-12-18 16:48:13', 'blouse-blue-color', '', '', '', ''),
(5, 2, 2, '', '', 'White Blouse', 'BL-1014', '670', '<p>Blouse White Color</p>\\r\\n', '<p>Blouse White Color</p>\\r\\n', 1, 50, '2020-12-18 16:56:31', 'white-blouse', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product_images`
--

CREATE TABLE `tbl_product_images` (
  `imgid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_photo` varchar(222) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_product_images`
--

INSERT INTO `tbl_product_images` (`imgid`, `product_id`, `product_photo`, `created`) VALUES
(1, 1, '100000129226_MAIN.jpg', '2020-12-18 16:40:03'),
(2, 1, '100000129226_OTHER_1.jpg', '2020-12-18 16:40:03'),
(3, 1, '100000129226_OTHER_2.jpg', '2020-12-18 16:40:03'),
(4, 1, '100000129228_MAIN.jpg', '2020-12-18 16:40:03'),
(5, 1, '100000129228_OTHER_1.jpg', '2020-12-18 16:40:03'),
(6, 3, '100000138769_1_3.jpg', '2020-12-18 16:46:06'),
(7, 3, '100000138769_2_3.jpg', '2020-12-18 16:46:06'),
(8, 3, '100000143501_1_4.jpg', '2020-12-18 16:46:06'),
(9, 3, '100000143501_2_4.jpg', '2020-12-18 16:46:06'),
(10, 3, '100000143501_6.jpg', '2020-12-18 16:46:06'),
(11, 4, 'BL- 1014 Main_image_url.jpeg', '2020-12-18 16:48:13'),
(12, 4, 'BL-1010-32-M-1-2x.jpg', '2020-12-18 16:48:14'),
(13, 4, 'BL-1010-32-M-2-2x.jpg', '2020-12-18 16:48:14'),
(14, 4, 'BL-1010-32-M-3-2x.jpg', '2020-12-18 16:48:14'),
(15, 4, 'BL-1014 Other_image_url1.jpeg', '2020-12-18 16:48:14'),
(16, 4, 'BL-1014 Other_image_url2.jpeg', '2020-12-18 16:48:14'),
(17, 5, 'BL- 1011 Other_image_url2.jpeg', '2020-12-18 16:56:31'),
(18, 5, 'BL-1011 Main_image_url.jpeg', '2020-12-18 16:56:31'),
(19, 5, 'BL-1011 Other_image_url1.jpeg', '2020-12-18 16:56:31'),
(20, 5, 'BL-1011 Other_image_url3.jpeg', '2020-12-18 16:56:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_size`
--

CREATE TABLE `tbl_size` (
  `size_id` int(11) NOT NULL,
  `size_name` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_size`
--

INSERT INTO `tbl_size` (`size_id`, `size_name`, `status`, `modified`, `created`) VALUES
(1, '15', '1', '2020-12-18 14:48:23', '2020-12-16 17:12:18'),
(2, '10', '1', '2020-12-18 14:48:26', '2020-12-18 13:26:13'),
(3, '30', '1', '2020-12-18 14:48:29', '2020-12-18 13:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sub_category`
--

CREATE TABLE `tbl_sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sub_category`
--

INSERT INTO `tbl_sub_category` (`id`, `category_id`, `sub_category`, `slug`, `status`) VALUES
(1, 1, 'Shirts', 'shirts', 1),
(2, 2, 'Blouse', 'blouse', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(222) NOT NULL,
  `lname` varchar(252) NOT NULL,
  `email` varchar(222) NOT NULL,
  `phone` varchar(222) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(222) NOT NULL,
  `state` varchar(222) NOT NULL,
  `country` varchar(222) NOT NULL,
  `zip` varchar(222) NOT NULL,
  `password` varchar(222) NOT NULL,
  `pass` varchar(222) NOT NULL,
  `photo` varchar(222) NOT NULL,
  `created` varchar(222) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `ip_address` varchar(222) NOT NULL,
  `last_update` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `fname`, `lname`, `email`, `phone`, `address`, `city`, `state`, `country`, `zip`, `password`, `pass`, `photo`, `created`, `status`, `ip_address`, `last_update`) VALUES
(1, 'Vishnu', 'Choudhary', 'jatvishnu2822@gmail.com', '8769606272', 'asdasd', 'Asdas', 'Dasd', 'Asdas', 'dasdasd', 'e10adc3949ba59abbe56e057f20f883e', '123456', '-5fd208fff3033-1607600383.jpg', '9th  December 2020 01:04 PM', '1', '::1', '10th  December 2020 05:09 PM');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us_details`
--
ALTER TABLE `contact_us_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contact_detail`
--
ALTER TABLE `tbl_contact_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_inner_category`
--
ALTER TABLE `tbl_inner_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  ADD PRIMARY KEY (`imgid`);

--
-- Indexes for table `tbl_size`
--
ALTER TABLE `tbl_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact_us_details`
--
ALTER TABLE `contact_us_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_contact_detail`
--
ALTER TABLE `tbl_contact_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_inner_category`
--
ALTER TABLE `tbl_inner_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_materials`
--
ALTER TABLE `tbl_materials`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_product_images`
--
ALTER TABLE `tbl_product_images`
  MODIFY `imgid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_size`
--
ALTER TABLE `tbl_size`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_sub_category`
--
ALTER TABLE `tbl_sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
