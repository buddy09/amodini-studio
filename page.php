<?php
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
$page_name = $_REQUEST['param1'];
$pg = mysqli_query($con, "select *from tbl_pages where slug='$page_name' and status=1") or die(mysqli_error());
$page = mysqli_fetch_array($pg);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $page['page_name'];?></title>
	<?php include 'includes/css.php'; ?>
</head>
<body class="boxed">
	<!-- Loader -->
	<div id="loader-wrapper">
		<div class="cube-wrapper">
			<div class="cube-folding">
				<span class="leaf1"></span>
				<span class="leaf2"></span>
				<span class="leaf3"></span>
				<span class="leaf4"></span>
			</div>
		</div>
	</div>
	<!-- /Loader -->
	
	<div id="wrapper">

		<!-- Page -->
		<div class="page-wrapper">
			<!-- Header -->
			<?php include 'includes/head.php'; ?>
            <?php include 'includes/menu.php'; ?>
			<!-- /Header -->
			<!-- Sidebar -->
			
			<!-- /Sidebar -->
			<!-- Page Content -->
			<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="icon icon-home"></i></a></li>
							<li>/<span><?php echo $page['page_name'];?></span></li>
						</ul>
					</div>
				</div>
				<div class="container">
					<!-- Two columns -->
					<div class="row">
						<!-- Left column -->
						<div class="col-md-3 filter-col aside">
							<div class="fixed-wrapper">
								<div class="fixed-scroll">
									<div class="filter-col-content">
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-9 aside">
							<!-- Page Title -->
							<div class="page-title">
								<div class="title center">
									<h1><?php echo $page['page_name'];?></h1>
								</div>
							</div>
							<div class="info-block">
								<p><?php echo $page['description'];?></p>
							</div>
						</div>
					</div>
				</div>
			</main>
			<!-- /Page Content -->
			<!-- Footer -->
			<?php include 'includes/footer.php'; ?>
			<!-- /Footer -->


		</div>
		<!-- /Page -->
	</div>
	<!-- ProductStack -->
    <?php include 'includes/footerJs.php'; ?>
	<!-- /ProductStack -->
	<!-- Modal Quick View -->
	
	<!-- /Modal Quick View -->

	<!-- jQuery Scripts  -->
	

</body>
</html>