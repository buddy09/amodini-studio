<?php include'includes/model.php'; ?>
<!-- jQuery Scripts  -->
<script src="<?php echo $siteurl; ?>js/vendor/jquery/jquery.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/swiper/swiper.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/slick/slick.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/parallax/parallax.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/magnificpopup/dist/jquery.magnific-popup.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/countdown/jquery.countdown.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/nouislider/nouislider.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/ez-plus/jquery.ez-plus.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/tocca/tocca.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/bootstrap-tabcollapse/bootstrap-tabcollapse.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/scrollLock/jquery-scrollLock.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/darktooltip/dist/jquery.darktooltip.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo $siteurl; ?>js/vendor/instafeed/instafeed.min.js"></script>
<script src="<?php echo $siteurl; ?>js/megamenu.min.js"></script>
<script src="<?php echo $siteurl; ?>js/tools.min.js"></script>
<script src="<?php echo $siteurl; ?>js/app.js"></script>
<script src="<?php echo $siteurl; ?>js/plugins/notifications/js/lobibox.min.js"></script>
<script src="<?php echo $siteurl; ?>js/notifications/js/notifications.min.js"></script>
<script src="<?php echo $siteurl; ?>js/notifications/js/notification-custom-script.js"></script>

<script src="<?php echo $siteurl; ?>js/forms.js"></script>

<script>
$(function(){
  $(document).on('click', '.passwordreset22', function(e){
    e.preventDefault();
    $('#Login').modal('hide');
  });
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    $(".delete-item").click(function(e){
        e.preventDefault();
        var res = $(this).attr("id");
        var id = res.split("~");
        var id =id[1];
        $.ajax({
        type: 'POST',
        url: '<?php echo $siteurl;?>ajax_table.php',
        data: {tag:'delete-item', id:id},
       //dataType: 'json',
       cache: false,
       success: function(response){
           window.location.reload();
       }
       });
    });

    $(".edit-item").click(function(){
        var id = $(this).attr("id");
        var id = id.split("~");
        var id = id[1];
        $("#tx-"+id).attr("disabled",false);
        $("#tx-"+id).focus();
        $("#upd-"+id).css("display","block");
    });

    $(".update-cart-item").click(function(e){
        e.preventDefault();
        var id = $(this).attr("id");
        var id = id.split("-");
        var id = id[1];
        var qty = $("#tx-" + id).val();
        alert(id);
        alert(qty);
        $.ajax({
        type: 'POST',
        url: '<?php echo $siteurl;?>ajax_table.php',
        data: {tag:'update-cart-item', id:id, qty:qty},
       cache: false,
       success: function(response){
           window.location.reload();
       }
       });
    });

    $(".update-cart-items").click(function(e){
        e.preventDefault();
        var id = $(this).attr("id");
        var id = id.split("-");
        var id = id[1];
        var qty = $("#txt-" + id).val();
        $.ajax({
        type: 'POST',
        url: '<?php echo $siteurl;?>ajax_table.php',
        data: {tag:'update-cart-item', id:id, qty:qty},
       cache: false,
       success: function(response){
           window.location.reload();
       }
       });
    });

    $("#clear-shopping-cart").click(function(e){
        $.ajax({
        type: 'POST',
        url: '<?php echo $siteurl;?>ajax_table.php',
        data: {tag:'clear-shopping-cart'},
       cache: false,
       success: function(response){
           window.location.reload();
       }
       });
    });

    $(".add-cart-item").click(function(e){
      e.preventDefault();
      var id = $(this).attr("id");
      var price = $("#hdnpPrice-" + id).val();
      $.ajax({
        type: 'POST',
        url: '<?php echo $siteurl;?>ajax_table.php',
        data: {tag:'add-to-cart', product_id:id, price:price},
       cache: false,
       success: function(response){
         alert("product added to cart");
         window.location.reload();
       }
       });
    });
});
</script>

<script>
$("#short_by_price").change(function(){
    var val= $(this).val();
    window.location.href="<?php echo $siteurl;?>short/<?php echo $slug1?>/<?php echo $slug2?>/<?php echo $slug3?>/" + val + "/1";
});

$(".sidebarlink").click(function(e){
  e.preventDefault();
  var href = $(this).attr("href");
  window.location.href = href;
});

$(".size-short").click(function(e){
  e.preventDefault();
  var href = $(this).attr("href");
  window.location.href = href;
});

$(".color-short").click(function(e){
  e.preventDefault();
  var href = $(this).attr("href");
  window.location.href = href;
});
</script>

<script>
$(function(){
  $(document).on('click', '.passwordreset22', function(e){
    e.preventDefault();
    $('#Login').modal('hide');
  });
});
</script>