	<!-- Modal Quick View -->
	<div class="modal quick-view zoom" id="quickView">
		<div class="modal-dialog">
			<div class="modalLoader-wrapper">
				<div class="modalLoader bg-striped"></div>
			</div>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button>
			</div>
			<div class="modal-content">
				<iframe></iframe>
			</div>
		</div>
	</div>
    <!-- /Modal Quick View -->
    
    <!-- ProductStack -->
	<div class="productStack disable hide_on_scroll">
		<a href="#" class="toggleStack"><i class="icon icon-cart"></i> (6) items</a>
		<div class="productstack-content">
			<div class="products-list-wrapper">
				<ul class="products-list">
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-10.jpg" alt=""></a>
						<span class="item-qty">3</span>
						<div class="actions">
							<a href="#" class="action edit" title="Edit item"><i class="icon icon-pencil"></i></a>
							<a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-11.jpg" alt=""></a>
						<span class="item-qty">3</span>
						<div class="actions">
							<a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a>
							<a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-12.jpg" alt=""></a>
						<span class="item-qty">3</span>
						<div class="actions">
							<a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a>
							<a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-13.jpg" alt=""></a>
						<span class="item-qty">3</span>
						<div class="actions">
							<a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a>
							<a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-14.jpg" alt=""></a>
						<span class="item-qty">3</span>
						<div class="actions">
							<a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a>
							<a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
					<li>
						<a href="product.html" title="Product Name Long Name"><img class="product-image-photo" src="images/products/product-15.jpg" alt=""></a>
						<span class="item-qty">3</span>
						<div class="actions">
							<a class="action edit" href="#" title="Edit item"><i class="icon icon-pencil"></i></a>
							<a class="action delete" href="#" title="Delete item"><i class="icon icon-trash-alt"></i></a>
							<div class="edit-qty">
								<input type="number" value="3">
								<button type="button" class="btn">Apply</button>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="action-cart">
				<button type="button" class="btn" title="Checkout"> <span>Checkout</span> </button>
				<button type="button" class="btn" title="Go to Cart"> <span>Go to Cart</span> </button>
			</div>
			<div class="total-cart">
				<div class="items-total">Items <span class="count">6</span></div>
				<div class="subtotal">Subtotal <span class="price">2.150</span></div>
			</div>
		</div>
	</div>
	<!-- /ProductStack -->

	<div class="modal fade zoom" id="Login" data-pause='1500'>
		<div class="modal-dialog">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button>
			</div>
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-12">
							<div class="title text-center"><b>Login</b></div>
									<div class="top-text"></div>
									<div class="alert alert-danger error_log alert-dismissible" id="error_log" style="display:none;">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								    </div>
									<form class="account-create" action="#" method="POST">
										<label>E-mail<span class="required">*</span></label>
										<input type="email" required class="form-control email_log input-lg" name="email" id="email_log" placeholder="E-mail*">
										<label>Password<span class="required">*</span></label>
										<input type="password"  required class="form-control password_log input-lg" name="password" id="password_log" placeholder="Password*">
										<div>
											<button class="btn butlogin" type="button">Login</button><span class="required-text">* Required Fields</span></div>
										<div class="back"><a href="#"  class="passwordreset22" data-toggle="modal" data-target="#passwordreset22">Forgot Your Password?</a> <span style="float:right;">Create a <a href="<?php echo $siteurl; ?>register">New Account</a></span></div>
									</form>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade zoom" id="passwordreset22" data-pause='1500'>
		<div class="modal-dialog">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button>
			</div>
			<div class="modal-content">
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-12">
						<div id="asuccess">
							<div class="title text-center"><b>Forgot your password?</b></div>
									<div class="top-text"></div>
									<div class="alert alert-danger error_log alert-dismissible" id="error_reset" style="display:none;">
								    <a href="#" class="close" dpasswordresetata-dismiss="alert" aria-label="close">&times;</a>
								    </div>
									<form class="account-create" action="#" method="POST">
										<label>E-mail<span class="required">*</span></label>
										<input type="email" required class="form-control input-lg" name="email" id="useremail" placeholder="E-mail*">
										<div>
											<center><button class="btn" id="passwordreset" type="button">Send Me Instructions</button></center>
										</div>
									</form>
							</div>
                         </div>
							<span id="rsuccess"></span>
					</div>
				</div>
			</div>
		</div>
	</div>