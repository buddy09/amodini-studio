<!-- Header -->
<?php
if(isset($_SESSION['username'])){
$useremail = $_SESSION['username'];
$us = mysqli_query($con,"SELECT * FROM `tbl_users` WHERE email='$useremail' LIMIT 1");
$result = mysqli_fetch_array($us);
}
?>
<header class="page-header variant-2 fullboxed sticky smart">
				<div class="navbar">
					<div class="container">
						<!-- Menu Toggle -->
						<div class="menu-toggle"><a href="#" class="mobilemenu-toggle"><i class="icon icon-menu"></i></a></div>
						<!-- /Menu Toggle -->
						<!-- Header Cart -->
						<div class="header-link dropdown-link header-cart variant-1">
						<?php 
						$uid = $_SESSION['uid'];
						$item = mysqli_query($con, "select count(*) as count from tbl_cart where uid='$uid'") or die(mysqli_error());
						$items = mysqli_fetch_array($item);
						$item_count = $items['count'];
						?>
							<a href="<?php echo $siteurl?>cart"> <i class="icon icon-cart"></i> <span class="badge"><?php echo $item_count;?></span> </a>
							<!-- minicart wrapper -->
							<div class="dropdown-container right">
								<!-- minicart content -->
								<div class="block block-minicart">
									<div class="minicart-content-wrapper">
										<div class="block-title">
											<span>Recently added item(s)</span>
										</div>
										<?php if($item_count == 0){?>
										<div class="block-title">
										<br><br>
											<span><b>Your cart is empty....</b></span>
										</div>
										<?php }?>
										<a class="btn-minicart-close" title="Close">&#10060;</a>
										<div class="block-content">
											<div class="minicart-items-wrapper overflowed">
												<ol class="minicart-items">
												<?php 
												$uid = $_SESSION['uid'];
												$total_price = 0 ;
												$cart = mysqli_query($con, "select c.id,c.quantity,(c.price * c.quantity) as price,c.size,c.color,c.image,p.product_name,p.product_id,p.product_photo from tbl_cart c, tbl_products p where c.product_id=p.product_id and c.uid='$uid'") or die(mysqli_error());
												while($items = mysqli_fetch_array($cart))
												{
													$total_price = $total_price + $items['price'];
												?>
													<li class="item product product-item">
														<div class="product">
															<a class="product-item-photo" href="#" title="Long sleeve overall">
																<span class="product-image-container">
																<span class="product-image-wrapper">
																<img class="product-image-photo" src="<?php echo $siteurl;?>admin/uploads/productImages/<?php echo $items['image'];?>" alt="<?php echo $items['product_name'];?>">
																</span>
																</span>
															</a>
															<div class="product-item-details">
																<div class="product-item-name">
																	<a href="#"><?php echo $items['product_name'];?></a><br>
																	<span>size: <?php echo $items['size'];?></span><br>
																	color: <span style="background-color:<?php echo $items['color'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
																</div>
																<div class="product-item-qty">
																	<label class="label">Qty</label>
																	<input style="border:1px solid" disabled id="tx-<?php echo $items['id'];?>" class="item-qty cart-item-qty" maxlength="12" value="<?php echo $items['quantity'];?>">
																	<a href="#" style="display:none" class="update-cart-item" id="upd-<?php echo $items['id'];?>"><img style="width:16px" src="<?php echo $siteurl;?>images/success-icon.png"></a>
																</div>
																<div class="product-item-pricing">
																	<div class="price-container">
																		<span class="price-wrapper">
																			<span class="price-excluding-tax">
																			<span class="minicart-price">
																			<span class="price">&#8377;<?php echo number_format($items['price'], 2);?></span> </span>
																		</span>
																		</span>
																	</div>
																	<div class="product actions">
																		<div class="secondary">
																			<a href="#" id="delete~<?php echo $items['id'];?>" class="action delete delete-item" title="Remove item">
																				<span>Delete</span>
																			</a>
																		</div>
																		<div class="primary">
																			<a id="edit~<?php echo $items['id'];?>" class="action edit edit-item" href="#" title="Edit item">
																				<span>Edit</span>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</li>
													<?php }?>
												</ol>
											</div>
											<div class="subtotal">
												<span class="label">
													<span>Subtotal</span>
												</span>
												<div class="amount price-container">
													<span class="price-wrapper"><span class="price">&#8377;<?php echo number_format($total_price, 2);?></span></span>
												</div>
											</div>
											<div class="actions">
												<div class="secondary">
												<?php if($item_count > 0){?>
													<a href="<?php echo $siteurl;?>cart" class="btn btn-alt">
														<i class="icon icon-cart"></i><span>View and edit cart</span>
													</a>
												<?php } else{?>	
													<a href="#" disabled class="btn btn-alt">
														<i class="icon icon-cart"></i><span>View and edit cart</span>
													</a>
												<?php }?>
												</div>
												<div class="primary">
												<?php if($item_count > 0){?>
													<a class="btn" href="<?php echo $siteurl;?>checkout">
														<i class="icon icon-external-link"></i><span>Go to Checkout</span>
													</a>
												<?php } else {?>
													<a class="btn" disabled href="#">
														<i class="icon icon-external-link"></i><span>Go to Checkout</span>
													</a>
												<?php }?>
													
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /minicart content -->
							</div>
							<!-- /minicart wrapper -->
						</div>
						<!-- /Header Cart -->
						<!-- Header Links -->
						<div class="header-links">
							<!-- Header Language -->
							<!-- <div class="header-link header-select dropdown-link header-language">
								<a href="#"><img src="images/flags/in.png" alt /></a>
								<ul class="dropdown-container">
									<li class="active">
										<a href="#"><img src="images/flags/in.png" alt />India</a>
									</li>
									
								</ul>
							</div> -->
							<!-- /Header Language -->
							
								<!-- Header Account -->
							<div class="header-link dropdown-link header-account">
							<?php if(isset($_SESSION['username'])){ ?>
							
								<a class="text-white" href="<?php echo $siteurl; ?>dashboard">

							<?php if($result['photo']){ ?>
								<img class="img-circle" alt="" title="" style="height: 32px; width: 32px;box-shadow: 0px 0px 14px 2px rgb(255, 255)" src="<?php echo $siteurl; ?>images/users/<?php echo $result['photo']; ?>">
							  <?php
							  }else{
							  ?>
								<img class="img-circle" alt="user" title="user" src="<?php echo $siteurl; ?>images/users/avatar.jpg" style="height: 32px; width: 32px;box-shadow: 0px 0px 14px 2px rgb(255, 255)">
							  <?php
							  }
							  ?>

				  My Profile</a>

				  <!-- <ul class="dropdown-container">
											<li><a href="#"><span class="symbol">€</span>EUR</a></li>
											<li class="active"><a href="#"><span class="symbol">$</span>USD</a></li>
											<li><a href="#"><span class="symbol">£</span>GBP</a></li>
										</ul> -->
				  
							  
                
				<?php }else{ ?>
					<a href="#"><i class="icon icon-user"  data-toggle="modal" data-target="#Login"></i></a>
				<?php }?>
								
								<!-- <div class="dropdown-container right">
									<div class="title">Registered Customers</div>
									<div class="top-text">If you have an account with us, please log in.</div>
								
									<div class="alert alert-danger alert-dismissible" id="error_log" style="display:none;">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								    </div>
									<form action="#">
										<input type="email" class="form-control" name="email" id="email_log1" placeholder="E-mail*">
										<input type="password" class="form-control" name="password" id="password_log1" placeholder="Password*">
										<button type="button" id="butlogin" class="btn">Sign in</button>
									</form>
									
									<div class="title">OR</div>
									<div class="bottom-text">Create a <a href="register.php">New Account</a></div>
								</div> -->
							</div>
							<!-- /Header Account -->
						</div>
						<!-- /Header Links -->
						<!-- Header Search -->
						<div class="header-link header-search header-search">
							<div class="exp-search">
								<form>
									<input class="exp-search-input " placeholder="Search here ..." type="text" value="">
									<input class="exp-search-submit" type="submit" value="">
									<span class="exp-icon-search"><i class="icon icon-magnify"></i></span>
									<span class="exp-search-close"><i class="icon icon-close"></i></span>
								</form>
							</div>
						</div>
						<!-- /Header Search -->
						<!-- Logo -->
						<div class="header-logo">
							<a href="<?php echo $siteurl; ?>" title="Logo">
								<img src="<?php echo $siteurl; ?>amolgoog.jpg">
							</a>
						</div>
						<!-- /Logo -->
						<!-- Mobile Menu -->
						<div class="mobilemenu dblclick">
							<div class="mobilemenu-header">
								<div class="title">MENU</div>
								<a href="#" class="mobilemenu-toggle"></a>
							</div>
							<div class="mobilemenu-content">
								<ul class="nav">
									<li><a href="#">HOME</a><span class="arrow"></span>
										<ul>
											<li> <a href="#" title="">Default</a> </li>
											<li> <a href="#" title="">White Background</a> </li>
											<li> <a href="#" title="">Wide + Side Panel</a> </li>
											<li> <a href="#" title="">Classic</a> </li>
											<li> <a href="#" title="">Journal<span class="menu-label">new look</span></a> </li>
											<li> <a href="#" title="">Banners Boom</a> </li>
											<li> <a href="#" title="">Fullscreen Slider</a> </li>
											<li> <a href="#" title="">Amason</a> </li>
											<li> <a href="#" title="">Lookbook</a> </li>
											<li> <a href="#" title="">RTL</a> </li>
											<li> <a href="#" title="">Popup on Load</a> </li>
										</ul>
									</li>
									<li>
										<a href="#" title="">Pages</a><span class="arrow"></span>
										<ul>
											<li>
												<a href="#" title="">Listing<span class="menu-label-alt">NEW FEATURES</span></a><span class="arrow"></span>
												<ul>
													<li><a href="#" title="">Classic Listing</a>
													</li>
													<li><a href="#" title="">Listing Fixed Filter<span class="menu-label-alt">NEW </span></a>
													</li>
													<li><a href="#" title="">Listing No Filter</a></li>
													<li><a href="#" title="">Empty Category</a></li>
													<li><a href="#" title="">Products per row</a><span class="arrow"></span>
														<ul>
															<li> <a href="#" title="">2 per row</a></li>
															<li> <a href="#" title="">3 per row</a></li>
															<li> <a href="#" title="">4 per row</a></li>
															<li> <a href="#" title="">5 per row</a></li>
														</ul>
													</li>
												</ul>
											</li>
											<li>
												<a href="#" title="">Product</a><span class="arrow"></span>
												<ul>
													<li> <a href="#" title="">Classic design</a><span class="arrow"></span>
														<ul>
															<li> <a href="#" title="">Image small</a></li>
															<li> <a href="#" title="">Image medium</a></li>
															<li> <a href="#" title="">Image medium plus</a></li>
															<li> <a href="#" title="">Image large</a></li>
														</ul>
													</li>
													<li> <a href="#" title="">Creative design</a> </li>
												</ul>
											</li>
											<li>
												<a href="#" title="">Headers</a><span class="arrow"></span>
												<ul>
													<li> <a href="#" title="">Header 1 (one row shift)</a> </li>
													<li> <a href="#" title="">Header 2 (one row)</a> </li>
													<li> <a href="#" title="">Header 3 (one row dark)</a> </li>
													<li> <a href="#" title="">Header 4 (three rows)</a> </li>
													<li> <a href="#" title="">Header 5 (two rows)</a> </li>
													<li> <a href="#" title="">Header 6 (two rows center)</a> </li>
													<li> <a href="#" title="">Header 7 (three row)</a> </li>
													<li> <a href="#" title="">Header 8 (department)</a> </li>
													<li> <a href="#" title="">Header 9 (creative)</a> </li>
												</ul>
											</li>
											<li>
												<a href="#" title="">Footers</a><span class="arrow"></span>
												<ul>
													<li> <a href="#" title="">Footer 1 (simple)</a> </li>
													<li> <a href="#" title="">Footer 2 (links)</a> </li>
													<li> <a href="#" title="">Footer 3 (menu)</a> </li>
													<li> <a href="#" title="">Footer 4 (advanced)</a> </li>
												</ul>
											</li>
											<li>
												<a href="#" title="">Gallery</a><span class="arrow"></span>
												<ul>
													<li> <a href="#" title="">Gallery 2 in row</a> </li>
													<li> <a href="#" title="">Gallery 3 in row</a> </li>
													<li> <a href="#" title="">No isotope Gallery</a> </li>
												</ul>
											</li>
											<li>
												<a href="#" title="">Blog</a>
												<ul>
													<li> <a href="#" title="">List+Sidebar</a> </li>
													<li> <a href="#" title="">Grid 2</a> </li>
													<li> <a href="#" title="">Grid 3</a> </li>
													<li> <a href="#" title="">Grid 4</a> </li>
													<li> <a href="#" title="">Single Post</a> </li>
												</ul>
											</li>
											<li>
												<a href="#" title="">Pages</a><span class="arrow"></span>
												<ul>
													<li> <a href="#" title="">Faq</a> </li>
													<li> <a href="#" title="">About Us</a> </li>
													<li> <a href="#" title="">Contact Us</a> </li>
													<li> <a href="#" title="">404</a> </li>
													<li> <a href="#" title="">Typography</a> </li>
													<li> <a href="#" title="">Coming soon</a> </li>
													<li> <a href="#" title="">Login</a> </li>
													<li> <a href="#" title="">Account</a> </li>
													<li> <a href="#" title="">Cart</a> </li>
													<li> <a href="#" title="">Empty Cart</a> </li>
													<li> <a href="#" title="">Wishlist</a> </li>
												</ul>
											</li>
											<li> <a href="#" title="">Banners / Grid Editor<span class="menu-label-alt">EXCLUSIVE</span></a> </li>
										</ul>
									</li>
									<li><a href="#">Men</a></li>
									<li><a href="#">Women</a></li>
									<li><a href="#">Electronics</a></li>
								</ul>
							</div>
						</div>
						<!-- Mobile Menu -->
						<!-- Mega Menu -->
						<div class="megamenu fadein blackout">
						<ul class="nav">
                            	<li><a href="<?php echo $siteurl;?>home" title=""><i class="icon icon-home"></i></a></li>
								<li class="mega-dropdown">
									<a href="<?php echo $siteurl;?>home">Home</a>
								</li>
								<?php 
								$m1 = mysqli_query($con , "select *from tbl_category") or die(mysqli_error());
								while($m11 = mysqli_fetch_array($m1))
								{
								?>
								<li class="simple-dropdown">
									<a href="#" title=""><?php echo $m11['category_name'];?></a>
									<div class="sub-menu">
										<ul class="category-links">
											<?php 
											$cat_id = $m11['id'];
											$m2 = mysqli_query($con, "select *from tbl_sub_category where category_id='$cat_id'") or die(mysqli_error());
											while($m22 = mysqli_fetch_array($m2))
											{
											?>
											<li>
												<a href="#" title=""><?php echo $m22['sub_category'];?></a>
												<ul>
													<?php 
													$subcat_id = $m22['id'];
													$m3 = mysqli_query($con, "select *from tbl_inner_category where sub_category_id='$subcat_id'") or die(mysqli_error());
													while($m33 = mysqli_fetch_array($m3))
													{
													?>
													<li> <a href="<?php echo $siteurl;?>products/<?php echo $m11['slug'];?>/<?php echo $m22['slug'];?>/<?php echo $m33['slug'];?>/1" title=""><?php echo $m33['inner_category'];?></a></li>
													<?php }?>
												</ul>
											</li>
											<?php }?>
										</ul>
									</div>
								</li>
								<?php }?>
							</ul>
						</div>
						<!-- /Mega Menu -->
					</div>
				</div>
			</header>
			<!-- /Header -->