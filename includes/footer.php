<!-- Footer -->
<footer class="page-footer variant4 fullboxed" style="background: #e6e6e6;">
				
				<div class="footer-middle">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-lg-3">
								<div class="footer-block collapsed-mobile">
									<div class="title">
										<h4>INFORMATION</h4>
										<div class="toggle-arrow"></div>
									</div>
									<div class="collapsed-content">
										<ul class="marker-list">
											<li><a href="<?php echo $siteurl;?>page/about-us">About Us</a></li>
											<li><a href="<?php echo $siteurl;?>page/privacy-policy">Privacy Policy</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3">
								<div class="footer-block collapsed-mobile">
									<div class="title">
										<h4>MY ACCOUNT</h4>
										<div class="toggle-arrow"></div>
									</div>
									<div class="collapsed-content">
										<ul class="marker-list">
											<?php
											if(isset($_SESSION['username']))
											{
											?>
											<li><a href="<?php echo $siteurl;?>dashboard">My account</a></li>
											<li><a href="<?php echo $siteurl;?>cart">My cart</a></li>
											<li><a href="<?php echo $siteurl;?>dashboard">My wishlist</a></li>
											<?php }else{?>
											<li><a href="#" data-toggle="modal" data-target="#Login">My account</a></li>
											<li><a href="<?php echo $siteurl;?>cart">My cart</a></li>
											<li><a href="#" data-toggle="modal" data-target="#Login">Login</a></li>
											<li><a href="#" data-toggle="modal" data-target="#Login">My wishlist</a></li>
											<?php }?>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3">
								<div class="footer-block collapsed-mobile">
									<div class="title">
										<h4>CUSTOMER CARE</h4>
										<div class="toggle-arrow"></div>
									</div>
									<div class="collapsed-content">
										<ul class="marker-list">
											<li><a href="<?php echo $siteurl;?>contact-us">Contact Us</a></li>
											<li><a href="<?php echo $siteurl;?>page/privacy-policy">Privacy Policy</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3 col-lg-3">
								<div class="footer-block collapsed-mobile">
									<div class="title">
										<h4>CONTACT US</h4>
										<div class="toggle-arrow"></div>
									</div>
									<?php 
									$contact = mysqli_query($con, "select *from tbl_contact_detail") or die(mysqli_error());
									$cont = mysqli_fetch_array($contact);
									?>
									<div class="collapsed-content">
										<ul class="simple-list">
											<li><i class="icon icon-phone"></i><?php echo $cont['phone'];?></li>
											<li><i class="icon icon-close-envelope"></i><a href="mailto:<?php echo $cont['email'];?>"><?php echo $cont['email'];?></a></li>
											<li><i class="icon icon-clock"></i><?php echo $cont['time'];?></li>
										</ul>
										<div class="footer-social">
											<a href="<?php echo $cont['facebook'];?>" target="_blank"><i class="icon icon-facebook-logo icon-circled"></i></a>
											<a href="<?php echo $cont['instagram'];?>" target="_blank"><i class="icon icon-instagram icon-circled"></i></a> 
											<!-- <a href="#"><i class="icon icon-linkedin-logo icon-circled"></i></a> <a href="#"><i class="icon icon-vimeo icon-circled"></i></a> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-bot">
					<div class="container">
						<div class="row">
							<div class="col-md-6 col-lg-8">
								<div class="image-banner text-center">
								<div class="footer-copyright text-left"> © 2020 <a href="http://amodinistudio.in/">Amodinidesignerstudio</a>. All Rights Reserved. </div>
								</div>
							</div>
							<div class=" col-md-6 col-lg-4">
								<!-- <div class="footer-copyright text-left"> © 2016 Demo Store. All Rights Reserved. </div> -->
								<div class="footer-payment-link text-center">
									<a href="#"><img src="images/payment-logo/icon-pay-1.png" alt=""></a>
									<a href="#"><img src="images/payment-logo/icon-pay-2.png" alt=""></a>
									<a href="#"><img src="images/payment-logo/icon-pay-3.png" alt=""></a>
									<a href="#"><img src="images/payment-logo/icon-pay-4.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- /Footer -->
			<a class="back-to-top back-to-top-mobile" href="#">
				<i class="icon icon-angle-up"></i> To Top
			</a>