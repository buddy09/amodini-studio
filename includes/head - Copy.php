<!-- Header -->
<?php
if(isset($_SESSION['username'])){
$useremail = $_SESSION['username'];
$us = mysqli_query($con,"SELECT * FROM `tbl_users` WHERE email='$useremail' LIMIT 1");
$result = mysqli_fetch_array($us);
}
?>
<header class="page-header variant-2 fullboxed sticky smart">
				<div class="navbar">
					<div class="container">
						<!-- Menu Toggle -->
						<div class="menu-toggle"><a href="#" class="mobilemenu-toggle"><i class="icon icon-menu"></i></a></div>
						<!-- /Menu Toggle -->
						<!-- Header Cart -->
						<div class="header-link dropdown-link header-cart variant-1">
						<?php 
						$uid = $_SESSION['uid'];
						$item = mysqli_query($con, "select count(*) as count from tbl_cart where uid='$uid'") or die(mysqli_error());
						$items = mysqli_fetch_array($item);
						$item_count = $items['count'];
						?>
							<a href="<?php echo $siteurl?>cart"> <i class="icon icon-cart"></i> <span class="badge"><?php echo $item_count;?></span> </a>
							<!-- minicart wrapper -->
							<div class="dropdown-container right">
								<!-- minicart content -->
								<div class="block block-minicart">
									<div class="minicart-content-wrapper">
										<div class="block-title">
											<span>Recently added item(s)</span>
										</div>
										<?php if($item_count == 0){?>
										<div class="block-title">
										<br><br>
											<span><b>Your cart is empty....</b></span>
										</div>
										<?php }?>
										<a class="btn-minicart-close" title="Close">&#10060;</a>
										<div class="block-content">
											<div class="minicart-items-wrapper overflowed">
												<ol class="minicart-items">
												<?php 
												$uid = $_SESSION['uid'];
												$total_price = 0 ;
												$cart = mysqli_query($con, "select c.id,c.quantity,(c.price * c.quantity) as price,p.product_name,p.product_id from tbl_cart c, tbl_products p where c.product_id=p.product_id and c.uid='$uid'") or die(mysqli_error());
												while($items = mysqli_fetch_array($cart))
												{
													$total_price = $total_price + $items['price'];
												?>
													<li class="item product product-item">
														<div class="product">
															<a class="product-item-photo" href="#" title="Long sleeve overall">
																<span class="product-image-container">
																<span class="product-image-wrapper">
																<?php 
																$product_id = $items['product_id'];
																$itemImage = mysqli_query($con, "select product_photo from tbl_product_images where product_id='$product_id' order by '$product_id' DESC LIMIT 0,1") or die(mysqli_error());
																$itemImg = mysqli_fetch_array($itemImage);
																?>
																<img class="product-image-photo" src="<?php echo $siteurl;?>admin/uploads/productImages/<?php echo $itemImg['product_photo'];?>" alt="Long sleeve overall">
																</span>
																</span>
															</a>
															<div class="product-item-details">
																<div class="product-item-name">
																	<a href="#"><?php echo $items['product_name'];?></a>
																</div>
																<div class="product-item-qty">
																	<label class="label">Qty</label>
																	<input style="border:1px solid" disabled id="tx-<?php echo $items['id'];?>" class="item-qty cart-item-qty" maxlength="12" value="<?php echo $items['quantity'];?>">
																	<a href="#" style="display:none" class="update-cart-item" id="upd-<?php echo $items['id'];?>"><img style="width:16px" src="<?php echo $siteurl;?>images/success-icon.png"></a>
																</div>
																<div class="product-item-pricing">
																	<div class="price-container">
																		<span class="price-wrapper">
																			<span class="price-excluding-tax">
																			<span class="minicart-price">
																			<span class="price">&#8377;<?php echo number_format($items['price'], 2);?></span> </span>
																		</span>
																		</span>
																	</div>
																	<div class="product actions">
																		<div class="secondary">
																			<a href="#" id="delete~<?php echo $items['id'];?>" class="action delete delete-item" title="Remove item">
																				<span>Delete</span>
																			</a>
																		</div>
																		<div class="primary">
																			<a id="edit~<?php echo $items['id'];?>" class="action edit edit-item" href="#" title="Edit item">
																				<span>Edit</span>
																			</a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</li>
													<?php }?>
												</ol>
											</div>
											<div class="subtotal">
												<span class="label">
													<span>Subtotal</span>
												</span>
												<div class="amount price-container">
													<span class="price-wrapper"><span class="price">&#8377;<?php echo number_format($total_price, 2);?></span></span>
												</div>
											</div>
											<div class="actions">
												<div class="secondary">
												<?php if($item_count > 0){?>
													<a href="<?php echo $siteurl;?>cart" class="btn btn-alt">
														<i class="icon icon-cart"></i><span>View and edit cart</span>
													</a>
												<?php } else{?>	
													<a href="#" disabled class="btn btn-alt">
														<i class="icon icon-cart"></i><span>View and edit cart</span>
													</a>
												<?php }?>
												</div>
												<div class="primary">
												<?php if($item_count > 0){?>
													<a class="btn" href="<?php echo $siteurl;?>checkout">
														<i class="icon icon-external-link"></i><span>Go to Checkout</span>
													</a>
												<?php } else {?>
													<a class="btn" disabled href="#">
														<i class="icon icon-external-link"></i><span>Go to Checkout</span>
													</a>
												<?php }?>
													
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /minicart content -->
							</div>
							<!-- /minicart wrapper -->
						</div>
						<!-- /Header Cart -->
						<!-- Header Links -->
						<div class="header-links">
							<!-- Header Language -->
							<!-- <div class="header-link header-select dropdown-link header-language">
								<a href="#"><img src="images/flags/in.png" alt /></a>
								<ul class="dropdown-container">
									<li class="active">
										<a href="#"><img src="images/flags/in.png" alt />India</a>
									</li>
									
								</ul>
							</div> -->
							<!-- /Header Language -->
							
								<!-- Header Account -->
							<div class="header-link dropdown-link header-account">
							<?php if(isset($_SESSION['username'])){ ?>
							
								<a class="text-white" href="<?php echo $siteurl; ?>dashboard">

							<?php if($result['photo']){ ?>
								<img class="img-circle" alt="" title="" style="height: 32px; width: 32px;box-shadow: 0px 0px 14px 2px rgb(255, 255)" src="<?php echo $siteurl; ?>images/users/<?php echo $result['photo']; ?>">
							  <?php
							  }else{
							  ?>
								<img class="img-circle" alt="user" title="user" src="<?php echo $siteurl; ?>images/users/avatar.jpg" style="height: 32px; width: 32px;box-shadow: 0px 0px 14px 2px rgb(255, 255)">
							  <?php
							  }
							  ?>

				  My Profile</a>

				  <!-- <ul class="dropdown-container">
											<li><a href="#"><span class="symbol">€</span>EUR</a></li>
											<li class="active"><a href="#"><span class="symbol">$</span>USD</a></li>
											<li><a href="#"><span class="symbol">£</span>GBP</a></li>
										</ul> -->
				  
							  
                
				<?php }else{ ?>
					<a href="#"><i class="icon icon-user"  data-toggle="modal" data-target="#Login"></i></a>
				<?php }?>
								
								<!-- <div class="dropdown-container right">
									<div class="title">Registered Customers</div>
									<div class="top-text">If you have an account with us, please log in.</div>
								
									<div class="alert alert-danger alert-dismissible" id="error_log" style="display:none;">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								    </div>
									<form action="#">
										<input type="email" class="form-control" name="email" id="email_log1" placeholder="E-mail*">
										<input type="password" class="form-control" name="password" id="password_log1" placeholder="Password*">
										<button type="button" id="butlogin" class="btn">Sign in</button>
									</form>
									
									<div class="title">OR</div>
									<div class="bottom-text">Create a <a href="register.php">New Account</a></div>
								</div> -->
							</div>
							<!-- /Header Account -->
						</div>
						<!-- /Header Links -->
						<!-- Header Search -->
						<div class="header-link header-search header-search">
							<div class="exp-search">
								<form>
									<input class="exp-search-input " placeholder="Search here ..." type="text" value="">
									<input class="exp-search-submit" type="submit" value="">
									<span class="exp-icon-search"><i class="icon icon-magnify"></i></span>
									<span class="exp-search-close"><i class="icon icon-close"></i></span>
								</form>
							</div>
						</div>
						<!-- /Header Search -->
						<!-- Logo -->
						<div class="header-logo">
							<a href="<?php echo $siteurl; ?>" title="Logo">
								<img src="<?php echo $siteurl; ?>amolgoog.jpg">
							</a>
						</div>
						<!-- /Logo -->
						<!-- Mobile Menu -->
						<div class="mobilemenu dblclick">
							<div class="mobilemenu-header">
								<div class="title">MENU</div>
								<a href="#" class="mobilemenu-toggle"></a>
							</div>
							<div class="mobilemenu-content">
							<ul class="nav">
								  <li><a href="<?php echo $siteurl; ?>">Home</a></li>
								  <?php 
									$cat = mysqli_query($con, "select *from tbl_category where status='1' LIMIT 5");
                                    while($category = mysqli_fetch_array($cat))
                                    {
										$cat_id = $category['id'];
										$scat = mysqli_query($con, "select * from tbl_sub_category where category_id = '$cat_id' AND status='1'");
										if(mysqli_num_rows($scat) !='0'){
                                    ?>
									<li><a href="#"><?php echo $category['category_name'];?></a><span class="arrow"></span>
										<ul>
										<?php 
                                                while($sub_category = mysqli_fetch_array($scat))
                                                {
                                                ?>
											<li> <a href="<?php echo $siteurl; ?>products/<?php echo $category['slug'];?>/<?php echo $sub_category['slug'];?>/1" title=""><?php echo $sub_category['sub_category'];?></a> </li>
											<?php }?>
										</ul>
									</li>
									<?php }else{ ?>
									<li><a href="<?php echo $category['slug'];?>" title=""><?php echo $category['category_name'];?></a></li>
								<?php }?>
								<?php } ?>
								</ul>
							</div>
						</div>
						<!-- Mobile Menu -->
						<!-- Mega Menu -->
						<div class="megamenu fadein blackout">
                            <ul class="nav">
                            	<li><a href="<?php echo $siteurl; ?>" title=""><i class="icon icon-home"></i></a></li>
								<?php 
									$cat = mysqli_query($con, "select *from tbl_category where status='1' LIMIT 5");
                                    while($category = mysqli_fetch_array($cat))
                                    {
										$cat_id = $category['id'];
										$scat = mysqli_query($con, "select * from tbl_sub_category where category_id = '$cat_id' AND status='1'");
										if(mysqli_num_rows($scat) !='0'){
                                    ?>
								<li class="simple-dropdown">
									<a href="#" title=""><?php echo $category['category_name'];?></a>
									<div class="sub-menu">
										<ul class="category-links">
										<?php 
                                                while($sub_category = mysqli_fetch_array($scat))
                                                {
                                                ?>
											<li> <a href="<?php echo $siteurl; ?>products/<?php echo $category['slug'];?>/<?php echo $sub_category['slug'];?>/1" title=""><?php echo $sub_category['sub_category'];?></a> </li>
											<?php }?>
										</ul>
									</div>
								</li>
								<?php }else{ ?>
									<li><a href="<?php echo $siteurl; ?>products/<?php echo $category['slug'];?>" title=""><?php echo $category['category_name'];?></a></li>
								<?php }?>
								<?php } ?>
							</ul>
						</div>
						<!-- /Mega Menu -->
					</div>
				</div>
			</header>
			<!-- /Header -->