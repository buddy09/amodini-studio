<div class="block fullwidth full-nopad bottom-space">
					<div class="container">
						<!-- Main Slider -->
						<div class="mainSlider" data-thumb="true" data-thumb-width="230" data-thumb-height="100">
							<div class="sliderLoader">Loading...</div>
							<!-- Slider main container -->
							<div class="swiper-container">
								<div class="swiper-wrapper">
									<!-- Slides -->
									<?php 
									$bnrs = mysqli_query($con, "select *from banner where status='1'") or die(mysqli_error());
									while($bnr = mysqli_fetch_array($bnrs))
									{
									?>
									<div class="swiper-slide" data-thumb="admin/images/banner/<?php echo $bnr['photo'];?>">
										<!-- _blank or _self ( _self is default )-->
										<div class="wrapper">
											<figure><img src="admin/images/banner/<?php echo $bnr['photo'];?>" alt=""></figure>
										</div>
									</div>
									<?php }?>
									
								</div>
								<!-- pagination -->
								<div class="swiper-pagination"></div>
								<!-- pagination thumbs -->
								<div class="swiper-pagination-thumbs">
									<div class="thumbs-wrapper">
										<div class="thumbs"></div>
									</div>
								</div>
							</div>
						</div>
						<!-- /Main Slider -->
					</div>
				</div>