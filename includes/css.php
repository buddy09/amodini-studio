<meta name="author" content="Vishnu Choudhary">
<meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="favicon.ico">

<!-- Vendor -->
<link href="<?php echo $siteurl; ?>js/vendor/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>js/vendor/slick/slick.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>js/vendor/swiper/swiper.min.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>js/vendor/magnificpopup/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>js/vendor/nouislider/nouislider.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>js/vendor/darktooltip/dist/darktooltip.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/animate.css" rel="stylesheet">

<!-- Custom -->
<link href="<?php echo $siteurl; ?>css/style.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/megamenu.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/tools.css" rel="stylesheet">

<!-- Color Schemes -->
<link href="<?php echo $siteurl; ?>css/style-color-pink.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-green.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-lightgreen.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-blue.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-lightblue.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-violet.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-tomato.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-caribbean.css" rel="stylesheet">
<link href="<?php echo $siteurl; ?>css/style-color-orange.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $siteurl; ?>js/notifications/css/lobibox.min.css"/>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Icon Font -->
<link href="<?php echo $siteurl; ?>fonts/icomoon-reg/style.css" rel="stylesheet">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700|Raleway:100,100i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    .myaccount-tab-menu {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
}
.nav {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}
.myaccount-tab-menu a:hover, .myaccount-tab-menu li.active {
    background-color: #f82e56;
        border-color: #f82e56;
    /* color: #fff; */
}
.myaccount-tab-menu, .myaccount-tab-menu li.active a {
    /* background-color: #f82e56;
        border-color: #f82e56; */
 color: #fff; 
}
.myaccount-tab-menu a {
    border: 1px solid #efefef;
    border-bottom: none;
    color: #222222;
    font-weight: 400;
    font-size: 15px;
    display: block;
    padding: 10px 15px;
    text-transform: capitalize;
}
.myaccount-tab-menu a i.fa {
    font-size: 14px;
    text-align: center;
    width: 25px;
}
.myaccount-content h5 {
    border-bottom: 1px dashed #ccc;
    padding-bottom: 10px;
    margin-bottom: 25px;
}
</style>