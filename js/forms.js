$(document).ready(function () {
    $('#registerNow').on('click', function () {
      $("#registerNow").attr("enabled", "enabled");
      
  
      var y = $('#fname').val();
      if (y == null || y == "") {
        $("#error").show();
        document.getElementById("error").innerHTML = "First name must be filled out.";
        return false;
      }
      var l = $('#lname').val();
      if (l == null || l == "") {
        $("#error").show();
        document.getElementById("error").innerHTML = "Last name must be filled out.";
        return false;
      }
      var x = $('#email').val();
      if (x.length == 0) {
        $("#error").show();
        document.getElementById("error").innerHTML = "Email must be filled out";
        return false;
      }
  
      var m = $('#phone').val();
      if (m.length != 10) {
        $("#error").show();
        document.getElementById("error").innerHTML = "Phone number must be 10 digits long";
        return false;
      }
   
    
      var a = $('#password').val();
      if (a == null || a == "") {
        $("#error").show();
        document.getElementById("error").innerHTML = "Password must be filled out";
        return false;
      }
  
      if (a.length < 1 || a.length > 15) {
        $("#error").show();
        document.getElementById("error").innerHTML = "Passwords must be 5 to 15 characters long.";
        return false;
      }
      var b = $('#cpassword').val();
      if (a != b) {
        $("#error").show();
        document.getElementById("error").innerHTML = "Passwords must match.";
        return false;
      }
      var fname = $('#fname').val();
      var lname = $('#lname').val();
      var email = $('#email').val();
      var phone = $('#phone').val();
      var address = $('#address').val();
      var city = $('#city').val();
      var state = $('#state').val();
      var country = $('#country').val();
      var zip = $('#zip').val();
      var password = $('#password').val();
      if (fname != "" && lname != "" && email != "" && phone != "" && password != "") {
        $('#registerNow').html('Please Wait..');
        $('#registerNow').attr("disabled", true);
        //alert()
        $.ajax({
          url: "save.php",
          type: "POST",
          data: {
            type: 1,
            fname: fname,
            lname: lname,
            email: email,
            phone: phone,
            address:address,
            city:city,
            state:state,
            country:country,
            zip:zip,
            password: password
          },
          cache: false,
          success: function (dataResult) {
           //alert(dataResult);
            var dataResult = JSON.parse(dataResult);
            if (dataResult.statusCode == 205) {
              location.href = "index.php";
              // $("#butsave").removeAttr("disabled");
              // $('#register_form').find('input:text').val('');
              // $("#success").show();
              // $('#success').html('Registration successful !');             
            } else if (dataResult.statusCode == 204) {
              $('#registerNow').html("Create Account");
              $('#registerNow').attr("disabled", false);
            //   $("#error").show();
            //   $('#error').html('Email ID already exists !');
              Swal.fire(
                'Error!',
                'Email ID already exists!',
                'error'
              )
            } else if (dataResult.statusCode == 206) {
              $('#registerNow').html("Create Account");
              $('#registerNow').attr("disabled", false);
  
              // $("#error").show();
              //$('#error').html('Error!');
              Swal.fire(
                'Error!',
                'something went wrong!',
                'error'
              )
            } else if (dataResult.statusCode == 207) {
                $('#registerNow').html("Create Account");
                $('#registerNow').attr("disabled", false);
    
                alert(dataResult.msg);
              }
  
          }
        });
      } else {
        alert('Please fill all the field !');
      }
    });

    $('.butlogin').on('click', function () {
        var username = $('.email_log').val();
        var password = $('.password_log').val();
        if (username != "" && password != "") {
          $('.butlogin').html('Please Wait..');
          $('.butlogin').attr("disabled", true);
          
          $.ajax({
            url: "save.php",
            type: "POST",
            data: {
              type: 2,
              username: username,
              password: password
            },
            cache: false,
            success: function (dataResult) {
              //alert(dataResult);
              var dataResult = JSON.parse(dataResult);
              // alert(dataResult.statusCode);
              if (dataResult.statusCode == 203) {
                //location.href = "account.php?q=1";
                location.href = "index.php";
              } else if (dataResult.statusCode == 204) {
                $(".error_log").show();
                $('.butlogin').html('Login');
                $('.butlogin').attr("disabled", false);
                $('.error_log').html('Invalid EmailId or Password !');
              }
            }
          });
        } else {
          alert('Please fill all the field !');
        }
      });

      $('#changepassword').on('click', function(e) {
        $("#changepassword").attr("enabled", "enabled");
    
         var y = $('#old_password').val(); 
        if (y == null || y == "") {
          $("#cerror").show();
          document.getElementById("cerror").innerHTML="Old Password must be filled out.";
          return false;
        }
       var a = $('#newpassword').val();
        if(a == null || a == ""){
          $("#cerror").show();
          document.getElementById("cerror").innerHTML="Password must be filled out";
          return false;
        }
        if(a.length<5 || a.length>15){
          $("#cerror").show();
          document.getElementById("cerror").innerHTML="Passwords must be 5 to 15 characters long.";
          return false;
        }
        var b = $('#cpassword2').val();
        if (a!=b){
          $("#cerror").show();
          document.getElementById("cerror").innerHTML="Passwords must match.";
          return false;
        }
        var us_id = $('#us_id').val();
        var old_password = $('#old_password').val();
        var newpassword = $('#newpassword').val();
        if (old_password != "" && newpassword != "") {
          $('#changepassword').html('Please Wait..');
          $('#changepassword').attr("disabled", true);
          
          $.ajax({
            url: "save.php",
            type: "POST",
            data: {
              type: 3,
              us_id:us_id,
              old_password: old_password,
              newpassword: newpassword
            },
            cache: false,
            success: function (dataResult) {
              //alert(dataResult);
              var dataResult = JSON.parse(dataResult);
              // alert(dataResult.statusCode);
              if (dataResult.statusCode == 203) {
                //location.href = "account.php?q=1";
                $("#cerror").hide();
                $('#changepassword').html('Change Password');
                $('#changepassword').attr("disabled", false);
                $("#csuccess").show();
                $('#csuccess').html('Password Changed!');
                $('#old_password').val('');
                $('#newpassword').val('');
                $('#cpassword2').val('');
              } else if (dataResult.statusCode == 204) {
                $("#cerror").show();
                $('#changepassword').html('Change Password');
                $('#changepassword').attr("disabled", false);
                $('#cerror').html('Current Password is not correct!');
              }
            }
          });
        } else {
          alert('Please fill all the field !');
        }
      });

      $('#passwordreset').on('click', function () {
        var useremail = $('#useremail').val();
       
        if (useremail != "") {
          $('#passwordreset').html('Please Wait..');
          $('#passwordreset').attr("disabled", true);
          
          $.ajax({
            url: "save.php",
            type: "POST",
            data: {
              type: 'passwordreset',
              useremail:useremail
            },
            cache: false,
            success: function (dataResult) {
              //alert(dataResult);
              var dataResult = JSON.parse(dataResult);
              // alert(dataResult.statusCode);
              if (dataResult.statusCode == 203) {
                //location.href = "account.php?q=1";
                $("#asuccess").hide();
                $('#rsuccess').show();
                $('#rsuccess').html('<div class="title text-center" style="text-align: center;"><b>Instructions sent!</b></div> <div class="top-text" style="text-align: center;">Instructions for resetting your password have been sent to <b style="text-align:center;">'+useremail+'.</b> </div><span style="font-size: 12px;">You’ll receive this email within 5 minutes. Be sure to check your spam folder, too.</span></div>');
                
              } else if (dataResult.statusCode == 205) {
                $("#error_reset").show();
                $('#passwordreset').html('Send Me Instructions');
                $('#passwordreset').attr("disabled", false);
                $('#error_reset').html('Sorry, the address '+useremail+' is not known to Amodinistudio.in.                ');
              }
            }
          });
        } else {
          alert('Please fill all the field !');
        }
      });

      $('#addressUpdate').on('click', function () {
        var use_id = $('#userr_id').val();
        var address = $('#address').val();
        var city = $('#city').val();
        var state = $('#state').val();
         var country = $('#country').val();
         var zip = $('#zip').val();

         var s_address = $('#s_address').val();
         var s_city = $('#s_city').val();
         var s_state = $('#s_state').val();
          var s_country = $('#s_country').val();
          var s_zip = $('#s_zip').val();

        if (use_id != "") {
          $('#addressUpdate').html('Please Wait..');
          $('#addressUpdate').attr("disabled", true);
          
          $.ajax({
            url: "save.php",
            type: "POST",
            data: {
              type: 5,
              use_id:use_id,
              address: address,
              city: city,
              state: state,
              country: country,
              zip:zip,
              s_address:s_address,
              s_city:s_city,
              s_state:s_state,
              s_country:s_country,
              s_zip:s_zip

            },
            cache: false,
            success: function (dataResult) {
              //alert(dataResult);
              var dataResult = JSON.parse(dataResult);
              // alert(dataResult.statusCode);
              if (dataResult.statusCode == 203) {
                //location.href = "account.php?q=1";
                $("#asuccess").show();
                $('#addressUpdate').html('Update');
                $('#addressUpdate').attr("disabled", false);
                $('#asuccess').html('Address Update Successfully.');
                
              } else if (dataResult.statusCode == 204) {
                $("#aerror").show();
                $('#addressUpdate').html('Update');
                $('#addressUpdate').attr("disabled", false);
                $('#aerror').html('something went wrong!');
              }
            }
          });
        } else {
          alert('Please fill all the field !');
        }
      });


});