<?php 
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
if(isset($_SESSION['username'])){
    header('location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Seikō HTML 5 eCommerce Responsive Theme</title>
	<?php include'includes/css.php'; ?>
</head>

<body class="boxed">
    <!-- Loader -->
    <div id="loader-wrapper">
        <div class="cube-wrapper">
            <div class="cube-folding">
                <span class="leaf1"></span>
                <span class="leaf2"></span>
                <span class="leaf3"></span>
                <span class="leaf4"></span>
            </div>
        </div>
    </div>
    <div id="wrapper">
        <!-- Page -->
        <div class="page-wrapper">
            <?php include 'includes/head.php'; ?>
            <?php include 'includes/menu.php'; ?>
            <!-- Page Content -->
            <main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="icon icon-home"></i></a></li>
							<li>/<span>Login</span></li>
						</ul>
					</div>
				</div>
				<div class="block">
					<div class="container">
						<div class="row row-eq-height">
							<div class="col-sm-6">
								<div class="form-card">
									<h4>New Customers</h4>
									<p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
									<div><a href="register.php" class="btn btn-lg"><i class="icon icon-user"></i><span>Create An Account</span></a></div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-card">
									<h4>Login</h4>
                                    <p>If you have an account with us, please log in.</p>
                                    <div class="alert alert-danger error_log alert-dismissible" id="error_log" style="display:none;">
								    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
								    </div>
									<form class="account-create" action="#" method="POST">
										<label>E-mail<span class="required">*</span></label>
										<input type="email" required class="form-control email_log input-lg" name="email" id="email_log" placeholder="E-mail*">
										<label>Password<span class="required">*</span></label>
										<input type="password" required class="form-control password_log input-lg" name="password" id="password_log" placeholder="Password*">
										<div>
											<button class="btn butlogin" type="button" id="butlogin">Login</button><span class="required-text">* Required Fields</span></div>
										<div class="back"><a href="#">Forgot Your Password?</a></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
            <!-- /Page Content -->
            <?php include 'includes/footer.php'; ?>
        </div>
        <!-- /Page -->
    </div>
    <?php include 'includes/footerJs.php'; ?>
</body>
</html>