<?php
date_default_timezone_set("Asia/Kolkata");
include 'includes/config.php';
$slug = $_REQUEST['param1'];
$size_id = $_REQUEST['param2'];
$color_id = $_REQUEST['param3'];
$query = mysqli_query($con, "select *from tbl_products where slug='$slug'") or die(mysqli_error());
$product = mysqli_fetch_array($query);
$pid = $product['product_id'];
//$size_ids = mysqli_query($con, "select distinct(size_id) from tbl_product_images where product_id='$pid'") or die(mysqli_error());
$message = '';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Product Detail</title>
	<?php include 'includes/css.php'; ?>
</head>

<body class="boxed">
		
	<div id="wrapper">
		<!-- Sidebar -->
		
		<!-- /Sidebar -->
		<!-- Page -->
		<div class="page-wrapper">

		<?php 
		if(isset($_POST['add-to-cart'])){
			$uid = $_SESSION['uid'];
			$product_id = $_POST['hdnproduct_id'];
			$quantity = $_POST['quantity'];
			$price = $_POST['hdnprice'];
			$size = explode("~",$_POST['size']);
			$sz = $size[0];
			$color = explode("~",$_POST['color']);
			$clr = $color[0];

			$image = $_POST['hdnimage'];
			$rw = mysqli_query($con, "select count(*) as count from tbl_cart where uid='$uid' AND product_id='$product_id' and size='$sz' and color='$clr'") or die(mysqli_error());
			$dw = mysqli_fetch_array($rw);
			$count = $dw['count'];
			if($count == 0){
				mysqli_query($con, "insert into tbl_cart(uid,product_id,price,size,color,quantity,image) values('$uid','$product_id','$price','$sz','$clr','$quantity','$image')") or die(mysqli_error());
				$message = 'Product added to cart..';
			}else{
				$message = 'Product already in cart..';
			}
		}
		?>

		<?php include 'includes/head.php'; ?>
			<!-- Sidebar -->
			
			<!-- /Sidebar -->
			<!-- Page Content -->
			<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href=""><i class="icon icon-home"></i></a></li>
							<!-- <li>/<a href="#">Women</a></li> -->
							<li>/<span><?php echo $product['product_name'];?></span></li>
						</ul>
					</div>
				</div>
				<div class="block product-block">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 col-md-6 col-lg-4">
								<!-- Product Gallery -->
								<div class="main-image">
									<?php 
									$product_id = $product['product_id'];
									if($size_id ==0 && $color_id==0){
										$at = mysqli_query($con, "select *from tbl_attributes where product_id = '$product_id' and is_default=1 order by '$product_id' DESC LIMIT 1") or die(mysqli_error());
									}else{
										$at = mysqli_query($con, "select *from tbl_attributes where product_id = '$product_id' and size_id=$size_id and color_id='$color_id' order by '$product_id' DESC LIMIT 1") or die(mysqli_error());
									}
									$att = mysqli_fetch_array($at);
									$aid = $att['id'];
									$a = mysqli_query($con, "select product_photo from tbl_product_images where attribute_id='$aid' order by imgid desc limit 0,1") or die(mysqli_error());
									$aa = mysqli_fetch_array($a);
									?>
									<img src="<?php echo $siteurl; ?>admin/uploads/productImages/<?php echo $aa['product_photo'];?>" class="zoom" alt="" data-zoom-image="<?php echo $siteurl; ?>admin/uploads/productImages/<?php echo $mimage['product_photo'];?>" />
									<div class="dblclick-text"><span>Double click for enlarge</span></div>
									<!-- <a href="http://www.youtube.com/watch?v=qZeeMm35LXo" class="video-link"><i class="icon icon-film"></i></a> -->
									<a href="images/products/large/product-gallery-1.jpg" class="zoom-link"><i class="icon icon-zoomin"></i></a>
								</div>
								<div class="product-previews-wrapper">
									<div class="product-previews-carousel" id="previewsGallery">
										<?php 
										$images = mysqli_query($con, "select product_photo from tbl_product_images where attribute_id = '$aid' order by '$product_id' DESC") or die(mysqli_error());
										while($imgs = mysqli_fetch_array($images))
										{
										?>
										<a href="#" data-image="<?php echo $siteurl; ?>admin/uploads/productImages/<?php echo $imgs['product_photo'];?>" data-zoom-image="<?php echo $siteurl; ?>admin/uploads/productImages/<?php echo $imgs['product_photo'];?>"><img src="<?php echo $siteurl; ?>admin/uploads/productImages/<?php echo $imgs['product_photo'];?>" style="height:125px" alt="" /></a>
										<?php }?>
									</div>
								</div>
								<!-- /Product Gallery -->
							</div>
							<div class="col-sm-6 col-md-6 col-lg-5">
								<div class="product-info-block classic">
									<!-- <div class="product-info-top">
										<div class="product-sku">SKU: <span>Stock Keeping Unit</span></div>
										<div class="rating">
											<i class="icon icon-star fill"></i><i class="icon icon-star fill"></i><i class="icon icon-star fill"></i><i class="icon icon-star fill"></i><i class="icon icon-star"></i><span class="count">248 reviews</span>
										</div>
									</div>
									<div class="product-name-wrapper">
										<h1 class="product-name">Cover up tunic</h1>
										<div class="product-labels">
											<span class="product-label sale">SALE</span>
											<span class="product-label new">NEW</span>
										</div>
									</div> -->
									<div class="product-availability">Availability: <span><?php $stock = $product['stock'];if($stock==1){echo 'In Stock';}else{echo 'Out of Stock';}?></span></div>
									<div class="product-description">
										<p><?php echo $product['short_description'];?></p>
									</div>
									<!-- <div class="countdown-circle hidden-xs">
										<div class="countdown-wrapper">
											<div class="countdown" data-promoperiod="100000000"></div>
											<div class="countdown-text">
												<div class="text1">Discount 45% OFF</div>
												<div class="text2">Hurry, there are only <span>14</span> item(s) left!</div>
											</div>
										</div>
									</div> -->
									<form action="" method="post">
									<input type="hidden" name="hdnproduct_id" value="<?php echo $product['product_id'];?>">
									<input type="hidden" name="hdnimage" value="<?php echo $aa['product_photo'];?>">
									<div class="product-options">
										<div class="product-color">
											<span class="option-label">Size:</span>
											<div class="select-wrapper-sm">
											<select name="size" id="size" class="form-control input-sm" style="width:100px">
											<?php 
											$product_id = $product['product_id'];
											$sid = $att['size_id'];
											$size = mysqli_query($con, "select distinct(a.size_id) as size_id,s.size_name from tbl_attributes a,tbl_size s where a.size_id=s.size_id and a.product_id='$product_id' and a.size_id='$sid'") or die(mysqli_error());
											$sze = mysqli_fetch_array($size);
											?>
											<option value="<?php echo $sze['size_name'];?>~<?php echo $sze['size_id'];?>"><?php echo $sze['size_name'];?></option>
											<?php 
											$ss = mysqli_query($con, "select distinct(a.size_id) as size_id,s.size_name from tbl_attributes a,tbl_size s where a.size_id=s.size_id and a.product_id='$product_id'") or die(mysqli_error());
											while($sss = mysqli_fetch_array($ss))
											{
											?>
											<option value="<?php echo $sss['size_name'];?>~<?php echo $sss['size_id'];?>"><?php echo $sss['size_name'];?></option>
											<?php }?>
											</select>
											</div>
										</div>

										<div class="product-color">
											<span class="option-label">Color:</span>
											<div class="">
												<?php 
												$sid = $att['size_id'];
												$cid = $att['color_id'];
												$clr = mysqli_query($con, "select distinct(a.color_id) as color_id,c.color_code from tbl_attributes a,tbl_color c where a.color_id=c.id and a.product_id='$product_id' and a.size_id='$sid' and a.color_id='$cid'") or die(mysqli_error());
												$cl = mysqli_fetch_array($clr);
												?>
												<span style="background-color:<?php echo $cl['color_code'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <input type="radio" name="color" id="color" checked value="<?php echo $cl['color_code'];?>~<?php echo $cl['color_id'];?>">&nbsp;
												<?php 
												$clrr = mysqli_query($con, "select a.color_id,c.color_code from tbl_attributes a,tbl_color c where a.color_id=c.id and a.product_id='$product_id' and a.size_id='$sid' and a.color_id!='$cid'") or die(mysqli_error());
												while($clrrr = mysqli_fetch_array($clrr))
												{
												?>
												<span style="background-color:<?php echo $clrrr['color_code'];?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <input type="radio" name="color" id="color" value="<?php echo $clrrr['color_code'];?>~<?php echo $clrrr['color_id'];?>">&nbsp;
												<?php }?>
											</div>
										
										</div>
										<div class="product-qty">
											<span class="option-label">Qty:</span>
											<div class="qty qty-changer">
												<fieldset>
													<input type="button" value="&#8210;" class="decrease">
													<input type="text" class="qty-input" value="1" data-min="0" data-max="10" name="quantity">
													<input type="button" value="+" class="increase">
												</fieldset>
											</div>
										</div>
										<div class="product-qty">
											<span class="option-label">Delivery Charges:</span>
											 <span>&nbsp;&nbsp;Free Delivery</span>
										</div>
									</div>
									<div class="product-actions">
										<div class="row">
											<div class="col-md-6">
												<!-- <div class="product-meta">
													<span><a href="#"><i class="icon icon-heart"></i> Add to wishlist</a></span>
													<span><a href="#"><i class="icon icon-balance"></i> Add to Compare</a></span>
												</div>
												<div class="social">
													<div class="share-button toLeft">
														<span class="toggle">Share</span>
														<ul class="social-list">
															<li>
																<a href="#" class="icon icon-google google"></a>
															</li>
															<li>
																<a href="#" class="icon icon-fancy fancy"></a>
															</li>
															<li>
																<a href="#" class="icon icon-pinterest pinterest"></a>
															</li>
															<li>
																<a href="#" class="icon icon-twitter-logo twitter"></a>
															</li>
															<li>
																<a href="#" class="icon icon-facebook-logo facebook"></a>
															</li>
														</ul>
													</div>
												</div> -->
											</div>
											<div class="col-md-6">
											<?php 
											if(isset($_POST['add-to-cart'])){
												echo '<span style="color:green;font-weight:bold"><span>'.$message.'</span></span>';
											}
											?>
											<div class="countdown-text">
											<?php 
											$dis = $att['discount'];
											if($dis > 0.00)
											{
											?>
                                            <div class="text1">Discount <?php echo $att['discount'];?>% OFF</div>
											<?php }?>
                                            </div>
												<?php 
												if($dis > 0.00)
												{
												?>
												<div class="price">
												<span class="old-price"><span>&#8377;<?php echo number_format($att['price'], 2);?></span></span>
													<span class="special-price"><span>&#8377;<?php echo number_format($att['price']-(($att['price']*$att['discount'])/100), 2);?></span>
													<input type="hidden" name="hdnprice" value="<?php echo $att['price']-(($att['price']*$att['discount'])/100);?>">
												</div>
												<?php }else{?>
												<div class="price">
													<span class="special-price"><span>&#8377;<?php echo number_format($att['price'], 2);?></span>
													<input type="hidden" name="hdnprice" value="<?php echo $att['price']?>">
												</div>
												<?php }?>
												<div class="actions">
													<button type="submit" name="add-to-cart" data-loading-text='<i class="icon icon-spinner spin"></i><span>Add to cart</span>' class="btn btn-lg btn-loading"><i class="icon icon-cart"></i><span>Add to cart</span></button>
													<a href="#" class="btn btn-lg product-details"><i class="icon icon-external-link"></i></a>
												</div>
											</div>
										</div>
										</form>
									</div>
								</div>
							</div>
              <div class="clearfix hidden-lg"></div>
							<!-- <div class="col-md-12 col-lg-3 hidden-quickview">
								<div class="box-icon-row">
									<div class="box-left-icon-bg">
										<div class="box-icon"><i class="icon icon-gift"></i></div>
										<div class="box-text">
											<div class="title">Special offer: 1+2=4</div>
											Get a gift!
										</div>
									</div>
									<div class="box-left-icon-bg">
										<div class="box-icon"><i class="icon icon-dollar-bills"></i></div>
										<div class="box-text">
											<div class="title">Free Reward Card</div>
											Worth $10, $50, $100
										</div>
									</div>
									<div class="box-left-icon-bg">
										<div class="box-icon"><i class="icon icon-undo"></i></div>
										<div class="box-text">
											<div class="title">Order return</div>
											Returns within 5 days
										</div>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<div class="block">
					<div class="tabaccordion">
						<div class="container">
							<!-- Nav tabs -->
							<ul class="nav-tabs product-tab" role="tablist">
								<li><a href="#Tab1" role="tab" data-toggle="tab">Description</a></li>
								<!-- <li><a href="#Tab2" role="tab" data-toggle="tab">Custom tab</a></li>
								<li><a href="#Tab3" role="tab" data-toggle="tab">Sizing Guide</a></li>
								<li><a href="#Tab4" role="tab" data-toggle="tab">Tags</a></li>
								<li><a href="#Tab5" role="tab" data-toggle="tab">Reviews</a></li> -->
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane" id="Tab1">
									<p><?php echo $product['product_description'];?></p>
								</div>
								<div role="tabpanel" class="tab-pane" id="Tab2">
									<h3 class="custom-color">Take a trivial example which of us ever undertakes</h3>
									<p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure </p>
									<ul class="marker-simple-list two-columns">
										<li>Nam liberempore</li>
										<li>Cumsoluta nobisest</li>
										<li>Eligendptio cumque</li>
										<li>Nam liberempore</li>
										<li>Cumsoluta nobisest</li>
										<li>Eligendptio cumque</li>
									</ul>
								</div>
								<div role="tabpanel" class="tab-pane" id="Tab3">
									<h3>Single Size Conversion</h3>
									<div class="table-responsive">
										<table class="table table-bordered">
											<tbody>
												<tr>
													<td><strong>UK</strong></td>
													<td>
														<ul class="params-row">
															<li>18</li>
															<li>20</li>
															<li>22</li>
															<li>24</li>
															<li>26</li>
														</ul>
													</td>
												</tr>
												<tr>
													<td><strong>European</strong></td>
													<td>
														<ul class="params-row">
															<li>46</li>
															<li>48</li>
															<li>50</li>
															<li>52</li>
															<li>54</li>
														</ul>
													</td>
												</tr>
												<tr>
													<td><strong>US</strong></td>
													<td>
														<ul class="params-row">
															<li>14</li>
															<li>16</li>
															<li>18</li>
															<li>20</li>
															<li>22</li>
														</ul>
													</td>
												</tr>
												<tr>
													<td><strong>Australia</strong></td>
													<td>
														<ul class="params-row">
															<li>8</li>
															<li>10</li>
															<li>12</li>
															<li>14</li>
															<li>16</li>
														</ul>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="Tab4">
									<ul class="tags">
										<li><a href="#"><span class="value"><span>Dresses</span></span></a></li>
										<li><a href="#"><span class="value"><span>Outerwear</span></span></a></li>
										<li><a href="#"><span class="value"><span>Tops</span></span></a></li>
										<li><a href="#"><span class="value"><span>Sleeveless tops</span></span></a></li>
										<li><a href="#"><span class="value"><span>Sweaters</span></span></a></li>
									</ul>
									<div class="divider"></div>
									<h3>Add your tag</h3>
									<form class="contact-form white" action="#">
										<label>Tag<span class="required">*</span></label>
										<input class="form-control input-lg">
										<div>
											<button class="btn btn-lg">Submit Tag</button>
										</div>
										<div class="required-text">* Required Fields</div>
									</form>
								</div>
								<div role="tabpanel" class="tab-pane" id="Tab5">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<td></td>
													<td class="text-center">1 star</td>
													<td class="text-center">2 star</td>
													<td class="text-center">3 star</td>
													<td class="text-center">4 star</td>
													<td class="text-center">5 star</td>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><strong>Price</strong></td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-price1" type="radio" name="vote-price" value="1"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-price2" type="radio" name="vote-price" value="2"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-price3" type="radio" name="vote-price" value="3"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-price4" type="radio" name="vote-price" value="4"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-price5" type="radio" name="vote-price" value="5"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
												</tr>
												<tr>
													<td><strong>Value</strong></td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-value1" type="radio" name="vote-value" value="1"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-value2" type="radio" name="vote-value" value="2"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-value3" type="radio" name="vote-value" value="3"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-value4" type="radio" name="vote-value" value="4"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-value5" type="radio" name="vote-value" value="5"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
												</tr>
												<tr>
													<td><strong>Quality</strong></td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-quality1" type="radio" name="vote-quality" value="1"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-quality2" type="radio" name="vote-quality" value="2"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-quality3" type="radio" name="vote-quality" value="3"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-quality4" type="radio" name="vote-quality" value="4"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
													<td class="text-center">
														<label class="radio">
															<input id="vote-quality5" type="radio" name="vote-quality" value="5"><span class="outer"><span class="inner"></span></span>
														</label>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<h3>Add new review</h3>
									<form class="contact-form white" action="#">
										<label>Review<span class="required">*</span></label>
										<textarea class="form-control input-lg"></textarea>
										<div>
											<button class="btn btn-lg">Submit Review</button>
										</div>
										<div class="required-text">* Required Fields</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main>
			<!-- /Page Content -->
			<!-- Footer -->

			<?php include 'includes/footer.php'; ?>
			
			<!-- /Footer -->

		</div>
		<!-- /Page -->
	</div>
	<!-- ProductStack -->
	
	<!-- /ProductStack -->
	<!-- Modal Quick View -->
	
	<!-- /Modal Quick View -->
    
	<!-- jQuery Scripts  -->
	
	<?php include 'includes/footerJs.php'; ?>

	<script>
	$('input[name=color]:radio').change(function(){
		var cid = $('input[name=color]:checked').val();
		var sid = $('#size').val();
		var cidd = cid.split("~");
		var ciddd = cidd[1];
		var sidd = sid.split("~");
        var siddd = sidd[1];
		window.location.href="<?php echo $siteurl.'product-detail/'.$slug.'/'?>" + siddd + '/' + ciddd;
	});
	</script>

</body>
</html>